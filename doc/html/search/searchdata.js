var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuv",
  1: "cgmpt",
  2: "abcdfgklpt",
  3: "acdefgiklmoprstu",
  4: "aehimnprstv",
  5: "dt",
  6: "bdt",
  7: "mop"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Énumérations",
  7: "Valeurs énumérées"
};


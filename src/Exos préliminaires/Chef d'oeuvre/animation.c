#include "animation.h"

void Affiche_Game(SDL_Window *fenetre, SDL_Renderer *rendu, SDL_DisplayMode *ecran, fond_t **tabJeu, int tabWidth, int tabHeight, personnage_t **perso, int nbPersonnages, int *mouvValide, SDL_Texture *joueur_Text, SDL_Texture *monstre_Text, SDL_Texture **tileMap)
{
    //avec le tableau de jeu mis a jour et la liste des deplacements reussis

    int i, j, x, y, phaseSprite;
    SDL_Rect spriteSize = {0, 0, 80, 80}, //zone dans la spritesheet
        spritePos;                        //zone dans la fenetre
    SDL_GetWindowSize(fenetre, &spritePos.w, &spritePos.h);
    spritePos.x = 0;
    spritePos.y = 0;
    spritePos.w = +(spritePos.w / (tabWidth));
    spritePos.h = spritePos.h / (tabHeight + 1);
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    SDL_Texture *curr_Text = NULL;

    for (phaseSprite = 0; phaseSprite < 8; phaseSprite++)
    {
        // fonction de dessin de fond
        dessine_fond(fenetre, rendu, tileMap, tabJeu, tabHeight, tabWidth, perso[0]->vieCourante, perso[0]->vieMax);

        for (i = 0; i < nbPersonnages; i++)
        {
            if (estVivant(perso[i]))
            {
                if (getType(perso[i]))
                    curr_Text = monstre_Text;
                else
                    curr_Text = joueur_Text;

                switch (perso[i]->regard)
                {
                case (NORD):
                    spriteSize.y = 5 * spriteSize.h;                      // choix de la ligne de sprite
                    spritePos.x = (perso[i]->positionMapX) * spritePos.w; // position actuelle du personnage en X
                    flip = SDL_FLIP_NONE;

                    if (mouvValide[i] && phaseSprite < 6)
                    {                                                                                               // cas ou il y a eu deplacement
                        spriteSize.x = (phaseSprite)*spriteSize.w;                                                  //passage au sprite suivant
                        spritePos.y = (2 + perso[i]->positionMapY) * spritePos.h - (phaseSprite * spritePos.h / 5); //deplacement leger
                    }
                    else
                    {
                        spriteSize.x = 0;
                        spritePos.y = (perso[i]->positionMapY + 1) * spritePos.h;
                    }

                    break;
                case (SUD):
                    spriteSize.y = 5 * spriteSize.h;
                    if (getType(perso[i]))
                        spriteSize.y += spriteSize.h;
                    spritePos.x = perso[i]->positionMapX * spritePos.w;
                    flip = SDL_FLIP_NONE;

                    if (mouvValide[i] && phaseSprite < 6)
                    {
                        spriteSize.x = 6 * spriteSize.w + (phaseSprite)*spriteSize.w;
                        spritePos.y = (perso[i]->positionMapY) * spritePos.h + (phaseSprite * spritePos.h / 5);
                    }
                    else
                    {
                        spriteSize.y = 0;
                        spriteSize.x = 0;
                        spritePos.y = (perso[i]->positionMapY + 1) * spritePos.h;
                    }

                    break;
                case (EST):
                    spriteSize.y = 0;
                    spritePos.y = (1 + perso[i]->positionMapY) * spritePos.h;
                    flip = SDL_FLIP_NONE;
                    if (mouvValide[i])
                    {
                        spriteSize.x = (phaseSprite + 1) * spriteSize.w;
                        spritePos.x = (perso[i]->positionMapX - 1) * spritePos.w + (phaseSprite * spritePos.w / 7);
                    }
                    else
                    {
                        spriteSize.x = 0;
                        spritePos.x = perso[i]->positionMapX * spritePos.w;
                    }

                    break;

                case (OUEST):
                    spriteSize.y = 0;
                    spritePos.y = (1 + perso[i]->positionMapY) * spritePos.h;
                    flip = SDL_FLIP_HORIZONTAL;

                    if (mouvValide[i])
                    {
                        spriteSize.x = (phaseSprite + 1) * spriteSize.w;
                        spritePos.x = (perso[i]->positionMapX + 1) * spritePos.w - (phaseSprite * spritePos.w / 7);
                    }
                    else
                    {
                        spriteSize.x = 0;
                        spritePos.x = perso[i]->positionMapX * spritePos.w;
                    }
                    break;

                default:
                    flip = SDL_FLIP_NONE;
                    spriteSize.x = 0;
                    spriteSize.y = 0;
                    spritePos.x = perso[i]->positionMapX * spritePos.w;
                    spritePos.y = (1 + perso[i]->positionMapY) * spritePos.h;
                    break;
                }
                //affichage
                SDL_RenderCopyEx(rendu, curr_Text, &spriteSize, &spritePos, 0, NULL, flip);
            }
        }

        SDL_RenderPresent(rendu);
        SDL_Delay(100);
        SDL_RenderClear(rendu);
    }
}

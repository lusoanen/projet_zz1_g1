#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include"baseSDL.h"
#include"direction.h"
#include"fond.h"
#include"personnage.h" // personnage

//SDL_Texture * joueur_Text = IMG_LoadTexture("assets/personnages/fille.png");
//SDL_Texture * monstre_Text= IMG_LoadTexture("assets/personnages/monstre.png");

/**
 * @brief fonction d'affichage du chef d'oeuvre
 * 
 * @param fenetre SDL_Window utilisée
 * @param rendu SDL_reneder utilisée
 * @param ecran SDL_displayMode utilisée
 * @param tabJeu map
 * @param tabWidth largeur max de la map
 * @param tabHeight hauteur max de la map
 * @param perso tableau de toutes les coordonnées des personnages (player et monstre)
 * @param nbPersonnages taille du tableau de personnage
 * @param mouvValide booléen validant la posibilité de se deplacer dans une direction donnée
 * @param joueur_Text texture du personnage du joueur
 * @param monstre_Text texture des monstres
 * @param tab_texture texture de la map
 */
void Affiche_Game(SDL_Window * fenetre, SDL_Renderer * rendu, SDL_DisplayMode * ecran,fond_t** tabJeu,int tabWidth,int tabHeight ,personnage_t** perso,int nbPersonnages,int* mouvValide,SDL_Texture * joueur_Text,SDL_Texture * monstre_Text,SDL_Texture ** tab_texture);



#endif
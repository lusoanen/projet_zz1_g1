#include"baseSDL.h"


void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	fermetureSdl(0, "ERROR SDL INIT", *fenetre, *rendu);

       if(SDL_GetCurrentDisplayMode(0, ecran) != 0) 
              fermetureSdl(0, "ERROR GET DISPLAY MODE", *fenetre, *rendu);
       

	*fenetre = SDL_CreateWindow("Labyrinthe",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, ecran->w * 0.66,ecran->h * 0.66, SDL_WINDOW_OPENGL);
	if (*fenetre == NULL) 
		fermetureSdl(0, "ERROR WINDOW CREATION", *fenetre, *rendu);


	*rendu = SDL_CreateRenderer(*fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (*rendu == NULL) 
	  	fermetureSdl(0, "ERROR RENDERER CREATION", *fenetre, *rendu);


}

void fermetureSdl(int ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu) 
{                 
  char messageParDefaut[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(messageParDefaut, message, 250);                                 
         l = strlen(messageParDefaut);                                        
         strcpy(messageParDefaut + l, " : %s\n");                     

         SDL_Log(messageParDefaut, SDL_GetError());                   
  }                                                               

  if (rendu != NULL) SDL_DestroyRenderer(rendu);                            
  if (fenetre != NULL)   SDL_DestroyWindow(fenetre);                                     
  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}   
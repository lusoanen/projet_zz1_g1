#ifndef _BASE_SDL_H_
#define _BASE_SDL_H_

#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/**
 * @brief initialisation de la SDL
 * 
 * @param fenetre SDL_Window utilisée
 * @param rendu SDL_renderer utilisé
 * @param ecran SDL_DisplayMode utilisé
 */
void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran);

/**
 * @brief fermeture de la SDL
 * 
 * @param ok validation de fermeture sans erreur
 * @param message mesage de validation de fermeture sans erreur
 * @param fenetre SDL_Window utilisée
 * @param rendu  SDL_renderer utilisé
 */
void fermetureSdl(int ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);

#endif
#ifndef _BOOL_H
#define _BOOL_H

typedef enum
{
    FALSE, // = 0
    TRUE   // = 1
} bool_t;

#endif
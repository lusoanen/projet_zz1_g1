#include "chargementJeu.h"


FILE * ouvrirFichier(){
	FILE * fic = fopen("chefDOeuvre.txt", "r");
	if(fic == NULL)
		fprintf(stderr, "ouverture fichier impossible\n" );
	return fic;
}

void fermerFichier(FILE * fic){
	int ok = fclose(fic);
	if(ok != 0)
		fprintf(stderr, "echec de la fermeture de fichier\n");
}

fond_t ** chargerMap(int * ligne, int * colonne){
	FILE * fic = ouvrirFichier();
	int i;

	fscanf(fic, "%d %d", ligne, colonne);

	fond_t ** grille = malloc(sizeof(fond_t*)* (*ligne));
	for (i = 0; i < *ligne; ++i)
	{
		grille[i] = malloc(sizeof(fond_t)* (*colonne));
	}


	for(int j=0; j < *ligne; j++){
		for(i=0; i < *colonne; i++){
			fscanf(fic, "%d", (int*)(&grille[j][i]));
		}
	}

	fermerFichier(fic);

	return grille;
}
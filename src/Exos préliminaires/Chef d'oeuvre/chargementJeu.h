#ifndef _CHARGEMENT_H_
#define _CHARGEMENT_H_

#include "fond.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAILLEMAXMONSTRE 20 
#define TAILLEMAX 500 

FILE * ouvrirFichier();
void fermerFichier(FILE * fic);
fond_t ** chargerMap(int * ligne, int * colonne);

#endif
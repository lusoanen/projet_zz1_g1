#include "direction.h"

direction_t bord(int nb_ligne, int nb_colonne, int i, int j)
{
	direction_t bord = CENTRE;
	if (i == 0)
	{
		bord += NORD;
	}
	else if (i == nb_ligne-1)
	{
		bord += SUD;
	}
	if (j == 0)
	{
		bord = 2*bord + OUEST;
	}
	else if (j == nb_colonne-1)
	{
		bord = 2*bord + EST;
	}

	return bord;
}
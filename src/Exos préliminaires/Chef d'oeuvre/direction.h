#ifndef _DIRECTION_H_
#define _DIRECTION_H_

/**< enumération des différentes direction*/
typedef enum direction
{
	CENTRE,
	NORD,
	SUD,
	EST,
	OUEST,
	NE,
	NO,
	SE,
	SO
} direction_t;

/**
 * @brief definition de la direction dans lequel le bord de la map se trouve
 * 
 * @param nb_ligne nombre de ligne max de la map
 * @param nb_colonne nombre de colonne max de la map
 * @param i indice Y dans la map
 * @param j indice X dans la map
 * @return direction_t la où se trouve le bord, centre sinon
 */
direction_t bord(int nb_ligne, int nb_colonne, int i, int j);

#endif
#include "fond.h"
#include "bool.h"

SDL_Color couleur_fond = {181,176,221,255};

const char * num_text[15] = {"0032",  // Cactus
							 "0031",  // Palmier
							 "0134",  // Coffre
							 "0129",  // Coeur
							 "0128",  // Demi coeur
							 "0127",  // Coeur vide
							 "0022",  // Sable centre
							 "0005",  // Sable N
							 "0039",  // Sable S
							 "0023",  // Sable E
							 "0021",  // Sable O
							 "0006",  // Sable NE
							 "0004",  // Sable NO
							 "0040",  // Sable SE
							 "0038"}; // Sable SO

void charge_img_fond(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture ** tab_texture)
{
	bool_t ok = TRUE;
	int i = 0;
	char chemin[50];
	char file_name[20];

	while (ok && i < NB_TEXT)
	{
		sprintf(file_name,"tile_%s.png", num_text[i]);
		sprintf(chemin,"./asset/map/Default/Tiles/%s", file_name);
		tab_texture[i] = IMG_LoadTexture(renderer, chemin);
		if (tab_texture[i] == NULL)
		{
			ok = FALSE;
			for (int j=0; j < i; j++)
			{
				SDL_DestroyTexture(tab_texture[j]);
			}
			fermetureSdl(0, "Problème de chargement d'une texture de fond", fenetre, renderer);
		}
		i++;
	}
}

void free_tab_texture(SDL_Texture ** tab_texture)
{
	for (int i = 0; i < NB_TEXT; i++)
	{
		SDL_DestroyTexture(tab_texture[i]);
	}
	free(tab_texture);
}

void dessine_fond(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * tab_texture[], fond_t** grille, int nb_ligne, int nb_colonne, int vie_actuelle, int vie_max)
{
	SDL_SetRenderDrawColor(renderer, couleur_fond.r, couleur_fond.g, couleur_fond.b, couleur_fond.a);
	SDL_RenderClear(renderer);
	SDL_RenderFillRect(renderer,NULL);

	SDL_Rect position = {0,0, 0, 0};
	SDL_GetWindowSize(fenetre, &(position.w), &(position.h));
	position.w /= nb_colonne;
	position.h /= nb_ligne+1;

	int i=0;
	while (vie_max > 0)
	{
		fond_t coeur = COEUR_VIDE;
		if (vie_actuelle >= 2)
		{
			vie_actuelle -= 2;
			coeur = COEUR;
		}
		else if (vie_actuelle == 1)
		{
			vie_actuelle -= 1;
			coeur = DEMI_COEUR;
		}
		SDL_RenderCopy(renderer, tab_texture[coeur], NULL, &position);
		vie_max -= 2;
		i++;
		position.x = position.w*i;
	}

	for (i=0; i < nb_ligne; i++)
	{
		for (int j=0; j < nb_colonne; j++)
		{
			position.x = position.w*j;
			position.y = position.h*(i+1);

			if (grille[i][j] == SABLE)
			{
				SDL_RenderCopy(renderer, tab_texture[SABLE+bord(nb_ligne,nb_colonne,i,j)], NULL, &position);
			}
			else 
			{
				SDL_RenderCopy(renderer, tab_texture[grille[i][j]], NULL, &position);
			}
		}
	}
}
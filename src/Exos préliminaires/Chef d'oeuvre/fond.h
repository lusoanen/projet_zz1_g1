#ifndef _FOND_H_
#define _FOND_H_

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "baseSDL.h"
#include "direction.h"

#define NB_TEXT 15 /**< nombre de texture */
#define COTE_TEXT 16 

/**< énumération pour la texture des case de la map*/
typedef enum fond
{
	CACTUS,
	PALMIER,
	COFFRE,
	COEUR,
	DEMI_COEUR,
	COEUR_VIDE,
	SABLE
} fond_t;

/**
 * @brief chargement des texture de la map
 * 
 * @param fenetre SDL_window utilisée
 * @param renderer SDL_window utilisée
 * @param tab_texture tableau des texture de chaques cases de la map
 */
void charge_img_fond(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture ** tab_texture);

/**
 * @brief libération en mémoire du tableau des textures
 * 
 * @param tab_texture 
 */
void free_tab_texture(SDL_Texture * tab_texture[]);

/**
 * @brief fonction de crétaion graphique de la map
 * 
 * @param fenetre SDL_window utilisée
 * @param renderer SDL_window utilisée
 * @param tab_texture tableau des texture de chaques cases de la map
 * @param grille matrice 2D représentant la grille des cases de la map
 * @param nb_ligne nombre de ligne de la matrice
 * @param nb_colonne nombre d ecolonne de la matrice
 * @param vie_actuelle vie courante du personnage du joueur
 * @param vie_max vie max du personnage du joueur
 */
void dessine_fond(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture * tab_texture[], fond_t** grille, int nb_ligne, int nb_colonne, int vie_actuelle, int vie_max);

#endif
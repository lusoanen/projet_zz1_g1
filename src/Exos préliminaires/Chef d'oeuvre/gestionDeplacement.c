#include "gestionDeplacement.h"

bool_t estBord(int pos, int max, int sens){
	int resultat = pos + sens;
	bool_t estBord = TRUE;
	if(resultat >= 0 && resultat < max)
		estBord = FALSE;

	return estBord;
}

int estEnnemi(int x, int y, fond_t ** grille, personnage_t ** perso, int max){
	int degat = -1;

	switch(grille[y][x]){
		case PALMIER:
			degat = 0;
			break;
		case CACTUS :
			degat = 1;
			break;
		default:
			break;
	}

	//printf("%d %d\n",x,y );
	for(int i=0; i < max; i++){
		if(estVivant(perso[i]) && perso[i]->positionMapX == x && perso[i]->positionMapY == y){
			degat = perso[i]->attaque;
		}
	}
	
	return degat;
}

bool_t estCoffre(int x, int y, fond_t ** grille){
	bool_t estCoffre = FALSE;
	if(grille[y][x] == COFFRE)
	{
		estCoffre = TRUE;
	}
	return estCoffre;
}

bool_t deplacerPersonnage(int indice, fond_t ** grille, int nbLigne, int nbColonne, direction_t direction, personnage_t ** persos, int nbPerso){
	bool_t caseAccessible = TRUE;
	int sens = -1;
	if(direction == SUD || direction == EST)
		sens = 1;

	if(direction == NORD || direction == SUD){
		if(estBord(persos[indice]->positionMapY, nbLigne,sens) == TRUE){
			caseAccessible = FALSE;
		}else {
			
			int degat = estEnnemi(persos[indice]->positionMapX, (persos[indice]->positionMapY)+sens, grille, persos, nbPerso);
			if( degat >= 0){
				persos[indice]->vieCourante -= degat;
				caseAccessible = FALSE;
			}else{
				if(estCoffre( persos[indice]->positionMapX,  (persos[indice]->positionMapY)+sens, grille) == TRUE)
				{
					if (persos[indice]->type == PLAYER)
					{
						setAttaque(persos[indice], 2);
						grille[persos[indice]->positionMapY+sens][persos[indice]->positionMapX] = SABLE;
						seDeplacer(persos[indice], direction);
					}
					else {
						caseAccessible = FALSE;
					}
					
				}
				else
				{
					seDeplacer(persos[indice], direction);
				}
			}
		}
	} else {
		if(estBord(persos[indice]->positionMapX, nbColonne, sens) == TRUE){
			caseAccessible = FALSE;
		}else {
			int degat = estEnnemi(persos[indice]->positionMapX + sens, persos[indice]->positionMapY, grille, persos, nbPerso);
			if( degat >= 0){
				persos[indice]->vieCourante -= degat;
				caseAccessible = FALSE;
			}else{
				if(estCoffre(persos[indice]->positionMapX+sens, persos[indice]->positionMapY, grille) == TRUE )
				{	
					if (persos[indice]->type == PLAYER)
					{
						setAttaque(persos[indice], 2);
						grille[persos[indice]->positionMapY][persos[indice]->positionMapX+sens] = SABLE;
						seDeplacer(persos[indice], direction);
					}
					else {
						caseAccessible = FALSE;
					}
					
				}
				else
				{
					seDeplacer(persos[indice], direction);
				}
			}
		}
	}

	persos[indice]->regard = direction;
	return caseAccessible;
}
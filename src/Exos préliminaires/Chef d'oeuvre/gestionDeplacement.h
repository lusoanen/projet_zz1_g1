#ifndef _GESTION_D_H_
#define _GESTION_D_H_

#include <stdio.h>
#include <stdlib.h>
#include "personnage.h"
#include "bool.h"
#include "fond.h"
#include "direction.h"

/**
 * @brief verifie si il y a un ennemie sur la case que l'on souhaite atteindre 
 * 
 * @param x indice X de là où l'on souhaite aller sur la map
 * @param y indice Y de là où l'on souhaite aller sur la map
 * @param grille grille 2D représentant la map en mémoire
 * @param perso tableau des coordonnées de tous les personnages du jeu
 * @param max nombre de personnage dans le jeu
 * @return dégats infligés par l'ennemi
 */
int estEnnemi(int x, int y, fond_t ** grille, personnage_t ** perso, int max);

/**
 * @brief verifie sur là où l'on souhaite aller sur la map est un bord ou non
 * 
 * @param pos indice X (ou Y) dans la map
 * @param max nombre de colonne (ou ligne) dans la map
 * @param sens definit si le sens du déplacement (-1 ou 1)
 * @return est un bord ou non
 */
bool_t estBord(int pos, int max, int sens);

/**
 * @brief deplace le personnage si cela est possible
 * 
 * @param indice 
 * @param grille 
 * @param nbLigne 
 * @param nbColonne 
 * @param direction 
 * @param persos 
 * @param nbPerso 
 * @return bool_t 
 */
bool_t deplacerPersonnage(int indice, fond_t ** grille, int nbLigne, int nbColonne, direction_t direction, personnage_t ** persos, int nbPerso);

#endif
#include <stdio.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include "fond.h"
#include "baseSDL.h"
#include "bool.h"
#include "animation.h"
#include "personnage.h"
#include "gestionDeplacement.h"
#include "chargementJeu.h"

int main()
{
	srand(time(0));

	SDL_Window * fenetre = NULL;
	SDL_Renderer * renderer = NULL; 
	SDL_DisplayMode ecran;

	bool_t continuer = TRUE;
	SDL_Event event;

	int nbPersonnages =4,i, pause = 0;
	
	int nbLigne, nbColonne;
	fond_t ** grille = chargerMap(&nbLigne, &nbColonne);
	if (grille != NULL)
	{
		Init(&fenetre, &renderer, &ecran);
		
		//creation d'entites
		personnage_t *player =initPlayer();
		//regroupement dans un tableau de personnages avec le joueur en 1 pour l'instant
		personnage_t ** listePersos = (personnage_t **)malloc(nbPersonnages*sizeof(personnage_t));
		if (listePersos == NULL)
		{
			for (int i=0; i < nbLigne; i++)
			{
				free(grille[i]);
			}
			free(grille);
			exit(EXIT_FAILURE);
		}
		listePersos[0] = player;
		int * mouvValide = (int *) malloc(nbPersonnages*sizeof(int));
		if (mouvValide == NULL)
		{
			for (int i=0; i < nbLigne; i++)
			{
				free(grille[i]);
			}
			free(grille);
			free(listePersos);
			exit(EXIT_FAILURE);
		}
		mouvValide[0] = 0;
		int quart_x = nbColonne/4, quart_y = nbLigne/4;
		for(i =1;i<nbPersonnages;i++)
		{
			listePersos[i]=initMonstre();
			int x,y;
			do
			{
				x = i*quart_x + (rand()%quart_x);
				y = i*quart_y + (rand()%quart_y);
			} while (grille[y][x] != SABLE); 
			setPosition(listePersos[i], x, y, nbColonne-1, nbLigne-1);
			listePersos[i]->regard = NORD;
			mouvValide[i] = 0;
		}

		SDL_Texture * joueur_Text = IMG_LoadTexture(renderer,"asset/personnages/fille.png");
		SDL_Texture * monstre_Text= IMG_LoadTexture(renderer,"asset/personnages/monstre.png");
		SDL_Texture ** tab_texture = (SDL_Texture **) malloc(NB_TEXT*sizeof(SDL_Texture *));
		if (tab_texture != NULL && joueur_Text != NULL && monstre_Text != NULL)
		{
			charge_img_fond(fenetre, renderer, tab_texture);
			Affiche_Game(fenetre,renderer,&ecran,grille,nbColonne, nbLigne,listePersos,nbPersonnages, mouvValide,joueur_Text,monstre_Text,tab_texture);

			while (continuer)
			{
				while (continuer && SDL_PollEvent(&event))
				{
					switch (event.type)
					{
						case SDL_QUIT:
						continuer = FALSE;
						break;
						case SDL_KEYDOWN:
						switch (event.key.keysym.sym)
						{
							case SDLK_SPACE :
							pause = !pause;
							break;

							case SDLK_ESCAPE:
							continuer = FALSE;
							break;

							case SDLK_UP:
							mouvValide[0]= deplacerPersonnage(0,grille, nbLigne, nbColonne,NORD,listePersos,nbPersonnages);
							for(i = 1 ;i<nbPersonnages;i++)
								mouvValide[i]= deplacerPersonnage(i,grille, nbLigne,nbColonne,SUD,listePersos,nbPersonnages);

							Affiche_Game(fenetre,renderer,&ecran,grille,nbColonne, nbLigne,listePersos,nbPersonnages, mouvValide,joueur_Text,monstre_Text,tab_texture);
							break;

							case SDLK_DOWN:
							mouvValide[0]= deplacerPersonnage(0,grille, nbLigne,nbColonne,SUD,listePersos,nbPersonnages);
							for(i = 1 ;i<nbPersonnages;i++)
								mouvValide[i]= deplacerPersonnage(i,grille, nbLigne,nbColonne,NORD,listePersos,nbPersonnages);

							Affiche_Game(fenetre,renderer,&ecran,grille,nbColonne, nbLigne,listePersos,nbPersonnages, mouvValide,joueur_Text,monstre_Text,tab_texture);
							break;

							case SDLK_LEFT:
							mouvValide[0]= deplacerPersonnage(0,grille, nbLigne,nbColonne,OUEST,listePersos,nbPersonnages);
							for(i = 1 ;i<nbPersonnages;i++)
								mouvValide[i]= deplacerPersonnage(i,grille, nbLigne,nbColonne,EST,listePersos,nbPersonnages);

							Affiche_Game(fenetre,renderer,&ecran,grille,nbColonne, nbLigne,listePersos,nbPersonnages, mouvValide,joueur_Text,monstre_Text,tab_texture);
							break;

							case SDLK_RIGHT:
							mouvValide[0]= deplacerPersonnage(0,grille, nbLigne,nbColonne,EST,listePersos,nbPersonnages);
							for(i = 1 ;i<nbPersonnages;i++)
								mouvValide[i]= deplacerPersonnage(i,grille, nbLigne,nbColonne,OUEST,listePersos,nbPersonnages);

							Affiche_Game(fenetre,renderer,&ecran,grille,nbColonne, nbLigne,listePersos,nbPersonnages, mouvValide,joueur_Text,monstre_Text,tab_texture);
							break;
							default:
							break;
						}
						break;
						default:
			    			
						break;
						SDL_Delay(250);
					}
					// if(!pause){
					// }
					SDL_FlushEvent(SDL_KEYDOWN);
				}
			}
			SDL_DestroyTexture(joueur_Text);
			SDL_DestroyTexture(monstre_Text);
			free_tab_texture(tab_texture);
			
		}
		free(mouvValide);
		for(i =0;i<nbPersonnages;i++){
			libererPerso(listePersos[i]);
			free(grille[i]);
		}
		free(listePersos);
		free(grille);
		fermetureSdl(1, "", fenetre, renderer);
	}

	return 0;
}
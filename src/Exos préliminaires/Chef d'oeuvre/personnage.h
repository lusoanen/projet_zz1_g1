#ifndef _PERSO_H_
#define _PERSO_H_

#include <stdio.h>
#include <stdlib.h>
#include "direction.h"
#include "typePerso.h"
#include "bool.h"

typedef struct personnage{
	int vieMax;
	int vieCourante;
	int positionMapX;
	int positionMapY;
	direction_t regard;
	int attaque;
	typePerso_t type;
}personnage_t;


personnage_t * initPlayer();
personnage_t * initMonstre();

int getVieMax(personnage_t * perso);
int getVieCourante(personnage_t * perso);
void setVieCourante(personnage_t * perso, int nouvelleVie);
bool_t estVivant(personnage_t * perso);

bool_t subirAttaque(personnage_t * perso, int attaque);

void getPosition(personnage_t * perso, int * x, int * y);
void setPosition(personnage_t * perso, int nouvX, int nouvY, int maxGrilleX, int maxGrilleY);
void seDeplacer(personnage_t * perso, direction_t direction);

int getAttaque(personnage_t * perso);
void setAttaque(personnage_t * perso, int bonus);
typePerso_t getType(personnage_t * perso);

void libererPerso(personnage_t * perso);


#endif
#include "jeu_de_la_vie.h"

int regles[2][9] = {{0,0,0,1,0,0,0,0,0},{0,0,1,1,0,0,0,0,0}}; // regles[0] -> regles de naissance, regles[1] -> regles de survie

int ** init_tab(int nb_ligne, int nb_colonne) 
{
	int ** tab = (int **) malloc(nb_ligne*sizeof(int *));
	if (tab != NULL)
	{
		for (int i=0; i < nb_ligne; i++)
		{
			tab[i] = (int*) malloc(nb_colonne*sizeof(int));
			if (tab[i] == NULL)
			{
				fprintf(stderr, "Erreur lors de l'allocation du tableau\n");
				for (i=i-1; i >= 0; i--)
				{
					free(tab[i]);
				}
				free(tab);
				tab = NULL;
				exit(EXIT_FAILURE);
			}
		}
	}
	else
	{
		fprintf(stderr, "Erreur lors de l'allocation du tableau\n");
		tab = NULL;
		exit(EXIT_FAILURE);
	}

	return tab;
}

void free_tab(int ** tab, int nb_ligne)
{
	for (int i=0; i < nb_ligne; i++)
	{
		free(tab[i]);
	}
	free(tab);
}

void copie_tab(int ** src, int ** dest, int nb_ligne,int nb_colonne)
{
	for (int i=0; i < nb_ligne; i++)
	{
		for (int j=0; j < nb_colonne; j++) 
		{
			dest[i][j] = src[i][j];
		}
	}
}

void affiche_tab(int ** tab, int nb_ligne, int nb_colonne)
{
	for (int i=0; i < nb_ligne; i++)
	{
		for (int j=0; j < nb_colonne; j++) 
		{
			printf("%d ", tab[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void gen_tab(int ** tab, int nb_ligne, int nb_colonne)
{
	for (int i=0; i < nb_ligne; i++)
	{
		for (int j=0; j < nb_colonne; j++)
		{
			tab[i][j] = 0;
		}
	}

	int quart_x = (nb_colonne/4)-1;
	int quart_y = (nb_ligne/4)-1;

	// int milieu_x = (nb_colonne/2)-1;
	// int milieu_y = (nb_ligne/2)-1;

	int trois_quart_x = (3*nb_colonne/4)-1;
	int trois_quart_y = (3*nb_ligne/4)-1;

	// tab[milieu_y][milieu_x] = 1;
	// tab[milieu_y][milieu_x-1] = 1;
	// tab[milieu_y+1][milieu_x] = 1;
	// tab[milieu_y-1][milieu_x] = 1;
	// tab[milieu_y+1][milieu_x+1] = 1;
	
	tab[quart_y][quart_x] = 1;
	tab[quart_y][quart_x+1] = 1;
	tab[quart_y][quart_x+2] = 1;
	tab[quart_y][quart_x-1] = 1;
	tab[quart_y-1][quart_x-1] = 1;
	tab[quart_y+1][quart_x+2] = 1;

	tab[trois_quart_y][trois_quart_x] = 1;
	tab[trois_quart_y][trois_quart_x+1] = 1;
	tab[trois_quart_y][trois_quart_x+2] = 1;
	tab[trois_quart_y][trois_quart_x-1] = 1;
	tab[trois_quart_y-1][trois_quart_x-1] = 1;
	tab[trois_quart_y+1][trois_quart_x+2] = 1;
}

int nb_voisins(int ** tab, int nb_ligne, int nb_colonne, int x, int y)
{
	int nb = 0 - tab[y][x];

	for (int i=-1; i<=1; i++)
	{
		for (int j=-1; j<=1; j++)
		{
			nb += tab[(y+i+nb_ligne)%nb_ligne][(x+j+nb_colonne)%nb_colonne];
		}
	}

	return nb;
}

int maj_situation(int ** tab_prec, int ** tab, int nb_ligne, int nb_colonne) 
{
	int nb_vivants = 0;
	copie_tab(tab, tab_prec, nb_ligne, nb_colonne);

	for (int i=0; i < nb_ligne; i++) 
	{
		for (int j=0; j < nb_colonne; j++)
		{
			if (regles[tab_prec[i][j]][nb_voisins(tab_prec, nb_ligne, nb_colonne, j, i)])
			{
				tab[i][j] = 1;
				nb_vivants++;
			}
			else
			{
				tab[i][j] = 0;
			}
		}
	}

	return nb_vivants;
}
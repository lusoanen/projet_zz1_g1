/**
 * \file jeu_de_la_vie.h
 * \brief Moteur de jeu du <A HREF="https://www.youtube.com/watch?v=S-W0NX97DB0">jeu de la vie</A> 
 */


#ifndef _JEU_VIE_H_
#define _JEU_VIE_H_

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

#define GRILLE_L 50 /**< Nombre de cases en largeur de la grille de jeu*/
#define GRILLE_H 30 /**< Nombre de cases en hauteur de la grille de jeu*/

/**
 * \fn int ** init_tab ()
 * \brief Fonction d'allocation d'un tableau d'entiers 2D dynamique
 * 
 * \param  nb_ligne   Nombre de ligne du tableau
 * \param  nb_colonne Nombre de colonne du tableau
 * 
 * \return            Tableau alloué ou NULL si échec
 */
int ** init_tab(int nb_ligne, int nb_colonne);

/**
 * \fn void free_tab (int ** tab, int nb_ligne)
 * \brief Libère un tableau d'entiers 2D dynamique
 * 
 * \param tab      Tableau à libérer
 * \param nb_ligne Nombre de ligne du tableau à libérer
 */
void free_tab(int ** tab, int nb_ligne);

/**
 * \fn void copie_tab(int ** src, int ** dest, int nb_ligne,int nb_colonne)
 * \brief Copie le contenu d'un tableau d'entiers dans un autre tableau d'entiers de même dimensions
 * 
 * \param src        Tableau à copier
 * \param dest       Tableau dans lequel copier
 * \param nb_ligne   Nombre de lignes des tableaux
 * \param nb_colonne Nombre de colonnes des tableaux
 */
void copie_tab(int ** src, int ** dest, int nb_ligne,int nb_colonne);

/**
 * \fn void affiche_tab(int ** tab, int nb_ligne, int nb_colonne)
 * \brief Affiche le contenu d'un tableau d'entiers dans le terminal
 * 
 * \param tab        Tableau à afficher
 * \param nb_ligne   Nombre de lignes du tableau
 * \param nb_colonne Nombre de colonnes du tableau
 */
void affiche_tab(int ** tab, int nb_ligne, int nb_colonne);

/**
 * \fn void gen_tab(int ** tab, int nb_ligne, int nb_colonne)
 * \brief Fonction de remplissage d'un tableau avec la situation de base du jeu de la vie (0 -> mort, 1 ->vivant)
 * 
 * \param tab        Tableau à remplir
 * \param nb_ligne   Nombre de lignes du tableau
 * \param nb_colonne Nombre de colonnes du tableau
 */
void gen_tab(int ** tab, int nb_ligne, int nb_colonne);

/**
 * \fn int nb_voisins(int ** tab, int nb_ligne, int nb_colonne, int x, int y)
 * \brief Fonction comptant le nombre de voisins vivants d'une case donnée
 * 
 * \param  tab        Tableau à évaluer
 * \param  nb_ligne   Nombre de lignes du tableau
 * \param  nb_colonne Nombre de colonnes du tableau
 * \param  x          Abscisse de la case dont on cherche le nombre de voisins
 * \param  y          Ordonné de la case dont on cherche le nombre de voisins
 * 
 * \return            Nombre de voisins vivants de la case de coordonnées (x,y)
 */
int nb_voisins(int ** tab, int nb_ligne, int nb_colonne, int x, int y);

/**
 * \fn int maj_situation(int ** tab_prec, int ** tab, int nb_ligne, int nb_colonne)
 * \brief Fonction faisant naître, survivre et mourir les cellules en fonction des règles utilisées
 * 
 * \param  tab_prec   Tableau permettant de sauvegarder la situation avant modifications
 * \param  tab        Tableau dans lequel on fait les modifications
 * \param  nb_ligne   Nombre de lignes du tableau
 * \param  nb_colonne Nombre de colonnes du tableau
 * 
 * \return            Nombre de cellules vivantes après la mise à jour de la situation
 */
int maj_situation(int ** tab_prec, int ** tab, int nb_ligne, int nb_colonne);

#endif
#include "vie_graph.h"

int r[2] = {255,0};
int g[2] = {255,0};
int b[2] = {255,0};
int a[2] = {255,255};

void jeu_de_la_vie() {

  SDL_Window * fenetre = NULL;
  SDL_Renderer * renderer = NULL;

  int largeur = LARGEUR_DEFAUT, hauteur = HAUTEUR_DEFAUT;
  int continuer = 1, pause = 0, nb_vivants = 0;
  
  // Initialisation de la SDL  + gestion de l'échec possible 
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
    exit(EXIT_FAILURE);
  }
  else 
  {
    // Récupération de la taille de l'écran 

    int i = 0;

    SDL_DisplayMode ecran;

    int code_retour = SDL_GetCurrentDisplayMode(i, &ecran);

    if(code_retour != 0) 
    {
      SDL_Log("Impossible de récupérer le mode d'affichage pour l'affichage vidéo #%d : %s", i, SDL_GetError());

    }
    else 
    {
      SDL_Log("Afichage #%d: mode d'affichage courrant %dx%dpx @ %dhz.", i, ecran.w, ecran.h, ecran.refresh_rate);
      largeur = ecran.w;
      hauteur = ecran.h;
    }
    if (SDL_CreateWindowAndRenderer(largeur, hauteur, SDL_WINDOW_MAXIMIZED, &fenetre, &renderer) < 0) 
    {
      SDL_Log("Erreur d'initialisation de la fenêtre et du renderer : %s\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }
    else 
    {
      SDL_SetWindowTitle(fenetre, "Jeu de la vie");
      SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); 
      SDL_RenderSetLogicalSize(renderer, largeur, hauteur);
      SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      SDL_RenderClear(renderer);
      SDL_RenderPresent(renderer);
      SDL_UpdateWindowSurface(fenetre);

      SDL_Delay( 1000 );

      int ** tab = init_tab(GRILLE_H, GRILLE_L);
      if (tab != NULL) 
      {
        int ** tab_prec = init_tab(GRILLE_H, GRILLE_L);
        if (tab_prec != NULL)
        {

          SDL_Event event;
          coordonnees_t souris = {0,0};
          coordonnees_t i_j = {0,0};

          gen_tab(tab, GRILLE_H, GRILLE_L);
          affiche_ecran(renderer, tab, GRILLE_H, GRILLE_L, largeur, hauteur);

          while (continuer)
          { 
            while (SDL_PollEvent(&event))
            {
              switch (event.type) 
              {
                case SDL_QUIT :
                  continuer = 0;
                  break;
                case SDL_KEYDOWN :
                  switch (event.key.keysym.sym)
                  {
                    case SDLK_SPACE :
                      pause = !pause;
                      break;
                    case SDLK_ESCAPE :
                      continuer = 0;
                      break;
                    case SDLK_q :
                      continuer = 0;
                      break;
                    default :
                      break;
                  }
                  break;
                case SDL_MOUSEBUTTONDOWN :
                  souris.x = event.button.x;
                  souris.y = event.button.y;
                  if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) 
                  {
                    i_j = pixel_a_case(souris, GRILLE_H, GRILLE_L, largeur, hauteur);
                    if (i_j.x >= 0 && i_j.x < GRILLE_L && i_j.y >= 0 && i_j.y < GRILLE_H)
                    {
                      tab[i_j.y][i_j.x] = 1;
                      affiche_ecran(renderer, tab, GRILLE_H, GRILLE_L, largeur, hauteur);
                      SDL_Delay(250);
                    }
                  }
                  else if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
                  {
                    i_j = pixel_a_case(souris, GRILLE_H, GRILLE_L, largeur, hauteur);
                    if (i_j.x >= 0 && i_j.x < GRILLE_L && i_j.y >= 0 && i_j.y < GRILLE_H)
                    {
                      tab[i_j.y][i_j.x] = 0;
                      affiche_ecran(renderer, tab, GRILLE_H, GRILLE_L, largeur, hauteur);
                      SDL_Delay(250);
                    }
                  }
                  break;
                default :
                break;
              }
            }

            if (!pause)
            { 
              // SDL_GetWindowSize(fenetre, &largeur, &hauteur);
              nb_vivants = maj_situation(tab_prec, tab, GRILLE_H, GRILLE_L);
              affiche_ecran(renderer, tab, GRILLE_H, GRILLE_L, largeur, hauteur);

              if(nb_vivants == 0)
              {
                affiche_ecran(renderer, tab, GRILLE_H, GRILLE_L, largeur, hauteur);
                printf("Toutes les cellules sont mortes...\n");
                continuer = 0;
              }
              SDL_Delay(250);
            }
            SDL_Delay(50);
          }

          free_tab(tab_prec, GRILLE_H);
        }

        free_tab(tab, GRILLE_H);
      }
    }
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fenetre);

    SDL_Quit();
  }
}

void affiche_ecran( SDL_Renderer * renderer, int ** tab, int nb_ligne, int nb_colonne, int largeur, int hauteur)
{
  SDL_Rect rect; 

  SDL_SetRenderDrawColor(renderer, r[0], b[0], g[0], a[0]);
  SDL_RenderClear(renderer);

  rect.w = largeur/nb_colonne;
  rect.h = hauteur/nb_ligne;

  for (int i=0; i < nb_ligne; i++)
  {
    for (int j=0; j < nb_colonne; j++)
    {
      rect.x = j*(rect.w);
      rect.y = i*(rect.h);
      int c = tab[i][j];
      SDL_SetRenderDrawColor(renderer, r[c], g[c], b[c], a[c]);
      SDL_RenderFillRect(renderer,&rect);
    }
  }

  SDL_RenderPresent(renderer);
}

coordonnees_t pixel_a_case(coordonnees_t pixels, int nb_ligne, int nb_colonne, int largeur, int hauteur)
{
  int hauteur_case = hauteur/nb_ligne;
  int largeur_case = largeur/nb_colonne;
  coordonnees_t res = {pixels.x/largeur_case,pixels.y/hauteur_case};
  return res;
}
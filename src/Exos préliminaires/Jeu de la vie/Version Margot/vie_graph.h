/**
 * \file vie_graph.h
 * \brief Moteur graphique du <A HREF="https://www.youtube.com/watch?v=S-W0NX97DB0">jeu de la vie</A> 
 */

#ifndef _VIE_GRAPH_H_
#define _VIE_GRAPH_H_

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "jeu_de_la_vie.h"

#define HAUTEUR_DEFAUT 480 
#define LARGEUR_DEFAUT 640

typedef struct coordonnees
{
	int x; /**< Abscisse */
	int y; /**< Ordonnée */
}coordonnees_t;

/**
 * \fn void jeu_de_la_vie()
 * \brief Boucle de jeu contenant l'initialisation graphique
 */
void jeu_de_la_vie();

/**
 * \fn void affiche_ecran( SDL_Renderer * renderer, int ** tab, int nb_ligne, int nb_colonne, int largeur, int hauteur)
 * \brief Fonction d'affichage graphique de la grille de jeu
 * 
 * \param renderer   Renderer sur lequel dessiner
 * \param tab        Tableau à afficher
 * \param nb_ligne   Nombre de lignes du tableau
 * \param nb_colonne Nombre de colonnes du tableau
 * \param largeur    Largeur de la fenêtre en pixels
 * \param hauteur    Hauteur de la fenêtre en pixels
 */
void affiche_ecran( SDL_Renderer * renderer, int ** tab, int nb_ligne, int nb_colonne, int largeur, int hauteur);

/**
 * \fn coordonnees_t pixel_a_case(coordonnees_t pixels, int nb_ligne, int nb_colonne, int largeur, int hauteur)
 * \brief Fonction de conversion des coordonnées en pixels en coordonnées dans le tableau
 * 
 * \param  pixels     Coordonnées en pixels
 * \param  nb_ligne   Nombre de lignes de la grille
 * \param  nb_colonne Nombre de colonnes de la grille
 * \param largeur     Largeur de la fenêtre en pixels
 * \param hauteur     Hauteur de la fenêtre en pixels
 * 
 * \return            Les coordonnées converties
 */
coordonnees_t pixel_a_case(coordonnees_t pixels, int nb_ligne, int nb_colonne, int largeur, int hauteur);

#endif
#include "jeuDeLaVie.h"


void initGrille(int grille[LARGEUR][HAUTEUR]){
	grille[10][11] = 1;
	grille[12][11] = 1;
	grille[11][11] = 1;
	grille[20][20] = 1;
	grille[20][21] = 1;
	grille[21][20] = 1;
	grille[21][19] = 1;

}

void afficherGrilleDebug(int grille[LARGEUR][HAUTEUR]){
	for(int i = 0; i < LARGEUR; i++){
		for(int j = 0; j < HAUTEUR; j++){
			printf("%d ", grille[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int compterVoisin(int a, int b, int grille[LARGEUR][HAUTEUR]){
	int nb = 0 - grille[a][b];

	for (int i=-1; i<=1; i++){
		for (int j=-1; j<=1; j++){
			nb += grille[(a+i+LARGEUR)%LARGEUR][(b+j+HAUTEUR)%HAUTEUR];
		}
	}

	return nb;
}

int actualiserVie(int grille[LARGEUR][HAUTEUR]){
	int nbVoisin;
	int tmp[LARGEUR][HAUTEUR] = {0};
	int i, j;
	int nbSurvivant = 0;

	for(i = 0; i < LARGEUR; i++){
		for(j = 0; j < HAUTEUR; j++){
			nbVoisin = compterVoisin(i, j, grille);
			if(grille[i][j] == 0 ){
				if(nbVoisin == 3){
					tmp[i][j] = 1;
					nbSurvivant += 1;
				}
			}
			else {
				if(nbVoisin == 3 || nbVoisin == 2){
					tmp[i][j] = 1;
					nbSurvivant += 1;
				}
			}
		}
	}
	for(i = 0; i < LARGEUR; i++){
		for(j = 0; j < HAUTEUR; j++){
			grille[i][j] = tmp[i][j];
		}
	}

	return nbSurvivant;
}




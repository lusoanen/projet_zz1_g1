#ifndef _JEU_VIE_H_
#define _JEU_VIE_H_

#include <stdlib.h>
#include <stdio.h>

#define LARGEUR 25 /**< largeur de la grille */
#define HAUTEUR 25 /**< hauteur de la grille */

/**
 * @fn void initGrille(int grille[LARGEUR][HAUTEUR])
 * @brief initialise la grille avec un motif prédéfinit en dur
 * 
 * @param grille grille des cellules
 */
void initGrille(int grille[LARGEUR][HAUTEUR]);

/**
 * @fn void afficherGrilleDebug(int grille[LARGEUR][HAUTEUR])
 * @brief affiche la grille dans lle terminal
 * 
 * @param grille grille des cellules
 */
void afficherGrilleDebug(int grille[LARGEUR][HAUTEUR]);

/**
 * @fn int compterVoisin(int a, int b, int grille[LARGEUR][HAUTEUR])
 * @brief compte le nombre de voisin d'une cellule dans la grille
 * 
 * @param a coordonnée de la colonne de la cellule observée
 * @param b coordonnée de la ligne de la cellule observée
 * @param grille grille des cellules
 * @return nombre de voisin de la cellule 
 */
int compterVoisin(int a, int b, int grille[LARGEUR][HAUTEUR]);

/**
 * @fn int actualiserVie(int grille[LARGEUR][HAUTEUR])
 * @brief calcule la prochaine génération de cellule
 * 
 * @param grille grille des cellules
 * @return le nombre de cellules vivantes de la nouvelle génération
 */
int actualiserVie(int grille[LARGEUR][HAUTEUR]);

#endif
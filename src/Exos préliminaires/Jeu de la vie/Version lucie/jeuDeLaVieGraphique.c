#include "jeuDeLaVieGraphique.h"

void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu) {                 
    char messageParDefaut[255];                                         
    int l;                                                          

    if (!ok) {                                                      
           strncpy(messageParDefaut, message, 250);                                 
           l = strlen(messageParDefaut);                                        
           strcpy(messageParDefaut + l, " : %s\n");                     

           SDL_Log(messageParDefaut, SDL_GetError());                   
    }                                                               

    if (rendu != NULL) SDL_DestroyRenderer(rendu);                            
    if (fenetre != NULL)   SDL_DestroyWindow(fenetre);                                        

    SDL_Quit();                                                     

    if (!ok) {                                                      
           exit(EXIT_FAILURE);                                              
    }                                                               
}   

void dessinerRectangle(int i, int j, SDL_Renderer* rendu, int largeur, int hauteur, int eventCouleur){
  SDL_SetRenderDrawColor(rendu, 0+eventCouleur , 0+eventCouleur, 0+eventCouleur, 255);

  SDL_Rect rectangle;
  rectangle.x = i*largeur;
  rectangle.y = j*hauteur;
  rectangle.w = largeur;
  rectangle.h = hauteur;

  SDL_RenderFillRect(rendu, &rectangle);
}

void afficherGrilleSDL(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu, int eventCouleur, int couleurX, int couleurY){
  SDL_SetRenderDrawColor(rendu, 0, 255, 255, 255);

  int largeurRectangle = LARGEUR_WINDOW/LARGEUR;
  int hauteurRectangle = HAUTEUR_WINDOW/HAUTEUR;

  for(int i = 0; i < LARGEUR; i++){
    for(int j = 0; j < HAUTEUR; j++){
      if(grille[i][j] == 1){
        dessinerRectangle(i,j,rendu, largeurRectangle, hauteurRectangle, 0);
      }
      else{
        if(i == couleurX && j == couleurY){
          dessinerRectangle(i,j,rendu,largeurRectangle,hauteurRectangle, eventCouleur);
        }
      }
    }
  }
  

  SDL_RenderPresent(rendu);
  SDL_Delay(200);
  SDL_SetRenderDrawColor(rendu, 0 , 255, 255, 255); 
  SDL_RenderClear(rendu);
}

void jeu(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu){

  initGrille(grille);

  int vivant = 1, changementCouleurFond = 0;
  int couleurX = -1;
  int couleurY = -1;

  SDL_bool enCours = SDL_TRUE, pause = SDL_FALSE;                             
  while (enCours) {                        
    SDL_Event event;                              

    while (enCours && SDL_PollEvent(&event)) {   
                                            
      switch (event.type) {                         
        case SDL_QUIT:                                
          enCours = SDL_FALSE;                     
          break;
        case SDL_KEYDOWN:                                
          switch (event.key.keysym.sym) {             
            case SDLK_SPACE:                           
              pause = !pause;
              break;
            case SDLK_ESCAPE:                                                          
              enCours = SDL_FALSE;                                                               
              break;
            default:                                
              break;
          }
          break;
        case SDL_MOUSEBUTTONDOWN: 
          if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT) ) {         
            changementCouleurFond = (changementCouleurFond + 20)%254; 
            couleurX = event.button.x;
            couleurY = event.button.y;   
            afficherGrilleSDL(grille, rendu, changementCouleurFond, couleurX*LARGEUR/LARGEUR_WINDOW, couleurY*HAUTEUR/HAUTEUR_WINDOW);      
          }                      
          break;
        default:                                      
          break;
      }
    }
    
    if (!pause) {                                  
      if(vivant != 0){
        if(couleurX != -1 && couleurY != -1)
          afficherGrilleSDL(grille, rendu, changementCouleurFond, couleurX*LARGEUR/LARGEUR_WINDOW, couleurY*HAUTEUR/HAUTEUR_WINDOW);
        else
          afficherGrilleSDL(grille, rendu, changementCouleurFond, couleurX, couleurY);
        vivant = actualiserVie(grille);  
      }
      couleurX = -1;
      couleurY = -1;
    }
    SDL_Delay(50);                                  
   }


  
} 

int mainJeuDeLaVie(){
    SDL_Window* fenetre = NULL;
    SDL_Renderer* rendu = NULL;

    SDL_DisplayMode ecran;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
      fermetureSdl(0, "ERROR SDL INIT", fenetre, rendu);

    SDL_GetCurrentDisplayMode(0, &ecran);


    fenetre = SDL_CreateWindow("Premier dessin",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, LARGEUR_WINDOW,
                                HAUTEUR_WINDOW,
                                SDL_WINDOW_OPENGL);
    if (fenetre == NULL) 
      fermetureSdl(0, "ERROR WINDOW CREATION", fenetre, rendu);


    rendu = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (rendu == NULL) 
        fermetureSdl(0, "ERROR RENDERER CREATION", fenetre, rendu);

    int grille[LARGEUR][HAUTEUR] = {0};
    jeu(grille, rendu);                                                  

    fermetureSdl(1, "Normal ending", fenetre, rendu);

    return EXIT_SUCCESS;
}
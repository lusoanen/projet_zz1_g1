#ifndef _GRAPH_H_
#define _GRAPH_H_

#define LARGEUR_WINDOW 500 /**< largeur de la fenetre */
#define HAUTEUR_WINDOW 500 /**< hauteur de la fenetre */
#include <SDL2/SDL.h>

#include "jeuDeLaVie.h"


/**
 * @fn void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu)
 * @brief fermeture des éléments de SDL (fenetre et renderer)
 * 
 * @param ok validation de la fermeture normal de la SDL
 * @param message message de fin normal ou message d'erreur si besoin
 * @param fenetre à fermer
 * @param rendu à fermer
 */
void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);

/**
 * @fn void dessinerRectangle(int i, int j, SDL_Renderer* rendu, int largeur, int hauteur, int eventCouleur)
 * @brief dessine un rectangle dans le renderer
 * 
 * @param i position celon l'axe x
 * @param j position celon l'axe y
 * @param rendu SDL_renderer
 * @param largeur largeur du rectangle
 * @param hauteur hauteur du rectangle
 * @param eventCouleur indique une couleur non standard suite à un événement (vaut 0 sinon)
 */
void dessinerRectangle(int i, int j, SDL_Renderer* rendu, int largeur, int hauteur, int eventCouleur);

/**
 * @fn void afficherGrilleSDL(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu, int eventCouleur, int couleurX, int couleurY)
 * @brief affiche la grille des cellules de manière graphique
 * 
 * @param grille grille des cellules
 * @param rendu SDL_renderer
 * @param eventCouleur indique une couleur non standard suite à un événement (vaut -1 sinon)
 * @param couleurX indique la position celon l'axe X de là ou la couleur doit changer
 * @param couleurY indique la position celon l'axe Y de là ou la couleur doit changer
 */
void afficherGrilleSDL(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu, int eventCouleur, int couleurX, int couleurY);

/**
 * @fn void jeu(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu)
 * @brief lance le jeu de la vie
 * 
 * @param grille grille des cellules
 * @param rendu SDL_renderer
 */
void jeu(int grille[LARGEUR][HAUTEUR], SDL_Renderer *rendu);

/**
 * @fn int mainJeuDeLaVie()
 * @brief point d'entrée du jeu de la vie et initialsation des outils SDL
 * 
 * @return erreur si besoin 
 */
int mainJeuDeLaVie();

#endif


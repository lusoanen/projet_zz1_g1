#include"GOL_SDL.h"

void afficheGame(int ** tab,int taille,SDL_DisplayMode * ecran,SDL_Renderer * rendu)
{
  SDL_Rect carre;
  carre.w = ecran->w/taille;
  carre.h = ecran->h/taille;
  for(int i = 0;i<taille; i++)
    {
      carre.x = i*carre.w;
      for(int j=0; j<taille; j++)
	{
	  carre.y =j* carre.h;
	  if(tab[i][j])
	    {
	      SDL_SetRenderDrawColor(rendu,255,255,255,0);
	      SDL_RenderFillRect(rendu,&carre);
	    }
	    else{  
	      SDL_SetRenderDrawColor(rendu,0,0,0,0);
	      SDL_RenderFillRect(rendu,&carre);
	      //printf("\033[1;41m %d \033[0;0m",curr);
	      

	    }
	}
    }
  SDL_RenderPresent(rendu);
  SDL_Delay(100);
  SDL_RenderClear(rendu);
}

void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran){
    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
		fermetureSdl(0, "ERROR SDL INIT", *fenetre, *rendu);

	SDL_GetCurrentDisplayMode(0, &(*ecran));


	*fenetre = SDL_CreateWindow("Premier dessin",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, ecran->w * 0.66,ecran->h * 0.66, SDL_WINDOW_OPENGL);
	if (*fenetre == NULL) 
		fermetureSdl(0, "ERROR WINDOW CREATION", *fenetre, *rendu);


	*rendu = SDL_CreateRenderer(*fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (*rendu == NULL) 
	  	fermetureSdl(0, "ERROR RENDERER CREATION", *fenetre, *rendu);


}


void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu) {                 
  char messageParDefaut[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(messageParDefaut, message, 250);                                 
         l = strlen(messageParDefaut);                                        
         strcpy(messageParDefaut + l, " : %s\n");                     

         SDL_Log(messageParDefaut, SDL_GetError());                   
  }                                                               

  if (rendu != NULL) SDL_DestroyRenderer(rendu);                            
  if (fenetre != NULL)   SDL_DestroyWindow(fenetre);                                     
  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}   


void getCase(int ** tab,int taille,int mouseX,int mouseY,SDL_DisplayMode * ecran)
{
  int x = mouseX*taille/ecran->w;
  int y = mouseY*taille/ecran->h;
  printf(" x = %d\t y = %d\n",x,y);
  tab[x][y] = !tab[x][y];
}




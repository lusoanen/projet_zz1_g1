/**
 * \file GOL_SDL.h
 * \brief Gestion de la SDL pour le Jeu de la Vie de Conway
*/

#ifndef _GOL_SDL_H_
#define _GOL_SDL_H_


#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>
/**
 * \fn void afficheGame(int ** tab,int taille,SDL_DisplayMode * ecran,SDL_Renderer * rendu);
 * \brief affiche un fond blanc et pour chaque case dessine un carré
 * \param tab le tableau correspondant à l'état du jeu
 * \param taille taille du tableau
 * \param ecran sert à déterminer la taille des cases
 * \param rendu rendu sur lequel dessiner les cases pour l'affichage
*/
void afficheGame(int ** tab,int taille,SDL_DisplayMode * ecran,SDL_Renderer * rendu);

/**
 * \fn void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran);
 * \brief initialisation de la SDL, de la fenêtre, du rendu et de l'écran
 * \param fenetre l'adresse de la fenêtre à initialiser
 * \param rendu l'adresse du rendu à initialiser
 * \param ecran l'adresse de l'écran' à initialiser 
*/
void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran);

/**
 * \fn void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);
 * \brief fermeture de la SDL, de la fenêtre et du rendu avec le  message d'erreur approprié
 * \param ok vaut 0 si erreur à imprimer
 * \param message message d'erreur à imprimer
 * \param fenetre fenêtre à supprimer
 * \param rendu rendu à supprimer
*/
void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);

/**
 * \fn void getCase(int ** tab,int taille,int mouseX,int mouseY,SDL_DisplayMode *ecran);
 * \brief transforme la case pointée par la souris en son inverse
 * \param tab le tableau de jeu
 * \param taille taille du tableau
 * \param mouseX coordonnée en X de la souris
 * \param mouseY coordonnée en Y de la souris
 * \param ecran ecran du jeu pour selectionner la case
 */
void getCase(int ** tab,int taille,int mouseX,int mouseY,SDL_DisplayMode *ecran);

#endif
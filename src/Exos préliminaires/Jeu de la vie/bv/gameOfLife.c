#include "gameOfLife.h"

int main()
{

  SDL_Window *fenetre;
  SDL_Renderer *rendu;
  SDL_DisplayMode ecran;

  Init(&fenetre, &rendu, &ecran);
  int i, taille = 100;
  int **tab = creeTab2D(taille);
  int **tmpTab = creeTab2D(taille);
  initialiser(tab, taille);
  tab[0][2] = 1;
  tab[1][0] = 1;
  tab[1][2] = 1;
  tab[2][1] = 1;
  tab[2][2] = 1;
  gameloop(tab, tmpTab, taille, rendu, &ecran);
  freeTab2D(tab, taille);
  freeTab2D(tmpTab, taille);
  fermetureSdl(1, "Normal ending", fenetre, rendu);
  return 0;
}

void gameloop(int **tab, int **tmpTab, int taille, SDL_Renderer *rendu, SDL_DisplayMode *ecran)
{
  SDL_Event event;
  SDL_Rect rect;
  int v = finDuJeu(tab, taille), pause = 1;
  while (!v)
  {
    afficheGame(tab, taille, ecran, rendu);
    while (SDL_PollEvent(&event))
    {
      switch (event.type)
      {
      case SDL_WINDOWEVENT:
        switch (event.window.event)
        {
        case SDL_WINDOWEVENT_CLOSE:
          v = 1;
          break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
          ecran->w = event.window.data1;
          ecran->h = event.window.data2;
          rect.w = ecran->w;
          rect.h = ecran->h;
          break;

        default:
          break;
        }
        break;

      case SDL_KEYDOWN:
        switch (event.key.keysym.sym)
        {
        case SDLK_SPACE:
          pause = !pause;
          break;

        default:
          break;
        }
        break;

      case SDL_MOUSEBUTTONDOWN:
        //printf("Appui :%d %d\n", event.button.x, event.button.y);
        getCase(tab, taille, event.button.x, event.button.y, ecran);
        break;
      case SDL_QUIT:
        v = 1;
        break;
      default:
        break;
      }
    }
    if (!pause)
    {
      evolutionTab(tab, tmpTab, taille);
      updateTab(&tab, &tmpTab);
      v = finDuJeu(tab, taille);
    }
    SDL_Delay(60);
  }
}

int **creeTab2D(int taille)
{
  int i;
  int **tab = (int **)malloc(taille * sizeof(int *));
  for (i = 0; i < taille; i++)
  {
    tab[i] = (int *)malloc(taille * sizeof(int));
  }
  return tab;
}

void initialiser(int **t, int taille)
{
  int i, j;
  for (i = 0; i < taille; i++)
  {
    for (j = 0; j < taille; j++)
    {
      t[i][j] = 0;
      //t[i][j] = 1; //fin fonctionne
    }
  }
}
int evolutionCase(int **tab, int x, int y, int taille)
{
  int i, j, compte = 0;
  for (i = -1; i < 2; i++)
  {
    for (j = -1; j < 2; j++)
    {
      if ((i || j) && tab[(x + i + taille) % taille][(y + j + taille) % taille])
        compte++;
    }
  }
  return ((tab[x][y] && compte == 2) || compte == 3);
}

void updateTab(int ***tab1, int ***tab2)
{
  int **swap;
  swap = *tab1;
  *tab1 = *tab2;
  *tab2 = swap;
}

void freeTab2D(int **tab, int taille)
{
  int i;
  for (i = 0; i < taille; i++)
  {
    free(tab[i]);
  }
  free(tab);
}

void afficheTab2D(int **tab, int taille)
{
  printf("Tableau de jeu \n");
  int i, j;
  for (i = 0; i < taille; i++)
  {
    for (j = 0; j < taille; j++)
    {
      printf("%d\t", tab[i][j]);
    }
    printf("\n");
  }
}

void evolutionTab(int **tab, int **tmpTab, int taille)
{
  int i, j;
  for (i = 0; i < taille; i++)
  {
    for (j = 0; j < taille; j++)
    {
      tmpTab[i][j] = evolutionCase(tab, i, j, taille);
    }
  }
}

int finDuJeu(int **tab, int taille)
{
  int i = 0, j, fin = 1;
  while (fin && i < taille)
  {
    j = 0;
    while (fin && (j < taille))
    {
      fin = !tab[i][j];
      j++;
    }
    i++;
  }
  return fin;
}
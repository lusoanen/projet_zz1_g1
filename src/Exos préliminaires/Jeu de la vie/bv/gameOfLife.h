/**
 * \file gameOfLife.h
 * \brief Gestion du moteur de jeu du Jeu de la vie de Conway
*/

#ifndef _GAMEOFLIFE_H_
#define _GAMEOFLIFE_H_


#include"GOL_SDL.h"
#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>


int main();
/**
 * \fn int ** creeTab2D(int taille);
 * \brief alloue l'espace pour une matrice carrée
 * \param taille la taille de la matrice
 * \return la matrice
 */
int ** creeTab2D(int taille);

/**
 * \fn void initialiser(int** t,int taille);
 * \brief initialise toutes les cases à 0
 * \param t le tableau
 * \param taille la taille du tableau
 */
void initialiser(int** t,int taille);

/**
 * \fn int evolutionCase(int** tab,int x,int y,int taille);
 * \brief determine l'état prochain d'une case
 * \param tab le tableau de jeu
 * \param x coordonnée en X de la case à tester
 * \param y coordonnée en Y de la case à tester
 * \param taille taille du tableau
 * \return resultat du test
*/
int evolutionCase(int** tab,int x,int y,int taille);

/**
 * \fn void evolutionTab(int** tab,int** tmpTab,int taille);
 * \brief met à jour un tableau temporaire pour l'état suivant en appellant la fonction evolutionCase() pour toutes les cases
 * \param tab le tableau de jeu à l'état courant
 * \param tmpTab un tableau temporaire qui stocke les nouvelles cases
 * \param taille la taille des tableaux
 */
void evolutionTab(int** tab,int** tmpTab,int taille);

/**
 * \fn void updateTab(int*** tab1,int*** tab2);
 * \brief echange les pointeurs des tableaux
 * \param tab1 l'adresse d'un premier tableau
 * \param tab2 l'adress d'un second tableau
 */
void updateTab(int*** tab1,int*** tab2);

/**
 * \fn void freeTab2D(int** tab,int taille);
 * \brief libere la mémoire d'un tableau
 * \param tab le tableau à libérer
 * \param taille taille du tableau
*/
void freeTab2D(int** tab,int taille);

/**
 * \fn void afficheTab2D(int** tab,int taille);
 * \brief affiche le contenu d'un tableau en 2 dimensions
 * \param tab le tableau à afficher
 * \param taille taille du tableau
*/
void afficheTab2D(int** tab,int taille);

/**
 * \fn void gameloop(int** tab,int ** tmpTab,int taille,SDL_Renderer* rendu,SDL_DisplayMode * ecran);
 * \brief moteur du jeu, gère les évènements de clic, pause, fermeture de la fenetre. Calcule et affiche le tableau
 * \param tab tableau de jeu
 * \param tmpTab tableau de jeu temporaire
 * \param taille taille des tableaux
 * \param rendu rendu avec lequel afficher l'état du jeu
 * \param ecran ecran sur lequel afficher le jeu
*/
void gameloop(int** tab,int ** tmpTab,int taille,SDL_Renderer* rendu,SDL_DisplayMode * ecran);

/**
 * \fn int finDuJeu(int** tab,int taille );
 * \brief vérifie qu'il y a encore des cases vivantes
 * \param tab le tableau de jeu
 * \param taille taille du tableau de jeu
 * \return test de s'il existe encore des cases vivantes
*/
int finDuJeu(int** tab,int taille );


#endif
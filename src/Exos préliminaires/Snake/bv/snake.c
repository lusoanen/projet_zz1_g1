#include"snake.h"

int main()
{

	SDL_Window* fenetre;
 	SDL_Renderer* rendu;
 	SDL_DisplayMode ecran;

    int pas = 200;
    Init(&fenetre,&rendu,&ecran);

    animation(pas ,rendu,&ecran);


	fermetureSdl(1, "Normal ending", fenetre, rendu);
    return 0;
}

void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran){
    if (SDL_Init(SDL_INIT_VIDEO) != 0) 
		fermetureSdl(0, "ERROR SDL INIT", *fenetre, *rendu);

	SDL_GetCurrentDisplayMode(0, &(*ecran));


	*fenetre = SDL_CreateWindow("Premier dessin",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, ecran->w * 0.66,ecran->h * 0.66, SDL_WINDOW_OPENGL);
	if (*fenetre == NULL) 
		fermetureSdl(0, "ERROR WINDOW CREATION", *fenetre, *rendu);


	*rendu = SDL_CreateRenderer(*fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (*rendu == NULL) 
	  	fermetureSdl(0, "ERROR RENDERER CREATION", *fenetre, *rendu);


}


void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon){
   	SDL_SetRenderDrawColor(rendu, 255 , 255, 0, 255);
   	int diametre = rayon*2;

   	int x = (rayon - 1);
   	int y = 0;
	int tx = 1;
	int ty = 1;
	int error = (tx - diametre);

   	while (x >= y)
   	{
   
	    SDL_RenderDrawPoint(rendu, centreX + x, centreY - y);
	    SDL_RenderDrawPoint(rendu, centreX + x, centreY + y);
	    SDL_RenderDrawPoint(rendu, centreX - x, centreY - y);
	    SDL_RenderDrawPoint(rendu, centreX - x, centreY + y);
	    SDL_RenderDrawPoint(rendu, centreX + y, centreY - x);
	    SDL_RenderDrawPoint(rendu, centreX + y, centreY + x);
	    SDL_RenderDrawPoint(rendu, centreX - y, centreY - x);
	    SDL_RenderDrawPoint(rendu, centreX - y, centreY + x);
    
	    if (error <= 0){
	        ++y;
	        error += ty;
	        ty += 2;
	    }

	    if (error > 0){
	        --x;
	        tx += 2;
	        error += (tx - diametre);
	    }
	}
	
}


void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu) {                 
  char messageParDefaut[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(messageParDefaut, message, 250);                                 
         l = strlen(messageParDefaut);                                        
         strcpy(messageParDefaut + l, " : %s\n");                     

         SDL_Log(messageParDefaut, SDL_GetError());                   
  }                                                               

  if (rendu != NULL) SDL_DestroyRenderer(rendu);                            
  if (fenetre != NULL)   SDL_DestroyWindow(fenetre);                                     
  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}   
void dessinerMaison(SDL_Renderer* rendu, SDL_Rect* coord){
	SDL_SetRenderDrawColor(rendu, 255 ,0, 0, 255);
    //quatres murs
	SDL_RenderDrawLine(rendu,coord->x, coord->y, coord->x, coord->y +coord->h );
	SDL_RenderDrawLine(rendu,coord->x+coord->w, coord->y, coord->x+coord->w, coord->y+coord->h );
	SDL_RenderDrawLine(rendu,coord->x, coord->y, coord->x+coord->w, coord->y );
	SDL_RenderDrawLine(rendu,coord->x, coord->y +coord->h, coord->x+coord->w, coord->y +coord->h );
}

void animation(int pas , SDL_Renderer* rendu, SDL_DisplayMode* ecran){
	int i = 0, j = 0;
	float rotationI, rotationJ;
	int centreRotationI = (ecran->w)*0.66/2 , centreRotationJ = (ecran->h)*0.66/2;
	SDL_Rect maison = {centreRotationI-50,centreRotationJ-50,100,100};
	SDL_Rect mouvement = maison;
	while(i < pas){

		//dessinerRectangle(rendu, i*10);
        mouvement.x = i*cos(i) +maison.x;
        mouvement.y = i*sin(i) +maison.y;
		dessinerCercle(rendu, centreRotationI, centreRotationJ, i);
		dessinerMaison(rendu,&mouvement );
		SDL_SetRenderDrawColor(rendu, 0 , 0,0, 255);
		SDL_RenderPresent(rendu);
		SDL_Delay(16);
		
		SDL_RenderClear(rendu);
		i++;
	}	
}
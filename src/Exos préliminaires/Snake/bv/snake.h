/**
 * \file snake.h
 * \brief L'animation de Baptiste VALLET avec le renderer : un carré qui bouge au centre d'un cerlce grandissant
 */

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <string.h>
/**
 * \fn void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran);
 * \brief initialisation de la SDL, de la fenêtre, du rendu et de l'écran
 * \param fenetre l'adresse de la fenêtre à initialiser
 * \param rendu l'adresse du rendu à initialiser
 * \param ecran l'adresse de l'écran' à initialiser 
*/
void Init(SDL_Window **fenetre, SDL_Renderer **rendu, SDL_DisplayMode *ecran);

/**
 * \fn void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);
 * \brief fermeture de la SDL, de la fenêtre et du rendu avec le  message d'erreur approprié
 * \param ok vaut 0 si erreur à imprimer
 * \param message message d'erreur à imprimer
 * \param fenetre fenêtre à supprimer
 * \param rendu rendu à supprimer
*/
void fermetureSdl(char ok, char const *message, SDL_Window *fenetre, SDL_Renderer *rendu);

/**
 * \fn void animation(int pas , SDL_Renderer* rendu, SDL_DisplayMode* ecran);
 * \brief affiche l'animation du cercle grandissant et du carré qui bouge à l'interieur
 * \param pas nombre de boucles de l'animation
 * \param rendu rendu sur lequel dessiner pour réaliser l'animation
 * \param ecran il determine l'emplacement  des figures
*/
void animation(int pas, SDL_Renderer *rendu, SDL_DisplayMode *ecran);

/**
 * \fn void dessinerMaison(SDL_Renderer* rendu, SDL_Rect* coord);
 * \brief dessine les cotés d'un carré. A l'origine je voulais faire d'autre choses avec d'où le nom
 * \param rendu rendu sur lequel dessiner
 * \param coord les coordonnées du carré à dessiner
*/
void dessinerMaison(SDL_Renderer *rendu, SDL_Rect *coord);

/**
 * \fn void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon)
 * \brief dessine un nombre fini de points  qui forment un cercle
 * \param rendu rendu sur lequel dessiner
 * \param centreX abscisse du centre du cercle
 * \param centreY coordonnée du centre du cercle
 * \param rayon rayon du cercle
*/
void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon);

int main();
#endif
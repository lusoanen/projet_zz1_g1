#include "snake.h"


void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu) {                 
  char messageParDefaut[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(messageParDefaut, message, 250);                                 
         l = strlen(messageParDefaut);                                        
         strcpy(messageParDefaut + l, " : %s\n");                     

         SDL_Log(messageParDefaut, SDL_GetError());                   
  }                                                               

  if (rendu != NULL) SDL_DestroyRenderer(rendu);                            
  if (fenetre != NULL)   SDL_DestroyWindow(fenetre);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}    

void dessinerRectangle(SDL_Renderer* rendu, int position){
	SDL_SetRenderDrawColor(rendu, 10 , 245, 10, 255);

	SDL_Rect rectangle;
	rectangle.x = 0 + position;
	rectangle.y = 0 + position;
	rectangle.w = 100;
	rectangle.h = 100;

	SDL_RenderFillRect(rendu, &rectangle);
}

void dessinerZZ(SDL_Renderer *rendu, int positionI, int positionJ){
	SDL_SetRenderDrawColor(rendu, 245 , 10, 10, 255);

	int x1 = positionI, y1 = positionJ , 
		x2 = positionI + 100, y2 = positionJ,
		x3 = positionI, y3 = positionJ + 100,
		x4 = positionI + 100, y4 = positionJ + 100;
		
	SDL_RenderDrawLine(rendu, x1, y1, x2, y2);
	SDL_RenderDrawLine(rendu, x2, y2, x3, y3);
	SDL_RenderDrawLine(rendu, x3, y3, x4, y4);
}

void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon){
   	SDL_SetRenderDrawColor(rendu, 10 , 10, 245, 255);
   	int diametre = rayon*2;

   	int x = (rayon - 1);
   	int y = 0;
	int tx = 1;
	int ty = 1;
	int error = (tx - diametre);

   	while (x >= y)
   	{
   
	    SDL_RenderDrawPoint(rendu, centreX + x, centreY - y);
	    SDL_RenderDrawPoint(rendu, centreX + x, centreY + y);
	    SDL_RenderDrawPoint(rendu, centreX - x, centreY - y);
	    SDL_RenderDrawPoint(rendu, centreX - x, centreY + y);
	    SDL_RenderDrawPoint(rendu, centreX + y, centreY - x);
	    SDL_RenderDrawPoint(rendu, centreX + y, centreY + x);
	    SDL_RenderDrawPoint(rendu, centreX - y, centreY - x);
	    SDL_RenderDrawPoint(rendu, centreX - y, centreY + x);
    
	    if (error <= 0){
	        ++y;
	        error += ty;
	        ty += 2;
	    }

	    if (error > 0){
	        --x;
	        tx += 2;
	        error += (tx - diametre);
	    }
	}
	
}

void calculerRotation(float *i, float *j, int tour, int rayon){
		*i = cos(tour) * rayon;
		*j = sin(tour) * rayon;
}

void animation(int pas , SDL_Renderer* rendu, SDL_DisplayMode* ecran){
	int i = 0, j = 0;
	float rotationI, rotationJ;
	int centreRotationI = (ecran->w)*0.66/2 , centreRotationJ = (ecran->h)*0.66/2;
	
	while(i < pas){
		if(i%2 == 0){
			calculerRotation(&rotationI, &rotationJ, j, i);
			j++;
		}

		//dessinerRectangle(rendu, i*10);
		dessinerZZ(rendu, centreRotationI + 5 + rotationI -50, centreRotationJ + rotationJ - 50);
		dessinerZZ(rendu, centreRotationI + rotationI -50, centreRotationJ + 10 + rotationJ - 50);
		dessinerCercle(rendu, centreRotationI, centreRotationJ, i);
		
		SDL_RenderPresent(rendu);
		SDL_Delay(16);
		SDL_SetRenderDrawColor(rendu, 0 , 0, 0, 255);
		
		SDL_RenderClear(rendu);
		i++;
	}	
}

int mainSnake(){
	int nbPas = 400;

	SDL_Window* fenetre = NULL;
 	SDL_Renderer* rendu = NULL;

 	SDL_DisplayMode ecran;

	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
		fermetureSdl(0, "ERROR SDL INIT", fenetre, rendu);

	SDL_GetCurrentDisplayMode(0, &ecran);


	fenetre = SDL_CreateWindow("Premier dessin",
	                            SDL_WINDOWPOS_CENTERED,
	                            SDL_WINDOWPOS_CENTERED, ecran.w * 0.66,
	                            ecran.h * 0.66,
	                            SDL_WINDOW_OPENGL);
	if (fenetre == NULL) 
		fermetureSdl(0, "ERROR WINDOW CREATION", fenetre, rendu);


	rendu = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (rendu == NULL) 
	  	fermetureSdl(0, "ERROR RENDERER CREATION", fenetre, rendu);
	
	animation(nbPas, rendu, &ecran);                                                    

	fermetureSdl(1, "Normal ending", fenetre, rendu);

	return EXIT_SUCCESS;
}
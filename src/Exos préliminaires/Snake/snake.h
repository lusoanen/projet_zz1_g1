#ifndef _SNAKE_H_
#define _SNAKE_H_

#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/**
 * @fn void dessinerRectangle(SDL_Renderer* rendu, int position)
 * @brief Dessine un rectangle à une position donnée.
 * 
 * @param rendu SDL_renderer utilisé.
 * @param position position voulue du coin haut gauche du rectangle
 */
void dessinerRectangle(SDL_Renderer* rendu, int position);

/**
 * @fn void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon)
 * @brief Dessine un rectangle à une position donnée.
 * 
 * @param rendu SDL_renderer utilisé.
 * @param centreX Position du centre du cercle suivant l'axe X.
 * @param centreY Position du centre du cercle suivant l'axe Y.
 * @param rayon Rayon du cercle.
 */
void dessinerCercle(SDL_Renderer *rendu, int centreX, int centreY, int rayon);

/**
 * @fn void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu)
 * @brief fermeture des éléments de SDL (fenetre et renderer)
 * 
 * @param ok validation de la fermeture normal de la SDL
 * @param message message de fin normal ou message d'erreur si besoin
 * @param fenetre à fermer
 * @param rendu à fermer
 */
void fermetureSdl(char ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);

/**
 * @fn void dessinerZZ(SDL_Renderer *rendu, int positionI, int positionJ)
 * @brief dessine un Z composé de 3 lignes, à la position souhaitez
 * 
 * @param rendu SDL_renderer utilisé
 * @param positionI position du point haut gauche du Z celon l'axe I (= X)
 * @param positionJ position du point haut gauche du Z celon l'axe J (= Y)
 */
void dessinerZZ(SDL_Renderer *rendu, int positionI, int positionJ);

/**
 * @fn void calculerRotation(float *i, float *j, int tour, int rayon)
 * @brief Calcul de la position IJ permettant la rotation des Z sur le cercle
 * 
 * @param i pointeur sur la valeur de la position celon l'axe I (= X)
 * @param j pointeur sur la valeur de la position celon l'axe J (= Y)
 * @param tour indique à quel moment de l'animation nous sommes
 * @param rayon rayon du cercle sur lequel doivent se trouver les Z
 */
void calculerRotation(float *i, float *j, int tour, int rayon);

/**
 * @fn void animation(int pas , SDL_Renderer* rendu, SDL_DisplayMode* ecran)
 * @brief boucle d'animation
 * 
 * @param pas nombre de tour à effectué
 * @param rendu SDL_renderer utilisé
 * @param ecran SDL_DisplayMode en cours d'utilisation
 */
void animation(int pas , SDL_Renderer* rendu, SDL_DisplayMode* ecran);

/**
 * @fn int mainSnake()
 * @brief point d'entrée permettant l'initialisation des éléments de SDL et le lancement de l'animation
 * 
 * @return message d'erreur ou de succès
 */
int mainSnake();

#endif
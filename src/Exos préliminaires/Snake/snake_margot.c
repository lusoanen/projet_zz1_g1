#include "snake_margot.h"

	int r[3] = {  0, 255, 155};
	int g[3] = {  0, 255, 255};
	int b[3] = {  0, 255,  75};
	int a[3] = {255, 255, 255};
	int sens_couleur[4] = {-1, -1, -1, -1};

int main()
{
	srand(time(0));

	SDL_Window * fenetre = NULL;
	SDL_Renderer * renderer = NULL;


	int largeur = LARGEUR_DEFAUT, hauteur = HAUTEUR_DEFAUT;
	int nb_iter = 400;

  // Initialisation de la SDL  + gestion de l'échec possible 
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
	    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());      // l'initialisation de la SDL a échoué 
	    exit(EXIT_FAILURE);
	}
	else 
	{
	    // Récupération de la taille de l'écran 

		int i = 0;

		SDL_DisplayMode ecran;

		int code_retour = SDL_GetCurrentDisplayMode(i, &ecran);

		if(code_retour != 0) 
		{
			SDL_Log("Impossible de récupérer le mode d'affichage pour l'affichage vidéo #%d : %s", i, SDL_GetError());

		}
		else 
		{
			SDL_Log("Afichage #%d: mode d'affichage courrant %dx%dpx @ %dhz.", i, ecran.w, ecran.h, ecran.refresh_rate);
			largeur = ecran.w;
			hauteur = ecran.h;
		}
		if (SDL_CreateWindowAndRenderer(largeur, hauteur, SDL_WINDOW_MAXIMIZED, &fenetre, &renderer) < 0) 
		{
			SDL_Log("Erreur d'initialisation de la fenêtre et du renderer : %s\n", SDL_GetError());
			exit(EXIT_FAILURE);
		}
		else 
		{
			SDL_SetWindowTitle(fenetre, "Snake");
			SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"); 
			SDL_RenderSetLogicalSize(renderer, largeur, hauteur);
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
			SDL_RenderPresent(renderer);
			SDL_UpdateWindowSurface(fenetre);

			SDL_Delay( 1000 );

			int l = largeur/10;
			int rayon = l*3/4;
			coordonnees_t A = {largeur/4,hauteur/4};
			coordonnees_t D = {A.x + (l/2), A.y - (l*sqrt(5)/4)};
			coordonnees_t sens = {1, 1};

			for (int i = 0; i < nb_iter; i++)
			{
				SDL_RenderClear(renderer);
				SDL_SetRenderDrawColor(renderer, r[0], g[0], b[0], a[0]);
				SDL_RenderDrawRect(renderer, NULL);

				dessine_triangle(renderer, r[2], g[2], b[2], a[2], A, l);

				dessine_cercle(renderer, r[1], g[1], b[1], a[1], D, rayon);		

				modif_couleurs();
				modif_coor(&A, &D, &sens, rayon, largeur, hauteur);

				SDL_SetRenderDrawColor(renderer, r[0], g[0], b[0], a[0]);
				SDL_RenderPresent(renderer);
				SDL_Delay(16);
			}
		}
	}
	return 0;
}

void dessine_triangle (SDL_Renderer * renderer, int r, int g, int b, int a, coordonnees_t A, int cote)
{
	coordonnees_t B = {A.x + cote, A.y}, C = {A.x + (cote/2), A.y - (cote*sqrt(5)/2)};

	SDL_SetRenderDrawColor(renderer, r, g, b, a);

	SDL_RenderDrawLine(renderer, A.x, A.y, B.x, B.y);
	SDL_RenderDrawLine(renderer, A.x, A.y, C.x, C.y);
	SDL_RenderDrawLine(renderer, B.x, B.y, C.x, C.y);
}

void dessine_cercle (SDL_Renderer * renderer, int r, int g, int b, int a, coordonnees_t centre, int rayon)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	int diametre = rayon*2;

   	int x = (rayon - 1);
   	int y = 0;
	int tx = 1;
	int ty = 1;
	int error = (tx - diametre);

   	while (x >= y)
   	{
   
	    SDL_RenderDrawPoint(renderer, centre.x + x, centre.y - y);
	    SDL_RenderDrawPoint(renderer, centre.x + x, centre.y + y);
	    SDL_RenderDrawPoint(renderer, centre.x - x, centre.y - y);
	    SDL_RenderDrawPoint(renderer, centre.x - x, centre.y + y);
	    SDL_RenderDrawPoint(renderer, centre.x + y, centre.y - x);
	    SDL_RenderDrawPoint(renderer, centre.x + y, centre.y + x);
	    SDL_RenderDrawPoint(renderer, centre.x - y, centre.y - x);
	    SDL_RenderDrawPoint(renderer, centre.x - y, centre.y + x);
    
	    if (error <= 0){
	        ++y;
	        error += ty;
	        ty += 2;
	    }

	    if (error > 0){
	        --x;
	        tx += 2;
	        error += (tx - diametre);
	    }
	}
}

void modif_coor(coordonnees_t * A, coordonnees_t * D,  coordonnees_t * sens, int rayon, int largeur, int hauteur)
{
	int pas = rand()%(largeur/100);
	int bordx = (D->x - rayon) + (sens->x*pas);
	int bordy = (D->y - rayon) + (sens->y*pas);
	if (bordx < 0)
	{
		pas = pas + bordx;
	}
	else if ((bordx + 2*rayon) >= largeur)
	{
		pas = pas - ((bordx + 2*rayon) - largeur);
	}

	A->x += sens->x * pas;
	D->x += sens->x * pas;

	if (bordx <= 0 || (bordx + 2*rayon) >= largeur-1)
	{
		sens->x = -sens->x;
	}

	if (bordy < 0)
	{
		pas = pas + bordy;
	}
	else if ((bordy + 2*rayon) >= hauteur)
	{
		pas = pas - ((bordy + 2*rayon) - hauteur);
	}

	A->y += sens->y * pas;
	D->y += sens->y * pas;

	if (bordy <= 0 || (bordy + 2*rayon) >= hauteur-1)
	{
		sens->y = -sens->y;
	}
}

void modif_couleurs ()
{
	int pas_r = rand()%6;
	int pas_g = rand()%6;
	int pas_b = rand()%6;
	int pas_gris = rand()%6;

	if (r[2] + (sens_couleur[0]*pas_r) < 40 || r[2] + (sens_couleur[0]*pas_r) >= 256)
	{
		sens_couleur[0] *= -1;
	}
	if (g[2] + (sens_couleur[1]*pas_g) < 40 || g[2] + (sens_couleur[1]*pas_g) >= 256)
	{
		sens_couleur[1] *= -1;
	}
	if (b[2] + (sens_couleur[2]*pas_b) < 40 || b[2] + (sens_couleur[2]*pas_b) >= 256)
	{
		sens_couleur[2] *= -1;
	}
	if (r[1] + (sens_couleur[3]*pas_gris) < 40 || r[1] + (sens_couleur[3]*pas_gris) >= 256)
	{
		sens_couleur[3] *= -1;
	}

	r[2] += sens_couleur[0]*pas_r;
	g[2] += sens_couleur[1]*pas_g;
	b[2] += sens_couleur[2]*pas_b;
	r[1] += sens_couleur[3]*pas_gris;
	g[1] = b[1] = r[1];
}
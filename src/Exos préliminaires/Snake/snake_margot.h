#ifndef _SNAKE_H_
#define _SNAKE_H_ 

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define HAUTEUR_DEFAUT 480 
#define LARGEUR_DEFAUT 640

typedef struct coordonnees
{
	int x; /**< Abscisse */
	int y; /**< Ordonnée */
}coordonnees_t;

void dessine_triangle (SDL_Renderer * renderer, int r, int g, int b, int a, coordonnees_t A, int cote);

void dessine_cercle (SDL_Renderer * renderer, int r, int g, int b, int a, coordonnees_t centre, int rayon);

void modif_coor(coordonnees_t * A, coordonnees_t * D,  coordonnees_t * sens, int rayon, int largeur, int hauteur);

void modif_couleurs();

#endif
/**
 * \file X_file.c
*/

#include <SDL2/SDL.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
/**
* \fn SDL_Window ** lineWin(int x,int y,int w,int h,int nb);
* \brief crée une ligne de fenetre entre deux coordonnées et les fait apparaître une à une
* \param x  abscisse du premier point
* \param y coordonnée du premier point
* \param w abscisse du second point
* \param h coordonnée du second point
* \return un tableau de fenêtres
*/
SDL_Window **lineWin(int x, int y, int w, int h, int nb);

/**
 * \fn void closeLineWin(SDL_Window **TabWin, int taille);
 * \brief detruit une à une toutes le fenêtres de la ligne
 * \param Tabwin le tableau de fenêtres
 * \param taille la taille du tableau de fenêtres
*/
void closeLineWin(SDL_Window **TabWin, int taille);

/**
 * \fn void BounceLineWin(SDL_Window **TabWin, int taille);
 * \brief fait aux fenêtres un mouvement d'aller retour rapide en diagonale synchronisé
 * \param Tabwin le tableau de fenêtres
 * \param taille la taille du tableau de fenêtres
*/
void BounceLineWin(SDL_Window **TabWin, int taille);

/**
 * \fn void WaveLine(SDL_Window **window, int taille, int w, int cycles);
 * \brief fait aux fenetres un mouvement en canon d'aller retour d'un bout à l'autre de l'écran en accélérant puis freinant
 * \param window le tableau de fenêtres
 * \param taille la taille du tableau de fenêtres
 * \param w la largeur de l'écran
 * \param cycles determine le nombre d'allaers retours à faire
*/
void WaveLine(SDL_Window **window, int taille, int w, int cycles);

/********************************************/
/* Vérification de l'installation de la SDL */
/********************************************/

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  SDL_version nb;
  SDL_VERSION(&nb);
  SDL_DisplayMode mode;

  printf("Version de la SDL : %d.%d.%d\n", nb.major, nb.minor, nb.patch);
  SDL_Window *window_1 = NULL, // Future fenêtre de gauche
      *window_2 = NULL;        // Future fenêtre de droite

  /* Initialisation de la SDL  + gestion de l'échec possible */
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError()); // l'initialisation de la SDL a échoué
    exit(EXIT_FAILURE);
  }
  int should_be_zero = SDL_GetCurrentDisplayMode(0, &mode);

  if (should_be_zero != 0)
    // In case of error...
    SDL_Log("Could not get display mode for video display %s", SDL_GetError());

  else
    SDL_Log("Display current display mode is %dx%dpx @ %dhz.", mode.w, mode.h, mode.refresh_rate);

  /* Création de la fenêtre de gauche */
  //window_1 = SDL_CreateWindow(
  //   "Fenêtre à gauche",                    // codage en utf8, donc accents possibles
  //  0, 0,                                  // coin haut gauche en haut gauche de l'écran
  // 400, 300,                              // largeur = 400, hauteur = 300
  // SDL_WINDOW_RESIZABLE);                 // redimensionnable

  //if (window_1 == NULL) {
  // SDL_Log("Error : SDL window 1 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
  // SDL_Quit();
  //exit(EXIT_FAILURE);
  //}
  /* Création de la fenêtre de droite */
  //window_2 = SDL_CreateWindow(
  //  "Fenêtre à droite",                    // codage en utf8, donc accents possibles
  //  400, 0,                                // à droite de la fenêtre de gauche
  //  500, 300,                              // largeur = 500, hauteur = 300
  //  0);

  //if (window_2 == NULL) {
  /* L'init de la SDL : OK
       fenêtre 1 :OK
       fenêtre 2 : échec */
  //SDL_Log("Error : SDL window 2 creation - %s\n", SDL_GetError());   // échec de la création de la fenêtre
  //SDL_DestroyWindow(window_1);
  //SDL_Quit();
  //exit(EXIT_FAILURE);
  // }

  /* Normalement, on devrait ici remplir les fenêtres... */
  //  SDL_Delay(2000);                           // Pause exprimée  en ms

  /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
  //  SDL_DestroyWindow(window_2);
  //  SDL_DestroyWindow(window_1);
  printf("screenCoord %d %d", mode.w, mode.h);
  int tailleLF1 = 10;
  int tailleLF2 = 10;
  SDL_Window **LigneFenetre1 = lineWin(0, 0, mode.w, mode.h, tailleLF1);
  //SDL_Window ** LigneFenetre2 =lineWin(0,mode.h,mode.w,0,tailleLF2);

  SDL_Delay(1000);

  //BounceLineWin(LigneFenetre1,tailleLF1);
  WaveLine(LigneFenetre1, tailleLF1, mode.w, 3);
  closeLineWin(LigneFenetre1, tailleLF1);
  //closeLineWin(LigneFenetre2,tailleLF2);
  SDL_Quit();
  return 0;
}

SDL_Window **lineWin(int x, int y, int w, int h, int nb)
{
  SDL_Window **TabWin = (SDL_Window **)malloc(nb * sizeof(SDL_Window *));
  if (TabWin != NULL)
  {
    int i = 0, b = 1;
    while (i < nb && b)
    {
      TabWin[i] = SDL_CreateWindow("X Files 2", x + i * (w - x) / nb, y + i * (h - y) / nb, w / nb, h / nb, 0);
      b = (TabWin[i] != NULL);
      i++;
      SDL_Delay(100);
    }
  }
  return TabWin;
}

void closeLineWin(SDL_Window **TabWin, int taille)
{
  int j = 0;
  while (TabWin[j] != NULL && j < taille)
  {
    SDL_DestroyWindow(TabWin[j]);
    j++;
    SDL_Delay(100);
  }
  free(TabWin);
}

void BounceLineWin(SDL_Window **TabWin, int taille)
{
  int i = 0, j = 0;
  int x = 0, y = 0;
  for (i = 0; i < taille; i++)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x + i, y + i);
      j++;
    }
  }
  for (i = taille; i > 0; i--)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x + i, y + i);
      j++;
    }
  }
  for (i = 0; i < taille; i++)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x - (2 * i), y - (2 * i));
      j++;
    }
  }
  for (i = taille; i > 0; i--)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x - (2 * i), y - (2 * i));
      j++;
    }
  }
  for (i = 0; i < taille; i++)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x + i, y + i);
      j++;
    }
  }
  for (i = taille; i > 0; i--)
  {
    j = 0;
    while (TabWin[j] != NULL && j < taille)
    {
      SDL_GetWindowPosition(TabWin[j], &x, &y);
      SDL_SetWindowPosition(TabWin[j], x + i, y + i);
      j++;
    }
  }
}

void WaveLine(SDL_Window **TabWin, int taille, int w, int cycles)
{
  int i = 0, j = 0;
  int x, y, k = 0;
  int largeur = w / taille;
  int *tabSens = (int *)malloc(taille * sizeof(int));
  for (i = 0; i < (taille - 1); i++)
    tabSens[i] = 0;
  tabSens[i] = 1;
  while (k < cycles)
  {
    for (i = 0; i < taille; i++)
    {
      j = 0;
      while (TabWin[j] != NULL && j < taille)
      {
        SDL_GetWindowPosition(TabWin[j], &x, &y);
        if (tabSens[j] == 0)
        {

          if (x < (w) / 2)
            x += 1 + x / largeur;
          else
            x += 1 + (w - x) / largeur;

          tabSens[j] = (x + largeur >= w);
        }
        else
        {

          if (x < (w) / 2)
            x -= 1 + x / largeur;
          else
            x -= 1 + (w - x) / largeur;

          if (x > 0)
          {
            tabSens[j] = 1;
          }
          else
          {
            tabSens[j] = 0;
            if (j == 0)
              k++;
          }
        }
        SDL_SetWindowPosition(TabWin[j], x, y);
        j++;
      }
    }
  }
  free(tabSens);
}
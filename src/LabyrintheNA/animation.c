#include "animation.h"

void Affiche_Game(SDL_Window *fenetre, SDL_Renderer *rendu, SDL_DisplayMode *ecran, piece_t **tabJeu, int tabWidth, int tabHeight, personnage_t **perso, int nbPersonnages, int *mouvValide, SDL_Texture **persoText, SDL_Texture **barriere, SDL_Texture *herbe)
{
    //avec le tableau de jeu mis a jour et la liste des deplacements reussis

    int i, phaseSprite;
    SDL_Rect spriteSize = {0, 0, 16, 16}, //zone dans la spritesheet
        spritePos;                        //zone dans la fenetre
    SDL_GetWindowSize(fenetre, &spritePos.w, &spritePos.h);
    spritePos.x = 0;
    spritePos.y = 0;
    spritePos.w = spritePos.w / tabWidth;
    spritePos.h = spritePos.h / tabHeight;
    int indice_Text = 0;

    for (phaseSprite = 0; phaseSprite < 4; phaseSprite++)
    {
        SDL_RenderClear(rendu);
        // fonction de dessin de fond
        SDL_RenderCopy(rendu, herbe, NULL, NULL);

        for (i = 0; i < nbPersonnages; i++)
        {

            if (estVivant(perso[i]) && getType(perso[i]) != MOUETTE)
            {
                indice_Text = getType(perso[i]);
                
                if (indice_Text < 2)
                {

                    affichePerso(rendu, persoText[indice_Text], spritePos, spriteSize, perso[i], mouvValide[i], phaseSprite);
                }
            }
        }
        dessine_barrieres(fenetre, rendu, barriere, tabJeu, tabHeight, tabWidth, spritePos.w, spritePos.h);
        for (i = 1; i < nbPersonnages; i++)
        {
            if (estVivant(perso[i]) && getType(perso[i]) == MOUETTE)
            {
                indice_Text = getType(perso[i]);
                affichePerso(rendu, persoText[indice_Text], spritePos, spriteSize, perso[i], mouvValide[i], phaseSprite);
            }
        }


        SDL_RenderPresent(rendu);
        SDL_Delay(100);
        SDL_RenderClear(rendu);
    }
}

void AfficherGameOver(SDL_Window * fenetre, SDL_DisplayMode * ecran, SDL_Renderer * rendu, int  nbTour, SDL_Texture *herbe, int nbLigne, int nbCol){
    SDL_RenderClear(rendu);
    SDL_RenderCopy(rendu, herbe, NULL, NULL);

    if(TTF_Init() == -1){
        fermetureSdl(0, "PROBLEME TTF", fenetre, rendu);
    }

    TTF_Font *policeTexte, *policeScore, *policeCredit;
    SDL_Rect posT, posS, posC;
    SDL_Texture *textureTexte;

    SDL_Surface *texte,*score ,*credit;

    policeTexte = TTF_OpenFont("asset/IndieFlower-Regular.ttf", 250);
    policeScore = TTF_OpenFont("asset/IndieFlower-Regular.ttf", 100);
    policeCredit = TTF_OpenFont("asset/IndieFlower-Regular.ttf", 60);

    SDL_Color noire = {0,0,0};
    texte = TTF_RenderText_Blended(policeTexte, "GAME OVER", noire);
    char phrase[100];
    sprintf(phrase, "Vous avez survecut %d tours", nbTour );
    score = TTF_RenderText_Blended(policeScore, phrase, noire);
    credit = TTF_RenderText_Blended(policeCredit, "Merci a Kacper Wozniak et Benjamin Tissot", noire);

    int h, w;
    SDL_GetRendererOutputSize(rendu, &w, &h);
    //SDL_Rect *tailleRend = {0,0, w, h};

    SDL_Surface *global = SDL_CreateRGBSurface(0, w, h, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);

    posT.x = w/2 - texte->w/2;
    posT.y = 0;
    SDL_BlitSurface(texte, NULL, global, &posT);

    posS.x = w/2 - score->w/2;
    posS.y = global->h /2;
    SDL_BlitSurface(score, NULL, global, &posS);

    posC.x = w/2 - credit->w/2;
    posC.y = (global->h/4 )*3;
    SDL_BlitSurface(credit, NULL, global, &posC);
 
    textureTexte = SDL_CreateTextureFromSurface(rendu, global);
    SDL_RenderCopy(rendu, textureTexte, NULL, NULL );

    SDL_RenderPresent(rendu);
    SDL_Delay(5000);
    SDL_RenderClear(rendu);

    TTF_CloseFont(policeTexte);
    TTF_CloseFont(policeCredit);
    TTF_CloseFont(policeScore);
    TTF_Quit();
}

void affichePerso(SDL_Renderer *rendu, SDL_Texture *currText, SDL_Rect spritePos, SDL_Rect spriteSize, personnage_t *perso, int mouvValide, int phaseSprite)
{
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    switch (perso->regard)
    {
    case (NORD):
        spriteSize.y = 2 * spriteSize.h;                   // choix de la ligne de sprite
        spritePos.x = (perso->positionMapX) * spritePos.w; // position actuelle du personnage en X

        if (mouvValide && phaseSprite < 4)
        {                                                                                            // cas ou il y a eu deplacement
            spriteSize.x = (phaseSprite)*spriteSize.w;                                               //passage au sprite suivant
            spritePos.y = (1 + perso->positionMapY) * spritePos.h - (phaseSprite * spritePos.h / 3); //deplacement leger
        }
        else
        {
            spriteSize.x = (1 + (2 & phaseSprite)) * spriteSize.w;
            spritePos.y = (perso->positionMapY) * spritePos.h;
        }

        break;
    case (SUD):
        spriteSize.y = spriteSize.h;
        spritePos.x = perso->positionMapX * spritePos.w;

        if (mouvValide && phaseSprite < 4)
        {
            spriteSize.x = (phaseSprite)*spriteSize.w;
            spritePos.y = (perso->positionMapY - 1) * spritePos.h + (phaseSprite * spritePos.h / 3);
        }
        else
        {
            spriteSize.x = (1 + (2 & phaseSprite)) * spriteSize.w;
            spritePos.y = (perso->positionMapY) * spritePos.h;
        }

        break;
    case (EST):
        spriteSize.y = 0;
        spritePos.y = (perso->positionMapY) * spritePos.h;
        if (mouvValide)
        {
            spriteSize.x = (phaseSprite)*spriteSize.w;
            spritePos.x = (perso->positionMapX - 1) * spritePos.w + (phaseSprite * spritePos.w / 3);
        }
        else
        {
            spriteSize.x = (1 + (2 & phaseSprite)) * spriteSize.w;
            spritePos.x = perso->positionMapX * spritePos.w;
        }

        break;

    case (OUEST):
        spriteSize.y = 0;
        spritePos.y = (perso->positionMapY) * spritePos.h;
        flip = SDL_FLIP_HORIZONTAL;

        if (mouvValide)
        {
            spriteSize.x = (phaseSprite)*spriteSize.w;
            spritePos.x = (perso->positionMapX + 1) * spritePos.w - (phaseSprite * spritePos.w / 3);
        }
        else
        {
            spriteSize.x = (1 + (2 & phaseSprite)) * spriteSize.w;
            spritePos.x = perso->positionMapX * spritePos.w;
        }
        break;


    default:
        spriteSize.y = 0;
        spritePos.y = (perso->positionMapY) * spritePos.h;
        spritePos.x = perso->positionMapX * spritePos.w;
        if (mouvValide)
            spriteSize.x = (1 + (2 & phaseSprite)) * spriteSize.w;
        else
            spriteSize.x = 3 * spriteSize.w;
        break;
    }
    //affichage
    SDL_RenderCopyEx(rendu, currText, &spriteSize, &spritePos, 0, NULL, flip);
}

SDL_Texture **loadTextPersos(SDL_Window *fenetre, SDL_Renderer *rendu)
{
    SDL_Texture **persoText = (SDL_Texture **)malloc(3 * sizeof(SDL_Texture *));

    persoText[0] = IMG_LoadTexture(rendu, "asset/Tiles/souris.png");
    if (persoText[0] == NULL)
    {
        fermetureSdl(0, "Problème de chargement de la texture de la souris", fenetre, rendu);
    }
    persoText[1] = IMG_LoadTexture(rendu, "asset/Tiles/ours.png");
    if (persoText[0] == NULL)
    {
        fermetureSdl(0, "Problème de chargement de la texture de l'ours", fenetre, rendu);
    }
    persoText[2] = IMG_LoadTexture(rendu, "asset/Tiles/mouette.png");
    if (persoText[0] == NULL)
    {
        fermetureSdl(0, "Problème de chargement de la texture de la mouette", fenetre, rendu);
    }

    return persoText;
}

void libereTextPersos(SDL_Texture **persoText)
{
    SDL_DestroyTexture(persoText[0]);
    SDL_DestroyTexture(persoText[1]);
    SDL_DestroyTexture(persoText[2]);
    free(persoText);
}
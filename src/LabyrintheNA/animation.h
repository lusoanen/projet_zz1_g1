/**
 * \file animation.h
 * \brief gestion des animation sdl
 */

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include"baseSDL.h"
#include"direction.h"
#include"fond_v2.h"
#include"personnage.h" // personnage
#include "baseSDL.h"
#include <math.h>

/**
 * @brief 
 * 
 * @param fenetre 
 * @param rendu 
 * @param ecran 
 * @param tabJeu matrice des pièces du labyrinthe
 * @param tabWidth nombre de colonne
 * @param tabHeight nombre de ligne
 * @param perso tableau de tous les personnages du labyrinthe
 * @param nbPersonnages nombre de personnage dans la tableau
 * @param mouvValide tableau des mouvements du personnage possible
 * @param persoText texture des textures des personnages
 * @param barriere tableau des textures des barrières
 * @param herbe texture de l'herbe
 */
void Affiche_Game(SDL_Window * fenetre, SDL_Renderer * rendu, SDL_DisplayMode * ecran,piece_t** tabJeu,int tabWidth,int tabHeight ,personnage_t** perso,int nbPersonnages,int* mouvValide,SDL_Texture **persoText,SDL_Texture ** barriere, SDL_Texture * herbe);

/**
 * @brief affichage de l'écran de game overet des crédits
 *
 * @param      fenetre  
 * @param      ecran    
 * @param      rendu    
 * @param[in]  nbTour   nombre de tour où le joueur a survécu
 */
void AfficherGameOver(SDL_Window * fenetre, SDL_DisplayMode * ecran, SDL_Renderer * rendu, int  nbTour, SDL_Texture *herbe, int nbLigne, int nbCol);

/**
 * @brief 
 * 
 * @param rendu renderer 
 * @param currText texture courante du personnage
 * @param spritePos position du sprite du personnage dans le renderer
 * @param spriteSize taille du sprite du personnage
 * @param perso pointeur sur le personnage étudié
 * @param mouvValide détermine si le mouvement est posible ou non
 * @param phaseSprite phase de l'animation du sprite du personnage
 */
void affichePerso(SDL_Renderer *rendu, SDL_Texture *currText, SDL_Rect spritePos, SDL_Rect spriteSize, personnage_t *perso, int mouvValide,int phaseSprite);

/**
 * @brief chargement de la texture du personnage
 * 
 * @param fenetre 
 * @param rendu 
 * @return SDL_Texture** texture chargé
 */
SDL_Texture ** loadTextPersos(SDL_Window * fenetre, SDL_Renderer * rendu);

/**
 * @brief libération en mémoire des textures de personnage
 * 
 * @param persoText texture à libérer
 */
void libereTextPersos(SDL_Texture **persoText);


#endif
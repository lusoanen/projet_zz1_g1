/**
 * \file baseSDL.h
 * \brief Fonction d'inintialisation et de fermeture de la SDL
 */

#ifndef _BASE_SDL_H_
#define _BASE_SDL_H_

#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

/**
 * @brief initialisation de la SDL
 * 
 * @param fenetre 
 * @param rendu renderer
 * @param ecran 
 */
void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran); 

/**
 * @brief fermeture de la SDL et gestion des erreurs
 * 
 * @param ok 
 * @param message accompagne l'explication de la valeur du ok
 * @param fenetre 
 * @param rendu renderer
 */
void fermetureSdl(int ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu);

#endif
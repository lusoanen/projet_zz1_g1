/**
 * \file bool.h
 */

#ifndef _BOOL_H
#define _BOOL_H

/**
 * @brief énumération du type booléen
 * 
 */
typedef enum
{
    FALSE, // = 0
    TRUE   // = 1
} bool_t;

#endif
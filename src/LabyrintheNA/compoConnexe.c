#include "compoConnexe.h"

int ** initMatrice(int n){
	srand(time(NULL));
	int **matrice = malloc(sizeof(int*)*n);
	if(matrice){
		for(int i = 0; i < n; i++){
			matrice[i] = malloc(sizeof(int)*n);
		}
	}

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if(rand()%15 == 0)
				matrice[i][j] = 1;
		}
	}
	return matrice;
}


void afficherMatrice(int **matrice, int n){
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			printf("%d ", matrice[i][j]);
		}
		printf("\n");
	}
}

void libererTab(int **matrice, int n){
	for(int i = 0; i < n; i++){
			free(matrice[i]);
	}
}

graphe_t * initGraphe(int n){
	srand(time(NULL));
	graphe_t *graphe = malloc(sizeof(graphe_t));

	if(graphe){
		graphe->nbNoeud = n;

		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				if(rand()%2 == 0 && i!=j){
					cellule2_t * nouv = AllocationMaillon2(i, j, 1);
					ADJ_CEL2(&(graphe->tabArete), nouv);
				}
			}
		}
	} else{
		fprintf(stderr, "Probleme malloc graphe\n");
	}
	return graphe;
}

graphe_t * initGrapheGrille(int nbLigne, int nbCol){
	graphe_t *graphe = malloc(sizeof(graphe_t));
	int n = nbLigne * nbCol;

	if(graphe){
		graphe->nbNoeud = n;

		for(int i = 0; i < n; i++){
			if(((i+1)/nbCol != 1 && (i+1)%nbCol != 0) || (i >= nbCol && i+1 < nbCol*2 ) ){
				cellule2_t * nouv = AllocationMaillon2(i, i+1, 1);
				ADJ_CEL2(&(graphe->tabArete), nouv);
			}

			if((i+nbCol)/nbCol < nbLigne){
				cellule2_t * nouv = AllocationMaillon2(i, i+nbCol, 1);
				ADJ_CEL2(&(graphe->tabArete), nouv);
			}
		}
	} else{
		fprintf(stderr, "Probleme malloc graphe\n");
	}
	return graphe;
}

void afficherGraphe(graphe_t *graphe){
	AfficheListe2(&(graphe->tabArete));
}

void uneDVersDeuxD(int a, int nbColonne, int *x, int *y){
	*y = floor(a/nbColonne);
	*x = a%nbColonne; 
}

int deuxDVersUneD(int nbColonne, int x, int y){
	return (y*nbColonne) + x;
}
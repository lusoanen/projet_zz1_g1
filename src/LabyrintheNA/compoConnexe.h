/**
 * \file compoConnexe.h
 * \brief Implémentation des graphes
 */

#ifndef _COMPOCON_H_
#define _COMPOCON_H_

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "liste2.h"
#include "partition.h"


typedef struct graphe{
	int nbNoeud; /**<nombre de noeuds dans le graphe */
	cellule2_t * tabArete; /**< liste des arretes dans le graphe */
}graphe_t;

/**
 * @brief initialisation de la matrice d'adjacence
 * 
 * @param n nombre d'élément 
 * @return int** tableau en 2d
 */
int ** initMatrice(int n);

/**
 * @brief affiche la matrice d'adjacence
 * 
 * @param matrice matrice à afficher
 * @param n nombre d'éléments
 */
void afficherMatrice(int **matrice, int n);

/**
 * @brief libétation de la mémoire allouée à de la matrice d'adjacence
 * 
 * @param matrice matrice d'adjacence
 * @param n nombre d'élément
 */
void libererTab(int **matrice, int n);

/**
 * @brief initialisation d'un graphe random
 * 
 * @param n nombre d'élément
 * @return graphe_t* graphe construit
 */
graphe_t * initGraphe(int n);

/**
 * @brief initialisation d'un graphe sous forme d'une grille
 * 
 * @param nbLigne nombre de ligne voulu
 * @param nbCol nombre de colonne voulu
 * @return graphe_t* 
 */
graphe_t * initGrapheGrille(int nbLigne, int nbCol);

/**
 * @brief affichage du graphe dans le terminal
 * 
 * @param graphe graphe à afficher
 */
void afficherGraphe(graphe_t *graphe);

/**
 * @brief traduit un indice de tableau 1d aux indices d'un tableau 2d
 *
 * @param  a indice dans le tableau 1d 
 * @param  nbColonne  nombre de colonne dans le tableau 2d
 * @param      x    indice de la colonne correspondante dans le tableu 2d
 * @param      y    indice de la ligne correspondante dans le tableu 2d
 */
void uneDVersDeuxD(int a, int nbColonne, int *x, int *y);

/**
 * @brief traduit les indices de tableau 2d à un indice detableau 1d
 *
 * @param[in]  nbColonne  nombre de colonne du tableau 2d
 * @param[in]  x     indice de la colonne correspondante dans le tableu 2d
 * @param      y    indice de la ligne correspondante dans le tableu 2d
 *
 * @return    indice dans le tableau 1d
 */
int deuxDVersUneD(int nbColonne, int x, int y);
#endif
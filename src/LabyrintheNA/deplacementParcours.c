#include "deplacementParcours.h"

direction_t directionDijkstra(graphe_t *graphe, personnage_t *persoDepart, personnage_t *persoCible, int nbColonnes)
{
    int noeudDepart = persoDepart->positionMapX + nbColonnes * persoDepart->positionMapY;
    int noeudCible = persoCible->positionMapX + nbColonnes * persoCible->positionMapY;
    direction_t direction;
    int *fils = dijkstra(graphe, noeudCible, noeudDepart); //on inverse pour avoir les fils plutot que les peres et acceder directement a la case souhaitee
    //printf("fils[noeudDepart] %d\tnoeudDepart %d\n", fils[noeudDepart], noeudDepart);
    int xDep, yDep, xCible, yCible;
    uneDVersDeuxD(noeudDepart, nbColonnes, &xDep, &yDep);
    uneDVersDeuxD(fils[noeudDepart], nbColonnes, &xCible, &yCible);
    //printf("dep x %d y %d\tcible x %d y %d\n", xDep, yDep, xCible, yCible);

    if (xCible != xDep)
    {
        if (xCible > xDep)
            direction = EST;
        else
            direction = OUEST;
    }
    else
    {
        if (yCible > yDep)
            direction = SUD;
        else
            direction = NORD;
    }
    free(fils);
    return direction;
}




direction_t directionAStar(graphe_t * graphe, personnage_t * persoDepart,personnage_t * persoCible,float (*dist)(int, int, int),int nbColonnes){
    int noeudDepart = (persoDepart->positionMapX) + nbColonnes * (persoDepart->positionMapY);
    int noeudCible = (persoCible->positionMapX) + nbColonnes * (persoCible->positionMapY);
    direction_t direction;
    int *fils = aStar(graphe, noeudCible, noeudDepart,dist,nbColonnes); //on inverse pour avoir les fils plutot que les peres et acceder directement a la case souhaitee
    //printf("fils[noeudDepart] %d\tnoeudDepart %d\n", fils[noeudDepart], noeudDepart);
    int xDep, yDep, xCible, yCible;
    uneDVersDeuxD(noeudDepart, nbColonnes, &xDep, &yDep);
    uneDVersDeuxD(fils[noeudDepart], nbColonnes, &xCible, &yCible);
    //printf("dep x %d y %d\tcible x %d y %d\n", xDep, yDep, xCible, yCible);

    if (xCible != xDep)
    {
        if (xCible > xDep)
            direction = EST;
        else
            direction = OUEST;
    }
    else
    {
        if (yCible > yDep)
            direction = SUD;
        else
            direction = NORD;
    }
    free(fils);
    return direction;
}

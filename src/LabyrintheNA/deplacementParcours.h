/**
 * \file deplacementParcours.h
 * \brief Gestion déplacement avec Dijkstra ou A*
 */

#ifndef _DEPLACEMENT_PARCOURS_H_
#define _DEPLACEMENT_PARCOURS_H_

#include <stdio.h>
#include "direction.h"
#include "personnage.h"
#include "typePerso.h"
#include "parcours.h"
#include "labyNonArbo.h"

direction_t directionDijkstra(graphe_t * graphe, personnage_t * persoDepart,personnage_t * persoCible,int nbColonnes);

direction_t directionAStar(graphe_t * graphe, personnage_t * persoDepart,personnage_t * persoCible,float (*dist)(int, int, int),int nbColonnes);

#endif
/**
 * \file direction.h
 * \brief Enum pour la gestion des directions
 */

#ifndef _DIRECTION_H_
#define _DIRECTION_H_ 

/**
 * @brief direction posible dans le labyrinthe (valeur des élément de l'énum en binaire)
 * 
 */
typedef enum direction 
{
	NORD  = 1,
	SUD   = 2,
	EST   = 4,
	OUEST = 8
} direction_t;

#endif
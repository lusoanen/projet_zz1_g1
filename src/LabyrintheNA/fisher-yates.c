#include "fisher-yates.h"

void fisher_yates(type_t * tab, int taille)
{
	srand(time(0));	
	int j;
	for (int i=taille; i > 0; i--)
	{
		j = rand()%i;
		type_t temp = tab[i-1];
		tab[i-1] = tab[j];
		tab[j] = temp;
	}
}

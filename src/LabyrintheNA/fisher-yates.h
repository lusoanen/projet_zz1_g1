/**
 * \file fisher-yates.h
 * \brief Implémentation de l'algorithme de <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">Fisher-Yates</a>
 */



#ifndef _FISHER_YATES_H_
#define _FISHER_YATES_H_ 

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "type.h"


/**
 * \fn void fisher_yates(type_t * tab, int taille)
 * \brief Mélange sur place le contenu d'un tableau selon l'algorithme de <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle">Fisher-Yates</a>
 * \param tab    Tableau dont on veut mélanger le contenu
 * \param taille Taille du tableau
 */
void fisher_yates(type_t * tab, int taille);

#endif
#include "fond_v1.h"

void dessine_mur(SDL_Renderer * renderer, direction_t cote, int case_x, int case_y, int largeur_case, int hauteur_case, int epaisseur_mur)
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_Rect rect_hor = { case_x, case_y, largeur_case, epaisseur_mur};
	SDL_Rect rect_ver = { case_x, case_y, epaisseur_mur, hauteur_case};
	if (cote & NORD)
	{
		SDL_RenderFillRect(renderer, &rect_hor);
	}
	if (cote & SUD)
	{
		rect_hor.y += hauteur_case - epaisseur_mur;
		SDL_RenderFillRect(renderer, &rect_hor);
	}
	if (cote & OUEST)
	{
		SDL_RenderFillRect(renderer, &rect_ver);
	}
	if (cote & EST)
	{
		rect_ver.x += largeur_case - epaisseur_mur;
		SDL_RenderFillRect(renderer, &rect_ver);
	}
}

void dessine_fond(SDL_Window * fenetre, SDL_Renderer * renderer, piece_t ** lab, int nb_ligne, int nb_colonne)
{
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderClear(renderer);
	SDL_RenderFillRect(renderer, NULL);

	int hauteur_case, largeur_case;
	SDL_GetWindowSize(fenetre, &largeur_case, &hauteur_case);
	hauteur_case /= nb_ligne;
	largeur_case /= nb_colonne;
	int epaisseur_mur = 1;

	for (int i=0; i < nb_ligne; i++)
	{
		for (int j=0; j < nb_colonne; j++)
		{
			dessine_mur(renderer, lab[i][j].murs, largeur_case*j, hauteur_case*i, largeur_case, hauteur_case, epaisseur_mur);
		}
	}
}


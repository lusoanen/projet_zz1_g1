/**
 * \file fond_v1.h
 * \brief Fonctions d'affichage du fond du labyrinthe avec des traits
 */

#ifndef _FOND_V1_H_
#define _FOND_V1_H_ 

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "direction.h"
#include "type.h"


/**
 * \fn void dessine_fond(SDL_Window * fenetre, SDL_Renderer * renderer, direction_t ** lab, int nb_ligne, int nb_colonne)
 * \brief Dessine le labyrinthe
 * 
 * @param fenetre    Fenetre dans laquelle on dessine (sert pour fermer en cas de problème)
 * @param renderer   Rendu dans lequel dessiner
 * @param lab        Matrice représentant le labyrinthe (une case contient la concaténation des directions des murs de cette case)
 * @param nb_ligne   Nombre de lignes de la matrice lab
 * @param nb_colonne Nombre de colonnes de la matrice lab
 */
void dessine_fond(SDL_Window * fenetre, SDL_Renderer * renderer, piece_t ** lab, int nb_ligne, int nb_colonne);


/**
 * \fn void dessine_mur(SDL_Renderer * renderer, direction_t cote, int case_x, int case_y, int largeur_case, int hauteur_case, int epaisseur_bord)
 * \brief Fonction dessinant les murs d'une case
 * 
 * @param renderer       Rendu sur lequel dessiner
 * @param cote           Somme des direction des murs à afficher (ex: NORD, NORD+OUEST, SUD+EST+OUEST, ...)
 * @param case_x         Abscisse en pixels du coin supérieur gauche de la case dont veut afficher les murs
 * @param case_y         Ordonnée en pixels du coin supérieur gauche de la case dont veut afficher les murs
 * @param largeur_case   Largeur en pixels de la case à afficher
 * @param hauteur_case   Hauteur en pixels de la case à afficher
 * @param epaisseur_mur  Epaisseur en pixels du mur
 */
void dessine_mur(SDL_Renderer * renderer, direction_t cote, int case_x, int case_y, int largeur_case, int hauteur_case, int epaisseur_mur);

#endif
#include "fond_v2.h"

const char * nom_img[6] =  {"herbe",
							"fleurs_b",
							"fleurs_r",
							"fleurs_bl",
							"barriere_face",
							"barriere_cote"};

int type_herbe(int nb_types, int freq_herbe)
{
	int type = 0;
	int alea = rand()%101;

	if (alea > freq_herbe)
	{
		type = 1 + rand()%(nb_types-1);
	}
	return type;
}

SDL_Texture * charge_texture_herbe(SDL_Window * fenetre, SDL_Renderer * renderer, int nb_ligne, int nb_colonne)
{
	int ok = 1;
	int i = 0;
	char chemin[50];
	char file_name[20];
	SDL_Surface * img[4];
	SDL_Surface * surf_herbe = NULL;
	SDL_Texture * herbe = NULL;

	while (ok && i < 4)
	{
		sprintf(file_name,"%s.png", nom_img[i]);
		sprintf(chemin,"./asset/Tiles/%s", file_name);
		img[i] = IMG_Load(chemin);
		if (img[i] == NULL)
		{
			ok = 0;
			for (int j=0; j < i; j++)
			{
				SDL_FreeSurface(img[j]);
			}
			fermetureSdl(0, "Problème de chargement d'une image de fond", fenetre, renderer);
		}
		i++;
	}

	if (ok)
	{
		surf_herbe = SDL_CreateRGBSurface(0, nb_colonne*16, nb_ligne*16, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
		if (surf_herbe != NULL)
		{			
			SDL_Rect position = { 0, 0, 16, 16};

			for (int i=0; i < nb_ligne; i++)
			{
				position.y = 16*i;
				for (int j=0; j < nb_colonne; j++)
				{
					position.x = 16*j;
					SDL_BlitSurface(img[type_herbe(4,85)], NULL, surf_herbe, &position);
				}
			}
			herbe = SDL_CreateTextureFromSurface(renderer, surf_herbe);
			SDL_FreeSurface(surf_herbe);
		}
		else 
		{
			ok = 0;
			fermetureSdl(0, "Problème de chargement d'une image de fond", fenetre, renderer);
		}
		for (int i=0; i < 4; i++)
		{
			SDL_FreeSurface(img[i]);
		}
	}
	return herbe;
}

SDL_Texture ** charge_barriere(SDL_Window * fenetre, SDL_Renderer * renderer)
{
	int ok = 1;
	int i = 0;
	char chemin[50];
	char file_name[20];

	SDL_Texture ** barrieres = (SDL_Texture **) malloc(sizeof(SDL_Texture *)*2);
	if (barrieres == NULL)
		fermetureSdl(0, "Problème d'allocation du tableau des textures de barrieres", fenetre, renderer);

	while(ok && i < 2)
	{
		sprintf(file_name,"%s.png", nom_img[i+4]);
		sprintf(chemin,"./asset/Tiles/%s", file_name);
		barrieres[i] = IMG_LoadTexture(renderer, chemin);
		if (barrieres[i] == NULL)
		{
			ok = 0; 
			for (int j=0; j < i; j++)
			{
				SDL_DestroyTexture(barrieres[j]);
			}
			fermetureSdl(0, "Problème de chargement d'une texture de barriere", fenetre, renderer);
		}
		i++;
	}
	return barrieres;
}

void dessine_barrieres(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture ** barrieres, piece_t ** lab, int nb_ligne, int nb_colonne, int largeur_case, int hauteur_case)
{
	SDL_Rect position = { 0, 0, largeur_case, hauteur_case};

	for (int i=0; i < nb_ligne; i++)
	{
		position.y = i*hauteur_case;
		for (int j=0; j < nb_colonne; j++)
		{
			position.x = j*largeur_case;
			direction_t murs = lab[i][j].murs;
			if (murs & EST)
			{
				SDL_RenderCopy(renderer, barrieres[1], NULL, &position);
			}
			if (murs & SUD)
			{
				SDL_RenderCopy(renderer, barrieres[0], NULL, &position);
			}
		}
	}
}
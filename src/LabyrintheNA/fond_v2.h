/**
 * \file fond_v2.h
 * \brief Fonctions d'affichage du fond du labyrinthe avec des textures
 */

#ifndef _FOND_V2_H_
#define _FOND_V2_H_ 

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "baseSDL.h"
#include "direction.h"
#include "type.h"

/**
 * @brief chargement de la texture herbe (font de la map)
 * 
 * @param fenetre 
 * @param renderer 
 * @param nb_ligne nombre de ligne de la matrice des pièces du labyrinthe
 * @param nb_colonne nombre de colonne de la matrice des pièces du labyrinthe
 * @return SDL_Texture* texture chargée
 */
SDL_Texture * charge_texture_herbe(SDL_Window * fenetre, SDL_Renderer * renderer, int nb_ligne, int nb_colonne);

/**
 * @brief chargement de la texture herbe (murs de la map)
 * 
 * @param fenetre 
 * @param renderer 
 * @return SDL_Texture** texture chargée
 */
SDL_Texture ** charge_barriere(SDL_Window * fenetre, SDL_Renderer * renderer);

/**
 * @brief dessine les barrières sur la map
 * 
 * @param fenetre 
 * @param renderer 
 * @param barrieres pointeur ur un tableau contenant les textures de barrières chargée
 * @param lab labyrinthe (matrice des pièces)
 * @param nb_ligne nombre de ligne de la matrice
 * @param nb_colonne nombre de colonne de la matrice 
 * @param largeur_case largeur d'une pièce 
 * @param hauteur_case hauteur d'une pièce
 */
void dessine_barrieres(SDL_Window * fenetre, SDL_Renderer * renderer, SDL_Texture ** barrieres, piece_t ** lab, int nb_ligne, int nb_colonne, int largeur_case, int hauteur_case);


#endif
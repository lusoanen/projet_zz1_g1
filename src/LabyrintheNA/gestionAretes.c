#include "gestionAretes.h"

void trieArete(type_t * tabArete, int taille){
	(void)tabArete;
	(void)taille;
}


int compteurArete(graphe_t * g){
	int cmp = 0;
	type_t * cour = g->tabArete;
	while(cour){
		cour = cour->suivant;
		cmp++;
	}

	return cmp;
}

void afficherForet(type_t * foret){
	type_t * cour = foret;
	while(cour){
		printf("%d -- %d\n", cour->ext1, cour->ext2);
		cour = cour->suivant;
	}
}

type_t * aretesParP(int n,void (*trie)(type_t*, int), graphe_t *graphe, float p){

	type_t *A = NULL;

	if(graphe){

		int nbArete = compteurArete(graphe);
		type_t * tmp = malloc(sizeof(type_t)*nbArete);
		if(tmp){
			int i = 0;
			type_t *cour = graphe->tabArete;

			while(cour){
				tmp[i].ext1 = cour->ext1;
				tmp[i].ext2 = cour->ext2;

				i++;
				cour = cour->suivant;
			}

			(*trie)(tmp,i);

			partition_t * partition = creer_partition(n);

			if(partition){
				int classe1, classe2;
				srand(time(NULL));
				float alpha ;
				int entier;

				for(int i = 0; i <nbArete; i++){
					entier = rand()%100;
					alpha = (float)entier/100;

					classe1 = recuperer_classe(partition, tmp[i].ext1);
					classe2 = recuperer_classe(partition, tmp[i].ext2);

					if(alpha < p || classe2 != classe1){
						fusionner(partition, tmp[i].ext1, tmp[i].ext2);
						cellule2_t * nouv = AllocationMaillon2(tmp[i].ext1, tmp[i].ext2, 1);
						if(nouv)
							ADJ_CEL2(&A, nouv);
					}
				}
				libere_partition(partition);
			}

			free(tmp);
		}
	}

	type_t *cour = A;
	graphe->tabArete = NULL; //pb avec libererListe2

	while(cour != NULL){
		cellule2_t * nouv = AllocationMaillon2(cour->ext1, cour->ext2, 1);
		if(nouv)
			ADJ_CEL2(&(graphe->tabArete), nouv);
		cour = cour->suivant;
	}
	return A;
}


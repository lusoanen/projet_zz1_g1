/**
 * \file gestionAretes.h
 * \brief Gestion des arêtes d'un graphe
 */

#ifndef _ARETE_H_
#define _ARETE_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "compoConnexe.h"
#include "partition.h"
#include "liste2.h"
#include "liste.h"
#include "type.h"


/**
 * @brief fonction fictive de trie des aretes (valuation à 1)
 * 
 * @param tabArete tableau des aretes
 * @param taille taille du tableau
 */
void trieArete(type_t * tabArete, int taille);

/**
 * @brief compteur du nombre d'arete dans un graphe
 * 
 * @param g graphe étudié
 * @return int nombre d'arete
 */
int compteurArete(graphe_t * g);

/**
 * @brief suppression d'arete celon le parametre p pour faire l'arbre couvrant
 * 
 * @param n nombre d'élément
 * @param trie pointeur sur la fonction de tri choisie
 * @param g graphe
 * @param p densité d'arete voulue 
 * @return type_t* 
 */
type_t * aretesParP(int n,void (*trie)(type_t*, int), graphe_t *g, float p);

/**
 * @brief affichage dans le terminal de l'arbre (ou foret) couvrante du graphe
 * 
 * @param foret 
 */
void afficherForet(type_t * foret);


#endif
#include "gestionDeplacement.h"

bool_t estBord(int pos, int max, int sens)
{
	int resultat = pos + sens;
	bool_t estBord = TRUE;
	if (resultat >= 0 && resultat < max)
		estBord = FALSE;

	return estBord;
}

int estEnnemi(int x, int y, piece_t **grille, personnage_t **perso, int max,int idPerso)
{
	int degat = -1;
	//printf("%d %d\n",x,y );
	for (int i = 0; i < max; i++)
	{
		if (estVivant(perso[i]) == TRUE && perso[i]->positionMapX == x && perso[i]->positionMapY == y)
		{
			subirAttaque(perso[i],perso[idPerso]->attaque);
			degat =perso[idPerso]->attaque;
		}
	}

	return degat;
}

int peutBouger(piece_t **grille, direction_t direction, personnage_t *perso)
{
	return (!(grille[perso->positionMapY][perso->positionMapX].murs & direction));
}

/*
bool_t estCoffre(int x, int y, piece_t **grille)
{
	bool_t estCoffre = FALSE;
	if (grille[y][x] == COFFRE)
	{
		estCoffre = TRUE;
	}
	return estCoffre;
}
*/

bool_t deplacerPersonnage(int indice, piece_t **grille, int nbLigne, int nbColonne, direction_t direction, personnage_t **persos, int nbPerso)
{
	bool_t caseAccessible = TRUE;
	int sens = -1;
	typePerso_t type = getType(persos[indice]);
	if (direction == SUD || direction == EST)
		sens = 1;

	if (direction == NORD || direction == SUD)
	{
		if (estBord(persos[indice]->positionMapY, nbLigne, sens) || !(type == MOUETTE || peutBouger(grille, direction, persos[indice])))
		{
			caseAccessible = FALSE;
		}
		else
		{

			int degat = estEnnemi(persos[indice]->positionMapX, (persos[indice]->positionMapY) + sens, grille, persos, nbPerso,indice);
			if (degat >= 0)
			{
				caseAccessible = FALSE;
			}
			else
			{
				/*
				if (estCoffre(persos[indice]->positionMapX, (persos[indice]->positionMapY) + sens, grille) == TRUE)
				{
					if (persos[indice]->type == PLAYER)
					{
						setAttaque(persos[indice], 2);
						seDeplacer(persos[indice], direction);
					}
					else
					{
						caseAccessible = FALSE;
					}
				}
				else
				{*/
				seDeplacer(persos[indice], direction);
			}
		}
	}
	else
	{
		if (estBord(persos[indice]->positionMapX, nbColonne, sens) ||!(type == MOUETTE || peutBouger(grille, direction, persos[indice])))
		{
			caseAccessible = FALSE;
		}
		else
		{
			int degat = estEnnemi(persos[indice]->positionMapX + sens, persos[indice]->positionMapY, grille, persos, nbPerso,indice);
			if (degat >= 0)
			{
				caseAccessible = FALSE;
			}
			else
			{ /*
				if (estCoffre(persos[indice]->positionMapX + sens, persos[indice]->positionMapY, grille) == TRUE)
				{
					if (persos[indice]->type == PLAYER)
					{
						setAttaque(persos[indice], 2);
						seDeplacer(persos[indice], direction);
					}
					else
					{
						caseAccessible = FALSE;
					}
				}
				else
				{*/
				seDeplacer(persos[indice], direction);
			}
		}
	}

	persos[indice]->regard = direction;
	return caseAccessible;
}
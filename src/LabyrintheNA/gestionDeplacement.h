/**
 * \file gestionDeplacement.h
 * \brief Gestion du déplacement des personnages
 */

#ifndef _GESTION_D_H_
#define _GESTION_D_H_

#include <stdio.h>
#include <stdlib.h>
#include "personnage.h"
#include "bool.h"
#include "fond_v2.h"
#include "direction.h"
#include "type.h"
#include <time.h>

/**
 * @brief verifie si il y a un ennemie sur la case que l'on souhaite atteindre 
 * 
 * @param x indice X de là où l'on souhaite aller sur la map
 * @param y indice Y de là où l'on souhaite aller sur la map
 * @param grille grille 2D représentant la map en mémoire
 * @param perso tableau des coordonnées de tous les personnages du jeu
 * @param max nombre de personnage dans le jeu
 * @param idPerso identifiant du personnage en mouvement
 * @return dégats infligés par l'ennemi
 */
int estEnnemi(int x, int y, piece_t ** grille, personnage_t ** perso, int max,int idPerso);

/**
 * @brief verifie sur là où l'on souhaite aller sur la map est un bord ou non
 * 
 * @param pos indice X (ou Y) dans la map
 * @param max nombre de colonne (ou ligne) dans la map
 * @param sens definit si le sens du déplacement (-1 ou 1)
 * @return est un bord ou non
 */
bool_t estBord(int pos, int max, int sens);

/**
 * @brief retourne si le déplacement du personnage dans une direction donnée est possible ou non
 * 
 * @param grille matrice des pièces du labyrinthe
 * @param direction direction dans laquelle le personnage veut aller
 * @param perso personnage étudié
 * @return int déplacement possible ou non
 */
int peutBouger(piece_t** grille, direction_t direction,personnage_t * perso);

/**
 * @brief deplace le personnage si cela est possible et inflige des dégats si il rencontre un ennemi
 * 
 * @param indice indice du personnage dans le tableau de personnage
 * @param grille matrice des pièces du labyrinthe
 * @param nbLigne nombre d eligne d ela matrice
 * @param nbColonne nombre de colonne de la matrice
 * @param direction direction de déplacement du personnage
 * @param persos tableaux de tout les personnages du jeu
 * @param nbPerso nombre de personnage dans le jeu
 * @return bool_t le personnage peut se déplacer dans cette direction ou non
 */
bool_t deplacerPersonnage(int indice, piece_t ** grille, int nbLigne, int nbColonne, direction_t direction, personnage_t ** persos, int nbPerso);

#endif
/**
 * \file graphviz.h
 * \brief Visualisation via graphviz
 */

#ifndef _GRAPHVIZ_H_
#define _GRAPHVIZ_H_

#include <stdlib.h>
#include <stdio.h>
#include "partition.h"
#include "compoConnexe.h"

/**
 * @brief transforme un type patition en fichier.dot
 * 
 * @param partition partition à transformer
 */
void partitionVersDot(partition_t *partition);

/**
 * @brief transforme une matrice d'adjacence en fichier.dot
 * 
 * @param matrice matrice à transformer
 * @param n nombre d'élément
 */
void matriceVersDot(int **matrice, int n);

/**
 * @brief transforme un type graphe en fichier.dot
 * 
 * @param g graphe à transformer
 */
void grapheVersDot(graphe_t g);

/**
 * @brief transforme une foret en fichier .dot
 * 
 * @param foret foret à transformer
 */
void foretVersDot(cellule2_t * foret);

/**
 * @brief création du fichier jpg à partir de foret.dot
 * 
 */
void afficherForetGraphviz();

/**
 * @brief création du fichier jpg à partir de partition.dot
 * 
 */
void afficherPartitionGraphviz();

/**
 * @brief création du fichier jpg à partir de matrice.dot
 * 
 */
void afficherMatriceGraphviz();

/**
 * @brief création du fichier jpg à partir de graphe.dot
 * 
 */
void afficherGrapheGraphviz();

#endif
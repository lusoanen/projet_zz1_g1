#include "kruskal.h"

cellule2_t * kruskal(int n, void (*trie)(cellule2_t*, int), graphe_t *graphe){

	cellule2_t *A = NULL;

	if(graphe){

		int nbArete = compteurArete(graphe);
		cellule2_t * tmp = malloc(sizeof(cellule2_t)*nbArete);
		if(tmp){
			int i = 0;
			cellule2_t *cour = graphe->tabArete;

			while(cour){
				tmp[i].ext1 = cour->ext1;
				tmp[i].ext2 = cour->ext2;

				i++;
				cour = cour->suivant;
			}

			(*trie)(tmp,i);

			partition_t * partition = creer_partition(n);
			

			if(partition){
				int classe1, classe2;

				for(int i = 0; i <nbArete; i++){
					classe1 = recuperer_classe(partition, tmp[i].ext1);
					classe2 = recuperer_classe(partition, tmp[i].ext2);
					if(classe2 != classe1){
						fusionner(partition, tmp[i].ext1, tmp[i].ext2);
						cellule2_t * nouv = AllocationMaillon2(tmp[i].ext1, tmp[i].ext2, 1);
						if(nouv)
							ADJ_CEL2(&A, nouv);
					}
					
				}
				libere_partition(partition);
			}

			free(tmp);
		}
	}
	return A;
}


/**
 * \file kruskal.h
 * \brief Implémentation de l'algorithme de Kruskal
 */

#ifndef _KRUSKAL_H_
#define _KRUSKAL_H_

#include <stdlib.h>
#include <stdio.h>

#include "compoConnexe.h"
#include "gestionAretes.h"
#include "partition.h"
#include "liste2.h"
#include "liste.h"

/**
 * @brief foction fictive de trie des arretes (valuation à 1 donc non triable)
 * 
 * @param tabArete tableau des arretes à triées
 * @param taille taille du tableau
 */
void trieArete(cellule2_t * tabArete, int taille);

/**
 * @brief compte le nombre d'arrete dans le graphe
 * 
 * @param g graphe à étudier
 * @return int nombre d'arete
 */
int compteurArete(graphe_t * g);

/**
 * @brief recherche de l'arbe couvrant sur un graphe
 * 
 * @param n nombre d'élément
 * @param trie pointeur sur la fonction de tri choisie
 * @param g graphe 
 * @return cellule2_t* liste des arete composant la foret couvrante
 */
cellule2_t * kruskal(int n,void (*trie)(cellule2_t*, int), graphe_t *g);

/**
 * @brief affichage dans le terminal des aretes composant la foret
 * 
 * @param foret foret à afficher
 */
void afficherForet(cellule2_t * foret);


#endif
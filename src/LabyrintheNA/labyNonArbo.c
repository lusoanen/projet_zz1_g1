#include "labyNonArbo.h"


bool_t estVoisin(matrice_t * matrice, int a, int b, direction_t * d){
	bool_t voisin = FALSE;

	int indiceY = a%matrice->nbCol;
	int indiceX = a/matrice->nbCol;

	if((indiceX-1) >= 0 && matrice->tabPiece[indiceX-1][indiceY].id == b){
		*d = NORD; 
		voisin = TRUE;
	}

	if((indiceX+1) < matrice->nbLigne && matrice->tabPiece[indiceX+1][indiceY].id == b){
		*d = SUD;
		voisin = TRUE;
	}

	if((indiceY-1) >= 0 && matrice->tabPiece[indiceX][indiceY-1].id == b ){
		*d = OUEST;
		voisin = TRUE;
	}

	if((indiceY+1) < matrice->nbCol && matrice->tabPiece[indiceX][indiceY+1].id == b){
		*d = EST;
		voisin = TRUE;
	}

	return voisin;
}

matrice_t * initMatriceLaby(int l, int c){
	matrice_t * nouv = malloc(sizeof(matrice_t));
	if(nouv){
		nouv->nbCol = c;
		nouv->nbLigne = l;

		nouv->tabPiece = malloc(sizeof(piece_t*)*l);
		for(int i = 0; i < l ;i++){
			nouv->tabPiece[i] = malloc(sizeof(piece_t)*c);
		}

		int cpt = 0;
		for(int i = 0; i < l ;i++){
			for(int j = 0; j < c ;j++){
				nouv->tabPiece[i][j].id = cpt;
				nouv->tabPiece[i][j].murs = 15;
				cpt++;
			}
		}

	}
	return nouv;
}

direction_t opposer(direction_t d){
	if(d == NORD || d == EST)
		return d*2;
	else
		return d/2;

}

matrice_t * creerLabyrintheNA(int l, int c, float p, graphe_t * g){
	matrice_t *m = initMatriceLaby(l,c);
	void (*pf)(type_t*,int);
	pf = fisher_yates;

	if(m != NULL && g != NULL){
		type_t * couvrant = aretesParP(l*c, pf, g, p);

		type_t * cour = couvrant;
		direction_t voisin;

		while(cour){
			if(estVoisin(m, cour->ext1, cour->ext2, &voisin) == TRUE){
				
				m->tabPiece[  cour->ext1 / m->nbCol ][cour->ext1 % m->nbCol ].murs -= (int)voisin;
				m->tabPiece[  cour->ext2 / m->nbCol ][cour->ext2 % m->nbCol ].murs -= (int)opposer(voisin);	
			}
			cour = cour->suivant;
		}
	}
	return m;
	
}


void afficherMatriceLaby(matrice_t *m){
	for(int i= 0 ; i< m->nbLigne; i++){
		for(int j=0; j < m->nbCol; j++){
			printf("%d(%d) ", m->tabPiece[i][j].id, m->tabPiece[i][j].murs);
		}
		printf("\n");
	}
}

void libererMatrice(matrice_t *m)
{
	for (int i=0; i < m->nbLigne; i++)
	{
		free(m->tabPiece[i]);
	}
	free(m->tabPiece);
	free(m);
}
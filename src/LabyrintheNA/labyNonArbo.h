/**
 * \file labyNonArbo.h
 * \brief Implémentation du labyrinthe non arborescent
 */

#ifndef _LABINONARBO_H_
#define _LABINONARBO_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "gestionAretes.h"
#include "compoConnexe.h"
#include "liste2.h"

#include "bool.h"
#include "fisher-yates.h"
#include "type.h"
#include "direction.h"

#include "graphviz.h"

typedef struct matrice
{
	int nbLigne; /**< nombre de ligne de la matrice */
	int nbCol; /**< nombre de colonne de la matrice */
	piece_t ** tabPiece; /**< matrice des pièces du labyrinthe */
}matrice_t;

/**
 * @brief determine si deux indices d'un tableau 1d sont voisin dans un tableau 2d
 * 
 * @param matrice matrice des pièces
 * @param a indice première pièce
 * @param b indice deuxième pièce
 * @param d direction dans laquelle les deux case sont voisines
 * @return bool_t est voisin ou non
 */
bool_t estVoisin(matrice_t * matrice, int a, int b, direction_t * d);

/**
 * @brief initialisation de la matrice des pièces du labyrinthe
 * 
 * @param l nombre de ligne de la matrice
 * @param c nombre de colonne de la matrice
 * @return matrice_t* matrice créer
 */
matrice_t * initMatriceLaby(int l, int c);

/**
 * @brief retourne la direction opposé à celle donnée
 * 
 * @param d direction initiale
 * @return direction_t direction opposée
 */
direction_t opposer(direction_t d);

/**
 * @brief création du labyrinthe non arborescent
 * 
 * @param l nombre de ligne  
 * @param c nombre de colonne
 * @param p densité de murs dans le labyrinthe
 * @param g graphe du labyrinthe
 * @return matrice_t* matrice des pièces du labyrhinthe
 */
matrice_t * creerLabyrintheNA(int l, int c, float p, graphe_t *g);

/**
 * @brief affichage dans le terminal de la matrice des pièces
 * 
 * @param m matrice
 */
void afficherMatriceLaby(matrice_t *m);

/**
 * @brief libération de la matrice en mémoire
 * 
 * @param m matrice à libérer
 */
void libererMatrice(matrice_t *m);

#endif
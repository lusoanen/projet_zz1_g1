/**
 * \file liste.h
 * \brief Implémentation de liste chainée avec une seule valeur dans un maillon
 */

#ifndef _LISTE_H_
#define _LISTE_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct cellule
{
    int                val; /**< valeur de la cellule */
    struct cellule   * suivant; /** pointeur sur la cellule suivante */
}cellule_t;

/**
 * @brief allocation en mémoire d'une cellule
 * 
 * @param x valeur de la cellule
 * @return cellule_t* cellule crée
 */
cellule_t * AllocationMaillon(int x);

/**
 * @brief ajout d'une cellule dans la liste chainée
 * 
 * @param prec adresse du précédent
 * @param elt élément à ajouter
 */
void ADJ_CEL(cellule_t **prec, cellule_t *elt);

/**
 * @brief suppression d'une cellule dans la liste chainée
 * 
 * @param prec adresse du précédent
 */
void SUP_CEL(cellule_t **prec);

/**
 * @brief libération en mémoire de la liste
 * 
 * @param tete adresse de la tête
 */
void LibererListe(cellule_t **tete);

/**
 * @brief affiche dans le terminal la liste chainée
 * 
 * @param tete adresse de la tête
 */
void AfficheListe(cellule_t **tete);

/**
 * @brief inseretion en tête d'un élément 
 * 
 * @param tete adresse de la tête 
 * @param elt élément à inserer
 */
void insere_tete(cellule_t ** tete, cellule_t * elt);

#endif
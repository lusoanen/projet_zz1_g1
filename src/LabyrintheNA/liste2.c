#include "liste2.h"

cellule2_t * AllocationMaillon2(int a, int b, int v)
{
    cellule2_t * elt = (cellule2_t*) malloc(sizeof(cellule2_t));
    if (elt != NULL)
    {
        elt->ext1 = a;
        elt->ext2 = b;
        elt->valuation = v;
        elt->suivant = NULL;
    }
    return elt;
}

void ADJ_CEL2(cellule2_t **prec, cellule2_t *elt)
{
	elt->suivant = *prec;
	*prec = elt;
}

void SUP_CEL2(cellule2_t **prec)
{
    cellule2_t 		 * temp = *prec;
    *prec = temp->suivant;
    free(temp);
}

void LibererListe2(cellule2_t **tete)
{
    cellule2_t        * cour = *tete;
    cellule2_t        * prec = NULL;

    while(cour != NULL)
    {
        prec = cour;
        cour = cour->suivant;
        free(prec);
    }
}

void AfficheListe2(cellule2_t ** tete)
{
    cellule2_t        * cour = *tete;
    // printf("affichage de la liste :\n");
    while(cour != NULL)
    {
        printf("%d -- %d ",cour->ext1, cour->ext2);
        cour = cour->suivant;
    }
    printf("\n");
}

/**
 * \file liste2.h
 * \brief Implémentation de liste chainée pour représenter les arêtes d'un graphe
 */

#ifndef _LISTE2_H_
#define _LISTE2_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct cellule2
{
    int  ext1; /**< première extrémité d'une arrete */
    int ext2; /**< deuxième extrémité d'une arrete */
    int valuation; /**< poid de l'arete */
    struct cellule2   * suivant; /**< pointeur sur le suivant */
}cellule2_t;

/**
 * @brief allocation en mémoire d'une cellule
 * 
 * @param a valeur de la première extrémité de l'arrete
 * @param b valeur de la deuxieme extrémité de l'arrete
 * @param v valuation de l'arrete
 * @return cellule2_t* 
 */
cellule2_t * AllocationMaillon2(int a, int b, int v);

/**
 * @brief ajout d'une cellule dans la liste chainée
 * 
 * @param prec adresse du précédent
 * @param elt élément à ajouter
 */
void ADJ_CEL2(cellule2_t **prec, cellule2_t *elt);

/**
 * @brief suppression d'une cellule dans la liste chainée
 * 
 * @param prec adresse du précédent
 */
void SUP_CEL2(cellule2_t **prec);

/**
 * @brief libération en mémoire de la liste
 * 
 * @param tete adresse de la tête
 */
void LibererListe2(cellule2_t **tete);

/**
 * @brief affiche dans le terminal la liste chainée
 * 
 * @param tete adresse de la tête
 */
void AfficheListe2(cellule2_t **tete);
#endif
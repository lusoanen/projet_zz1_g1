#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "labyNonArbo.h"
#include "bool.h"
#include "baseSDL.h"
#include "direction.h"
#include "parcours.h"

#include "fond_v2.h"
#include "fond_v1.h"
#include "baseSDL.h"
#include "direction.h"
#include "animation.h"
#include "personnage.h"
#include "typePerso.h"
#include "gestionDeplacement.h"
#include "mainJeu.h"

int main()
{
	SDL_Window *fenetre = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_DisplayMode ecran;

	//SDL_Event event;
	int continuer = 1;

	Init(&fenetre, &renderer, &ecran);
	if (Mix_OpenAudio(45000, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1)
	{
		fermetureSdl(0, "ERROR GET MIX AUDIO", fenetre, renderer);
	}

	Mix_Music *musique;
	musique = Mix_LoadMUS("asset/musiqueAmbiance.mp3");
	int nb_ligne = 10, nb_colonne = 10;
	float taux = 0.25;
	int mode = -1;
	graphe_t *g = initGrapheGrille(nb_ligne, nb_colonne);

	matrice_t *lab = creerLabyrintheNA(nb_ligne, nb_colonne, taux, g);

	float (*distEu)(int, int, int);
	distEu = distanceEuclidienne;
	float (*distMan)(int, int, int);
	distMan = distanceManhattan;
	float (*distTcheby)(int, int, int);
	distTcheby = distanceTchebychev;

	int *res;
	int largeur_case, hauteur_case;
	int caseDep, caseFin;

	if (lab)
	{
		srand(time(NULL));
		while (continuer)
		{
			SDL_RenderClear(renderer);
			SDL_GetWindowSize(fenetre, &largeur_case, &hauteur_case);
			largeur_case /= nb_colonne;
			hauteur_case /= nb_ligne;
			dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne, lab->nbCol);
			SDL_RenderPresent(renderer);
			while (mode < 0 || mode > 4)
			{
				printf("1\tle jeu\n2\tA star\n3\tarret\n4\tnouveau labyrinthe\n");
				scanf("%d", &mode);
				SDL_RenderClear(renderer);
				dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne, lab->nbCol);
				SDL_RenderPresent(renderer);
			}
			switch (mode)
			{
			case 1:
				Mix_PlayMusic(musique, -1);
				mainJeu(fenetre, renderer, &ecran, g, lab, nb_ligne, nb_colonne);
				Mix_FadeOutMusic(15);
				mode = -1;
				break;

			case 2:
				caseDep = rand() % (nb_ligne * nb_colonne);
				caseFin = rand() % (nb_ligne * nb_colonne);


				// grapheVersDot(*g);
				// 	afficherGrapheGraphviz();
				printf("\nDans l'ordre\nDijkstra\nA star distance Euclidienne\nA star distance Manhattan\nA star distance Tchebychev\n\n");

				res = dijkstra_affiche(g, caseDep, caseFin, renderer, nb_ligne, nb_colonne, largeur_case, hauteur_case);
				affiche_chemin(res, caseDep, caseFin, renderer, nb_colonne, largeur_case, hauteur_case);
				free(res);
				dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne, lab->nbCol);
				res = aStar_affiche(g, caseDep, caseFin, distEu, renderer, nb_ligne, nb_colonne, largeur_case, hauteur_case);
				affiche_chemin(res, caseDep, caseFin, renderer, nb_colonne, largeur_case, hauteur_case);
				free(res);
				dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne, lab->nbCol);
				res = aStar_affiche(g, caseDep, caseFin, distMan, renderer, nb_ligne, nb_colonne, largeur_case, hauteur_case);
				affiche_chemin(res, caseDep, caseFin, renderer, nb_colonne, largeur_case, hauteur_case);
				free(res);
				dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne, lab->nbCol);
				res = aStar_affiche(g, caseDep, caseFin, distTcheby, renderer, nb_ligne, nb_colonne, largeur_case, hauteur_case);
				affiche_chemin(res, caseDep, caseFin, renderer, nb_colonne, largeur_case, hauteur_case);
				free(res);
				mode = -1;
				break;

			case 4:
				libererMatrice(lab);
				LibererListe2(&g->tabArete);
				g = initGrapheGrille(nb_ligne, nb_colonne);

				lab = creerLabyrintheNA(nb_ligne, nb_colonne, taux, g);
				mode = -1;
				break;
			default:
				continuer = 0;
				break;
			}
		}

		fermetureSdl(1, "", fenetre, renderer);
	}

	libererMatrice(lab);
	LibererListe2(&g->tabArete);
	return 0;
}

#include "mainJeu.h"

int mainJeu(SDL_Window *fenetre, SDL_Renderer *renderer, SDL_DisplayMode *ecran, graphe_t *g, matrice_t *lab, int ligne, int colonnes)
{
	srand(time(0));

	SDL_Event event;
	int continuer = 1;

	int largeur_case, hauteur_case;
	SDL_GetWindowSize(fenetre, &largeur_case, &hauteur_case);
	largeur_case /= lab->nbCol;
	hauteur_case /= lab->nbLigne;

	/*
	float (*distEu)(int, int, int);	
	distEu = distanceEuclidienne;
	*/
	/*
	float (*distMan)(int, int, int);
	distMan = distanceManhattan;
	*/
	float (*distTcheby)(int, int, int);
	distTcheby = distanceTchebychev;
	

	int nbPersonnages = 3, i;
	personnage_t **persos = (personnage_t **)malloc(nbPersonnages * sizeof(personnage_t *));
	persos[0] = initPlayer();
	persos[1] = initOurs();
	persos[1]->positionMapX = (colonnes / 2) + rand() % (colonnes / 2);
	persos[1]->positionMapY = (ligne / 2) + rand() % (ligne / 2);
	persos[2] = initMouette();
	persos[2]->positionMapX = (colonnes / 2) + rand() % (colonnes / 2);
	persos[2]->positionMapY = rand() % (ligne / 2);
	int *mouvValide = (int *)malloc(nbPersonnages * sizeof(int));
	for (i = 0; i < nbPersonnages; i++)
		mouvValide[i] = 0;

	bool_t noInput = TRUE;
	direction_t direction;

	SDL_Texture *herbe = charge_texture_herbe(fenetre, renderer, lab->nbLigne, lab->nbCol);
	SDL_Texture **persoText = loadTextPersos(fenetre, renderer);
	if (herbe != NULL)
	{
		SDL_Texture **barrieres = charge_barriere(fenetre, renderer);
		if (barrieres != NULL)
		{

			Affiche_Game(fenetre, renderer, ecran, lab->tabPiece, lab->nbCol, lab->nbLigne, persos, nbPersonnages, mouvValide, persoText, barrieres, herbe);
			int nbTour = 0;

			while (continuer && estVivant(persos[0]))
			{
				noInput = TRUE;
				while (noInput && continuer && SDL_WaitEventTimeout(&event, 30))
				{
					switch (event.type)
					{
					case SDL_QUIT:
						continuer = FALSE;
						break;
					case SDL_KEYDOWN:
						switch (event.key.keysym.sym)
						{
						case SDLK_SPACE:
							continuer = !continuer;
							break;

						case SDLK_ESCAPE:
							continuer = !continuer;
							break;

						case SDLK_UP:
							noInput = FALSE;
							mouvValide[0] = deplacerPersonnage(0, lab->tabPiece, lab->nbLigne, lab->nbCol, NORD, persos, nbPersonnages);
							break;

						case SDLK_DOWN:
							noInput = FALSE;
							mouvValide[0] = deplacerPersonnage(0, lab->tabPiece, lab->nbLigne, lab->nbCol, SUD, persos, nbPersonnages);

							break;

						case SDLK_LEFT:
							noInput = FALSE;

							mouvValide[0] = deplacerPersonnage(0, lab->tabPiece, lab->nbLigne, lab->nbCol, OUEST, persos, nbPersonnages);

							break;

						case SDLK_RIGHT:
							noInput = FALSE;
							mouvValide[0] = deplacerPersonnage(0, lab->tabPiece, lab->nbLigne, lab->nbCol, EST, persos, nbPersonnages);

							break;
						default:

							break;
						}
						break;
					default:

						break;
					}
					SDL_FlushEvent(SDL_KEYDOWN);
				}

				if (noInput)
				{
					for (i = 0; i < nbPersonnages; i++)
						mouvValide[i] = 0;
				}
				else
				{
					for (i = 1; i < nbPersonnages; i++)
					{
						if (getType(persos[i]) == OURS)
						{
							direction = directionAStar(g, persos[i], persos[0], distTcheby, colonnes);
							mouvValide[i] = deplacerPersonnage(i, lab->tabPiece, lab->nbLigne, lab->nbCol, direction, persos, nbPersonnages);
							nbTour++;
						}
						else
						{
							direction = pow(2, rand() % 4);
							mouvValide[i] = deplacerPersonnage(i, lab->tabPiece, lab->nbLigne, lab->nbCol, direction, persos, nbPersonnages);
	
						}
					}
				}

				

				Affiche_Game(fenetre, renderer, ecran, lab->tabPiece, lab->nbCol, lab->nbLigne, persos, nbPersonnages, mouvValide, persoText, barrieres, herbe);

				if(!estVivant(persos[0])){
					AfficherGameOver(fenetre, ecran, renderer, nbTour, herbe, ligne, colonnes);

				}
			}

			for (int i = 0; i < 2; i++)
			{
				SDL_DestroyTexture(barrieres[i]);
			}
		}
		libereTextPersos(persoText);
		SDL_DestroyTexture(herbe);
		free(mouvValide);
	}

	// while(continuer)
	// {
	// 	while(continuer && SDL_PollEvent(&event))
	// 	{
	// 		switch (event.type)
	// 		{
	// 			case SDL_QUIT:
	// 				continuer = 0;
	// 				break;
	// 			default:
	// 				dessine_fond(fenetre, renderer, lab->tabPiece, lab->nbLigne , lab->nbCol);
	// 				break;
	// 		}
	// 		SDL_RenderPresent(renderer);
	// 	}
	// }
	return 0;
}

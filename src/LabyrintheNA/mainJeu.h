#ifndef _MAIN_JEU_H_
#define _MAIN_JEU_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "labyNonArbo.h"

#include "fond_v1.h"
#include "fond_v2.h"
#include "baseSDL.h"
#include "direction.h"
#include "animation.h"
#include "personnage.h"
#include "gestionDeplacement.h"
#include "deplacementParcours.h"
#include "parcours.h"

int mainJeu(SDL_Window *fenetre,SDL_Renderer *renderer,SDL_DisplayMode * ecran ,graphe_t* g,matrice_t * lab, int ligne,int colonnes);

#endif
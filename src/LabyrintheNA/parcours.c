#include "parcours.h"

int *dijkstra(graphe_t *graphe, int a, int b)
{
	int n = graphe->nbNoeud;
	int infini = n * n;
	int *arbre = (int *)malloc(n * sizeof(int));
	if (arbre != NULL)
	{

		tas_t *tas = init_tas(n);
		if (tas != NULL)
		{
			maillon_t *elt;
			for (int i = 0; i < n; i++)
			{
				elt = init_maillon(i, infini);
				enfiler(tas, elt);
				arbre[i] = i;
			}
			// Mise de la distance du noeud (a) dont on part à 0
			tas->tab[tas->adr[a]]->val = 0;
			tasser_parent(tas, tas->adr[a]);

			// Début

			maillon_t *elt_elu;
			int ident_elu;
			int continuer = 1;
			// affiche_tas(tas);
			while (continuer && tas->taille > 0)
			{
				elt_elu = defiler(tas);
				ident_elu = elt_elu->ident;

				// printf("Elu : %d\n", ident_elu);

				if (ident_elu != b)
				{
					cellule2_t *cour = graphe->tabArete;

					while (cour != NULL)
					{
						int ident_arrive = -1;
						if (cour->ext1 == ident_elu && tas->adr[cour->ext2] > -1)
						{
							ident_arrive = cour->ext2;
						}
						else if (cour->ext2 == ident_elu && tas->adr[cour->ext1] > -1)
						{
							ident_arrive = cour->ext1;
						}
						if (ident_arrive > -1)
						{
							if (elt_elu->val + cour->valuation < tas->tab[tas->adr[ident_arrive]]->val)
							{
								tas->tab[tas->adr[ident_arrive]]->val = elt_elu->val + cour->valuation;
								tasser_parent(tas, tas->adr[ident_arrive]);
								arbre[ident_arrive] = ident_elu;
							}
						}
						cour = cour->suivant;
					}
				}
				else
				{
					continuer = 0;
				}

				free(elt_elu);
				// affiche_tas(tas);
			}
			free_tas(tas);
		}
		else
		{
			free(arbre);
		}
	}
	else
	{
		perror("Erreur allocation dijkstra : tableau arbre : ");
	}
	return arbre;
}

int *dijkstra_affiche(graphe_t *graphe, int a, int b, SDL_Renderer *renderer, int nb_ligne, int nb_colonne, int largeur_case, int hauteur_case)
{
	int x, y;
	uneDVersDeuxD(a, nb_colonne, &x, &y);
	SDL_Rect depart_arrive = {(x * largeur_case) + (largeur_case / 4), (y * hauteur_case) + (hauteur_case / 4), largeur_case / 2, hauteur_case / 2};
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(renderer, &depart_arrive);
	SDL_RenderPresent(renderer);
	uneDVersDeuxD(b, nb_colonne, &x, &y);
	depart_arrive.x = (x * largeur_case) + (largeur_case / 4);
	depart_arrive.y = (y * hauteur_case) + (hauteur_case / 4);
	SDL_RenderDrawRect(renderer, &depart_arrive);
	SDL_RenderPresent(renderer);
	SDL_Delay(100);

	SDL_Rect piece_explo = {1, 1, largeur_case - 2, hauteur_case - 2}, piece_visite = {1, 1, largeur_case - 2, hauteur_case - 2};
	int bleu = 255, visite = 150, en_explo = 70, explore = 0, alpha = 150;

	int n = graphe->nbNoeud;
	int infini = n * n;
	int *arbre = (int *)malloc(n * sizeof(int));
	if (arbre != NULL)
	{

		tas_t *tas = init_tas(n);
		if (tas != NULL)
		{
			maillon_t *elt;
			for (int i = 0; i < n; i++)
			{
				elt = init_maillon(i, infini);
				enfiler(tas, elt);
				arbre[i] = i;
			}
			// Mise de la distance du noeud (a) dont on part à 0
			tas->tab[tas->adr[a]]->val = 0;
			tasser_parent(tas, tas->adr[a]);

			// Début

			maillon_t *elt_elu;
			int ident_elu;
			int continuer = 1;
			// affiche_tas(tas);
			while (continuer && tas->taille > 0)
			{
				elt_elu = defiler(tas);
				ident_elu = elt_elu->ident;

				SDL_SetRenderDrawColor(renderer, en_explo, en_explo, bleu, alpha);
				uneDVersDeuxD(ident_elu, nb_colonne, &x, &y);
				piece_explo.x = (x * largeur_case) + 1;
				piece_explo.y = (y * hauteur_case) + 1;
				SDL_RenderFillRect(renderer, &piece_explo);
				SDL_RenderPresent(renderer);
				SDL_Delay(100);

				// printf("Elu : %d\n", ident_elu);

				if (ident_elu != b)
				{
					cellule2_t *cour = graphe->tabArete;

					while (cour != NULL)
					{
						int ident_arrive = -1;
						if (cour->ext1 == ident_elu && tas->adr[cour->ext2] > -1)
						{
							ident_arrive = cour->ext2;
						}
						else if (cour->ext2 == ident_elu && tas->adr[cour->ext1] > -1)
						{
							ident_arrive = cour->ext1;
						}
						if (ident_arrive > -1)
						{
							SDL_SetRenderDrawColor(renderer, visite, visite, bleu, alpha);
							uneDVersDeuxD(ident_arrive, nb_colonne, &x, &y);
							piece_visite.x = (x * largeur_case) + 1;
							piece_visite.y = (y * hauteur_case) + 1;
							SDL_RenderFillRect(renderer, &piece_visite);
							SDL_RenderPresent(renderer);
							SDL_Delay(100);

							if (elt_elu->val + cour->valuation < tas->tab[tas->adr[ident_arrive]]->val)
							{
								tas->tab[tas->adr[ident_arrive]]->val = elt_elu->val + cour->valuation;
								tasser_parent(tas, tas->adr[ident_arrive]);
								arbre[ident_arrive] = ident_elu;
							}
						}
						cour = cour->suivant;
					}
				}
				else
				{
					continuer = 0;
				}

				SDL_SetRenderDrawColor(renderer, explore, explore, bleu, alpha);
				SDL_RenderFillRect(renderer, &piece_explo);
				SDL_RenderPresent(renderer);
				SDL_Delay(100);

				free(elt_elu);
				// affiche_tas(tas);
			}
			free_tas(tas);
		}
		else
		{
			free(arbre);
		}
	}
	else
	{
		perror("Erreur allocation dijkstra : tableau arbre : ");
	}
	return arbre;
}

float distanceEuclidienne(int a, int b, int nbColonne)
{
	int xa, xb, ya, yb;
	uneDVersDeuxD(a, nbColonne, &xa, &ya);
	uneDVersDeuxD(b, nbColonne, &xb, &yb);

	return (float)sqrt((xa - xb) * (xa - xb) + (ya - yb) * (ya - yb));
}

float distanceTchebychev(int a, int b, int nbColonne)
{
	int xa, xb, ya, yb;
	uneDVersDeuxD(a, nbColonne, &xa, &ya);
	uneDVersDeuxD(b, nbColonne, &xb, &yb);

	if (abs(xa - xb) < abs(ya - yb))
		return (float)abs(ya - yb);
	else
		return (float)abs(xa - xb);
}

float distanceManhattan(int a, int b, int nbColonne)
{
	int xa, xb, ya, yb;
	uneDVersDeuxD(a, nbColonne, &xa, &ya);
	uneDVersDeuxD(b, nbColonne, &xb, &yb);

	return (float)(abs(xa - xb) + abs(ya - yb));
}

int *aStar(graphe_t *graphe, int a, int b, float (*dist)(int, int, int), int nbColonne)
{
	//	SDL_Rect piece = { 1, 1, largeur_case-2, hauteur_case-2};
	int n = graphe->nbNoeud;
	int infini = n * n;
	float *distance = (float *)malloc(sizeof(float) * n);
	int *arbre = (int *)malloc(n * sizeof(int));
	if (arbre != NULL)
	{

		tas_t *tas = init_tas(n);
		if (tas != NULL)
		{
			maillon_t *elt;
			for (int i = 0; i < n; i++)
			{
				elt = init_maillon(i, infini);
				enfiler(tas, elt);
				arbre[i] = i;
			}
			// Mise de la distance du noeud (a) dont on part à 0
			distance[a] = dist(a, b, nbColonne);
			tas->tab[tas->adr[a]]->val = distance[a];
			tasser_parent(tas, tas->adr[a]);

			// Début

			maillon_t *elt_elu;
			int ident_elu;
			int continuer = 1;
			// affiche_tas(tas);
			while (continuer && tas->taille > 0)
			{
				elt_elu = defiler(tas);
				ident_elu = elt_elu->ident;

				// printf("Elu : %d\n", ident_elu);

				if (ident_elu != b)
				{
					cellule2_t *cour = graphe->tabArete;

					while (cour != NULL)
					{
						int ident_arrive = -1;
						if (cour->ext1 == ident_elu && tas->adr[cour->ext2] > -1)
						{
							ident_arrive = cour->ext2;
						}
						else if (cour->ext2 == ident_elu && tas->adr[cour->ext1] > -1)
						{
							ident_arrive = cour->ext1;
						}
						if (ident_arrive > -1)
						{
							distance[ident_arrive] = dist(ident_arrive, b, nbColonne);
							if (elt_elu->val + cour->valuation + distance[ident_arrive] - distance[ident_elu] < tas->tab[tas->adr[ident_arrive]]->val)

							{
								tas->tab[tas->adr[ident_arrive]]->val = elt_elu->val - distance[ident_elu] + cour->valuation + distance[ident_arrive];
								tasser_parent(tas, tas->adr[ident_arrive]);
								arbre[ident_arrive] = ident_elu;
							}
						}
						cour = cour->suivant;
					}
				}
				else
				{
					continuer = 0;
				}
				free(elt_elu);
				// affiche_tas(tas);
			}
			free_tas(tas);
		}
		else
		{
			free(distance);
			free(arbre);
		}
	}
	else
	{
		perror("Erreur allocation dijkstra : tableau arbre : ");
	}
	free(distance);
	return arbre;
}

int *aStar_affiche(graphe_t *graphe, int a, int b, float (*dist)(int, int, int), SDL_Renderer *renderer, int nbLigne, int nbColonne, int largeur_case, int hauteur_case)
{
	int x, y;
	uneDVersDeuxD(a, nbColonne, &x, &y);
	SDL_Rect depart_arrive = {(x * largeur_case) + (largeur_case / 4), (y * hauteur_case) + (hauteur_case / 4), largeur_case / 2, hauteur_case / 2};
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(renderer, &depart_arrive);
	SDL_RenderPresent(renderer);
	uneDVersDeuxD(b, nbColonne, &x, &y);
	depart_arrive.x = (x * largeur_case) + (largeur_case / 4);
	depart_arrive.y = (y * hauteur_case) + (hauteur_case / 4);
	SDL_RenderDrawRect(renderer, &depart_arrive);
	SDL_RenderPresent(renderer);
	SDL_Delay(100);

	SDL_Rect piece_explo = {1, 1, largeur_case - 2, hauteur_case - 2}, piece_visite = {1, 1, largeur_case - 2, hauteur_case - 2};
	int bleu = 255, visite = 150, en_explo = 70, explore = 0, alpha = 150;

	int n = graphe->nbNoeud;
	int infini = n * n;
	float *distance = (float *)malloc(sizeof(float) * n);
	int *arbre = (int *)malloc(n * sizeof(int));
	if (arbre != NULL)
	{
		tas_t *tas = init_tas(n);
		if (tas != NULL)
		{
			maillon_t *elt;
			for (int i = 0; i < n; i++)
			{
				elt = init_maillon(i, infini);
				enfiler(tas, elt);
				arbre[i] = i;
			}
			// Mise de la distance du noeud (a) dont on part à 0
			distance[a] = dist(a, b, nbColonne);
			tas->tab[tas->adr[a]]->val = distance[a];

			tasser_parent(tas, tas->adr[a]);

			// Début

			maillon_t *elt_elu;
			int ident_elu;
			int continuer = 1;
			// affiche_tas(tas);
			while (continuer && tas->taille > 0)
			{
				elt_elu = defiler(tas);
				ident_elu = elt_elu->ident;

				// printf("Elu : %d\n", ident_elu);

				SDL_SetRenderDrawColor(renderer, en_explo, en_explo, bleu, alpha);
				uneDVersDeuxD(ident_elu, nbColonne, &x, &y);
				piece_explo.x = (x * largeur_case) + 1;
				piece_explo.y = (y * hauteur_case) + 1;
				SDL_RenderFillRect(renderer, &piece_explo);
				SDL_RenderPresent(renderer);
				SDL_Delay(100);

				if (ident_elu != b)
				{
					cellule2_t *cour = graphe->tabArete;

					while (cour != NULL)
					{
						int ident_arrive = -1;
						if (cour->ext1 == ident_elu && tas->adr[cour->ext2] > -1)
						{
							ident_arrive = cour->ext2;
						}
						else if (cour->ext2 == ident_elu && tas->adr[cour->ext1] > -1)
						{
							ident_arrive = cour->ext1;
						}
						if (ident_arrive > -1)
						{
							SDL_SetRenderDrawColor(renderer, visite, visite, bleu, alpha);
							uneDVersDeuxD(ident_arrive, nbColonne, &x, &y);
							piece_visite.x = (x * largeur_case) + 1;
							piece_visite.y = (y * hauteur_case) + 1;
							SDL_RenderFillRect(renderer, &piece_visite);
							SDL_RenderPresent(renderer);
							SDL_Delay(100);

							distance[ident_arrive] = dist(ident_arrive, b, nbColonne);
							if (elt_elu->val + cour->valuation + distance[ident_arrive] - distance[ident_elu] < tas->tab[tas->adr[ident_arrive]]->val)
							{
								tas->tab[tas->adr[ident_arrive]]->val = elt_elu->val - distance[ident_elu] + cour->valuation + distance[ident_arrive];
								tasser_parent(tas, tas->adr[ident_arrive]);
								arbre[ident_arrive] = ident_elu;
							}
						}
						cour = cour->suivant;
					}
				}
				else
				{
					continuer = 0;
				}

				SDL_SetRenderDrawColor(renderer, explore, explore, bleu, alpha);
				SDL_RenderFillRect(renderer, &piece_explo);
				SDL_RenderPresent(renderer);
				SDL_Delay(100);

				free(elt_elu);
				// affiche_tas(tas);
			}
			free_tas(tas);
		}
		else
		{
			free(arbre);
		}
	}
	else
	{
		perror("Erreur allocation dijkstra : tableau arbre : ");
	}
	free(distance);
	return arbre;
}

void affiche_chemin(int *arbre, int a, int b, SDL_Renderer *renderer, int nb_colonne, int largeur_case, int hauteur_case)
{
	int x, y;
	uneDVersDeuxD(b, nb_colonne, &x, &y);

	SDL_Rect chemin = {(x * largeur_case) + (largeur_case / 4), (y * hauteur_case) + (hauteur_case / 4), largeur_case / 2, hauteur_case / 2};
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &chemin);
	SDL_RenderPresent(renderer);
	SDL_Delay(100);

	int cour = b;
	while (cour != a)
	{
		uneDVersDeuxD(arbre[cour], nb_colonne, &x, &y);
		chemin.x = (x * largeur_case) + (largeur_case / 4);
		chemin.y = (y * hauteur_case) + (hauteur_case / 4);
		SDL_RenderFillRect(renderer, &chemin);
		SDL_RenderPresent(renderer);
		SDL_Delay(100);
		cour = arbre[cour];
	}
}
/**
 * \file parcours.h
 * \brief Implémentation de l'algorithme de Dijkstra et A*
 */

#ifndef _DIJKSTRA_H_
#define _DIJKSTRA_H_

#include "compoConnexe.h"
#include "liste2.h"
#include "tas_binaire.h"
#include "baseSDL.h"

#include <SDL2/SDL.h>
#include <math.h>

/**
 * @brief algorithme de dijkstra
 * 
 * @param graphe graphe étudié
 * @param a depart
 * @param b arrivée
 * @return int* tableau des pères
 */
int * dijkstra(graphe_t * graphe, int a, int b);

/**
 * @brief algorithme de dijkstra version garphique pour sdl
 * 
 * @param graphe graphe étudié
 * @param a départ
 * @param b arrivée
 * @param renderer 
 * @param nb_ligne nombre de ligne du labyrinthe 
 * @param nb_colonne nombre de colonne du labyrinthe
 * @param largeur_case largeur d'une case de la map
 * @param hauteur_case hauteur d'une case de la map
 * @return int* tableau des pères
 */
int * dijkstra_affiche(graphe_t * graphe, int a, int b, SDL_Renderer * renderer, int nb_ligne, int nb_colonne, int largeur_case, int hauteur_case);


/**
 * @brief algorithme a*
 * 
 * @param graphe graphe étudié
 * @param a point de départ
 * @param b point d'arrivée
 * @param dist pointeur sur la fonction de calcul de distance
 * @param nbColonne nombre de colonne du labyrinthe
 * @return int* tableau des pères
 */
int *aStar(graphe_t *graphe, int a, int b, float (*dist)(int, int, int), int nbColonne);

/**
 * @brief algorithme a* version grpahique pour sdl
 *
 * @param graphe graphe étudié
 * @param a point de départ
 * @param b point d'arrivée
 * @param dist pointeur sur la fonction de calcul de distance
 * @param renderer 
 * @param nb_ligne nombre de ligne du labyrinthe 
 * @param nb_colonne nombre de colonne du labyrinthe
 * @param largeur_case largeur d'une case de la map
 * @param hauteur_case hauteur d'une case de la map
 * @param nbColonne nombre de colonne du labyrinthe
 * @return int* tableau des pères
 */
int *aStar_affiche(graphe_t *graphe, int a, int b, float (*dist)(int, int, int), SDL_Renderer * renderer, int nbLigne, int nbColonne,  int largeur_case, int hauteur_case);

/**
 * @brief calcul de la distance entre deux points celon le critère euclidien
 * 
 * @param a point de départ
 * @param b point d'arrivé
 * @param nbColonne nombre de colonne de la matrice
 * @return int distance
 */
float distanceEuclidienne(int a, int b, int nbColonne);

/**
 * @brief calcul de la distance entre deux points celon le critère de Tchebychev
 * 
 * @param a point de départ
 * @param b point d'arrivé
 * @param nbColonne nombre de colonne de la matrice
 * @return int distance
 */
float distanceTchebychev(int a, int b, int nbColonne);

/**
 * @brief calcul de la distance entre deux points celon le critère de Manhattan
 * 
 * @param a point de départ
 * @param b point d'arrivé
 * @param nbColonne nombre de colonne de la matrice
 * @return int distance
 */
float distanceManhattan(int a, int b, int nbColonne);

void affiche_chemin(int * arbre, int a, int b, SDL_Renderer * renderer, int nb_colonne, int largeur_case, int hauteur_case);

#endif
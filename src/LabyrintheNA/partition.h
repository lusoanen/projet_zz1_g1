/**
 * \file partition.h
 * \brief Implémentation de la partition arborescente
 */

#ifndef _PARTITION_H_
#define _PARTITION_H_ value

#include <stdlib.h>
#include <stdio.h>
#include "liste.h"

typedef struct partition
{
	int N; /**< nombre d'élément */
	int * pere; /**< tableau des pères de chaque élément */
	int * hauteur; /**< tableau des hauteurs de partition (nombre de pères) à partir de chaque élément */
}partition_t;

/**
 * @brief initialisation de la partition
 * 
 * @param n nombre d'élément
 * @return partition_t* partition allouée
 */
partition_t * creer_partition(int n);

/**
 * @brief libération en mémoire de la partition
 * 
 * @param part partition à libérer
 */
void libere_partition(partition_t * part);

/**
 * @brief affichage dans le terminale les élément de la partition 
 * 
 * @param part partition à afficher
 */
void affiche_partition(partition_t * part);

/**
 * @brief retourne la classe d'un élément de partiton
 * 
 * @param part partition étudiée
 * @param elt élément étudié
 * @return int classe de l'élément
 */
int recuperer_classe(partition_t * part, int elt);

/**
 * @brief fusion des classes de deux élément donnés
 * 
 * @param part partition utilisé
 * @param i premier élément à fusionner
 * @param j deuxième élément à fusionner
 * @return int la fusion à eu lieu ou pas
 */
int fusionner(partition_t * part, int i, int j);

/**
 * @brief liste les élément d'une classe donnée
 * 
 * @param t partition
 * @param classe classe voulue
 * @return cellule_t** liste des élémnets de la classe
 */
cellule_t ** lister_classe(partition_t * t, int classe); 

/**
 * @brief liste des classes (et de leurs élements) d'une partition 
 * 
 * @param t partiton étudiée
 * @return cellule_t*** tableau des éléments par classe
 */
cellule_t *** lister_partition(partition_t * t);

/**
 * @brief affiche dans le terminale les classes et leurs éléments respectifs 
 * 
 * @param classes liste des éléments rangés par classe
 * @param n nombre d'élément
 */
void affiche_classes(cellule_t *** classes, int n);

#endif
#include "personnage.h"



personnage_t * initPlayer(){
	personnage_t *nouveau = (personnage_t*)malloc(sizeof(personnage_t));
	if(nouveau != NULL){
		nouveau->vieMax = 2;
		nouveau->vieCourante = 2;
		nouveau->positionMapX = 0;
		nouveau->positionMapY = 0;
		nouveau->regard = SUD;
		nouveau->attaque = 1;
		nouveau->type = PLAYER;
		
	}
	else
		fprintf(stderr, "Malloc player impossible\n");

	return nouveau;
}



personnage_t * initOurs(){
	personnage_t *nouveau = (personnage_t*)malloc(sizeof(personnage_t));
	if(nouveau != NULL){
		nouveau->vieMax = 4;
		nouveau->vieCourante = 4;
		nouveau->regard = EST;
		nouveau->attaque = 2;
		nouveau->type = OURS;
		
	}
	else
		fprintf(stderr, "Malloc monstre impossible\n");

	return nouveau;
}

personnage_t * initMouette(){
	personnage_t *nouveau = (personnage_t*)malloc(sizeof(personnage_t));
	if(nouveau != NULL){
		nouveau->vieMax = 3;
		nouveau->vieCourante = 3;
		nouveau->regard = EST;
		nouveau->attaque = 1;
		nouveau->type = MOUETTE;
		
	}
	else
		fprintf(stderr, "Malloc monstre impossible\n");

	return nouveau;
}	

int getVieMax(personnage_t * perso){
	return perso->vieMax;
}

int getVieCourante(personnage_t * perso){
	return perso->vieCourante;
}


void setVieCourante(personnage_t * perso, int nouvelleVie){
	if(nouvelleVie <= perso->vieMax && nouvelleVie >= 0){
		perso->vieCourante = nouvelleVie;
	}
	else
		fprintf(stderr, "La vie  : %d est en dehors de l'intervale de vie du personnage [%d ; 0]\n", nouvelleVie, perso->vieMax );
}

bool_t estVivant(personnage_t * perso){
	if(perso->vieCourante <= 0)
		return FALSE;
	else	
		return TRUE;
}

void seDeplacer(personnage_t * perso, direction_t direction){
	switch(direction){
		case NORD :
			perso->positionMapY -= 1;
			break;
		case SUD :
			perso->positionMapY += 1;
			break;
		case OUEST :
			perso->positionMapX -= 1;
			break;
		case EST : 
			perso->positionMapX += 1;
			break;
		default:
			break;
	}
}

bool_t subirAttaque(personnage_t * perso, int attaque){
	perso->vieCourante -= attaque;
	return estVivant(perso);
}

void getPosition(personnage_t * perso, int * x, int * y){
	*x = perso->positionMapX;
	*y = perso->positionMapY;
}

void setPosition(personnage_t * perso, int nouvX, int nouvY, int maxGrilleX, int maxGrilleY){
	if(nouvX >= 0 && nouvX <= maxGrilleX)
		perso->positionMapX = nouvX;
	else
		fprintf(stderr, "coordonnée X invalide\n");
	if(nouvY >= 0 && nouvY <= maxGrilleY)
		perso->positionMapY = nouvY;
	else
		fprintf(stderr, "coordonnée Y invalide\n");
}

int getAttaque(personnage_t * perso){
	return perso->attaque;
}

void setAttaque(personnage_t * perso, int bonus){
	perso->attaque += bonus;
	if(perso->attaque < 0)
		perso->attaque = 0;
}

typePerso_t getType(personnage_t * perso){
	return perso->type;
}

void libererPerso(personnage_t * perso){
	free(perso);
}

void libererTableauPersos(personnage_t ** persos,int nbPersonnages){
	int i;
	for(i=0;i<nbPersonnages;i++){
		libererPerso(persos[i]);
	}
	free(persos);
}
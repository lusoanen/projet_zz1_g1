/**
 * \file personnage.h
 * \brief Gestion des personnages
 */

#ifndef _PERSO_H_
#define _PERSO_H_

#include <stdio.h>
#include <stdlib.h>
#include "direction.h"
#include "typePerso.h"
#include "bool.h"


typedef struct personnage{
	int vieMax; /**< vie max du personnage */
	int vieCourante; /**< vie courante du personnage */
	int positionMapX; /**< position du personnage celon l'axe x dans la matrice */
	int positionMapY; /**< position du personnage celon l'axe y dan sla matrice */
	direction_t regard; /**< direction dans lequel est orienté le personnage */
	int attaque; /**< dégats que le personnage inflige */
	typePerso_t type; /**< type du personnage */
}personnage_t;

/**
 * @brief initialisation du personnage de type player
 * 
 * @return personnage_t* personnage crée
 */
personnage_t * initPlayer();

/**
 * @brief initialisation du personnage de type ours
 * 
 * @return personnage_t* personnage crée
 */
personnage_t * initOurs();



/**
 * @brief initialisation du personnage de type mouette
 * 
 * @return personnage_t* personnage crée
 */
personnage_t * initMouette();

/**
 * @brief retourne la vie max d'un personnage
 * 
 * @param perso personnage étudié
 * @return int vie max
 */
int getVieMax(personnage_t * perso);

/**
 * @brief retourne la vie courante d'un personnage
 * 
 * @param perso personnage étudié
 * @return int vie courante
 */
int getVieCourante(personnage_t * perso);

/**
 * @brief mise à jour de la vie courante d'un personnage
 * 
 * @param perso personnage étudié
 * @param nouvelleVie valeur de la nouvelle vie courante
 */
void setVieCourante(personnage_t * perso, int nouvelleVie);

/**
 * @brief vérifie si un personnag est tujours en vie ou non
 * 
 * @param perso personnage étudié
 * @return bool_t est en vie ou non
 */
bool_t estVivant(personnage_t * perso);

/**
 * @brief inflige des dégats au personnage
 * 
 * @param perso personnage
 * @param attaque dégats infligés
 * @return bool_t retourne si le personnage est mort ou non
 */
bool_t subirAttaque(personnage_t * perso, int attaque);

/**
 * @brief retourne la position XY courante
 * 
 * @param perso personnage étudié
 * @param x position celon l'axe x
 * @param y position celon l'axe y
 */
void getPosition(personnage_t * perso, int * x, int * y);

/**
 * @brief modifie la position d'un personnage
 * 
 * @param perso personnage étudié
 * @param nouvX nouvelle position x
 * @param nouvY nouvelle position y
 * @param maxGrilleX nombre de colonne
 * @param maxGrilleY nombre de lignes
 */
void setPosition(personnage_t * perso, int nouvX, int nouvY, int maxGrilleX, int maxGrilleY);

/**
 * @brief déplace un personnage dans une direction donnée
 * 
 * @param perso personnage étudié
 * @param direction direction de déplacement
 */
void seDeplacer(personnage_t * perso, direction_t direction);

/**
 * @brief retourne les dégats qu'inflige un personnage
 * 
 * @param perso personnage étudié
 * @return int dégats
 */
int getAttaque(personnage_t * perso);

/**
 * @brief Modifie le nombre de dégat qu'infliege un personnage
 * 
 * @param perso personnage étudié
 * @param bonus bonus (ou malus) d'attaque à intégré
 */
void setAttaque(personnage_t * perso, int bonus);

/**
 * @brief donne le type du personnage
 * 
 * @param perso personnage étudié
 * @return typePerso_t type du personnage
 */
typePerso_t getType(personnage_t * perso);

/**
 * @brief libération en mémoire du personnage
 * 
 * @param perso personnage à libérer
 */
void libererPerso(personnage_t * perso);

/**
 * @brief libération en mémoire du tableau des personnages 
 * 
 * @param persos tableaux de personnage à libérer
 * @param nbPersonnages nombre de personnage dans le tableau
 */
void libererTableauPersos(personnage_t ** persos,int nbPersonnages);


#endif
#include "tas_binaire.h"

maillon_t * init_maillon(int id, float v)
{
	maillon_t * elt = (maillon_t *) malloc(sizeof(maillon_t));
	if(elt != NULL)
	{
		elt->ident = id;
		elt->val = v;
	}
	else
	{
		perror("Erreur allocation maillon : ");
	}
	return elt;
}

tas_t * init_tas(int taille)
{
	tas_t * tas = (tas_t *) malloc(sizeof(tas_t));
	if (tas != NULL) 
	{
		tas->taille = 0;
		tas->taille_max = taille;
		tas->tab = (maillon_t **) malloc(taille*sizeof(maillon_t));
		if (tas->tab != NULL)
		{
			tas->adr = (int *) malloc(taille*sizeof(int));
			if (tas->adr != NULL)
			{
				for (int i=0; i < taille; i++)
				{
					tas->tab[i] = NULL;
					tas->adr[i] = -1;
				}
			}
			else
			{
				perror("Erreur allocation de tas->adr : ");
				free(tas->tab);
				free(tas);
				tas =NULL;
			}
		}
		else
		{
			free(tas);
			tas = NULL;
			perror("Erreur allocation de tas->tab : ");
		}
	}
	else
	{
		perror("Erreur allocation de tas : ");
	}
	return tas;
}

void free_tas(tas_t * tas)
{
	for (int i=0; i < tas->taille; i++)
	{
		free(tas->tab[i]);
	}
	free(tas->adr);
	free(tas->tab);
	free(tas);
}

void enfiler(tas_t * tas, maillon_t * elt)
{
	// int code = 0;
	if (tas->taille < tas->taille_max)
	{
		tas->tab[tas->taille] = elt;
		tas->adr[elt->ident] = tas->taille;
		tasser_parent(tas, tas->taille);
		tas->taille++;
	}
	else
	{
		// code = 1;
	}

	// return code;
}

maillon_t * defiler(tas_t * tas)
{
	maillon_t * elt = NULL;
	if (tas->taille > 0)
	{
		elt = tas->tab[0];
		tas->taille--;
		echange(tas, 0, tas->taille);
		tas->tab[tas->taille] = NULL;
		tas->adr[elt->ident] = -1;
		tasser_fils(tas,0);
	}
	return elt;
}

void echange(tas_t * tas, int i, int k)
{
	tas->adr[tas->tab[i]->ident] = k;
	tas->adr[tas->tab[k]->ident] = i;
	maillon_t * temp = tas->tab[i];
	tas->tab[i] = tas->tab[k];
	tas->tab[k] = temp;
}

void tasser_parent(tas_t * tas, int i)
{
	if (tas->tab[i]->val < tas->tab[(i-1)/2]->val)
	{
		echange(tas,i,(i-1)/2);
		if ((i-1)/2 > 0)
		{
			tasser_parent(tas,(i-1)/2);
		}
	}
}

void tasser_fils(tas_t * tas, int i)
{
	int min = i;
	for (int k=2*i+1; k <= 2*i+2 && k < tas->taille; k++)
	{
		if (tas->tab[min]->val > tas->tab[k]->val)
		{
			min = k;
		}
	}
	if (min != i)
	{
		echange(tas, i, min);
		tasser_fils(tas, min);
	}
}

void affiche_tas(tas_t * t)
{
	printf("Taille du tas : %d\nTab : \n", t->taille);
	affiche_tab(t->tab, t->taille);
	printf("Adr : \n");
	for (int i=0; i < t->taille_max; i++)
	{
		printf("Ident : %d | adr : %d\n", i, t->adr[i]);
	}
	printf("\n");
}

int tri_par_tas(maillon_t ** tab, int taille)
{
	int code = 0;
	tas_t * tas = init_tas(taille);
	if (tas != NULL)
	{
		for (int i=0; i < taille; i++)
		{
			enfiler(tas, tab[i]);
		}

		for (int i=0; i < taille; i++)
		{
			tab[i] = defiler(tas);
		}
		free_tas(tas);
	}
	else
	{
		code = 1;
	}
	return code;
}

void affiche_tab(maillon_t * tab[], int taille)
{
	printf("  ident  |   val   \n_________|_________\n");
	for (int i=0; i < taille; i++)
	{
		printf("   %3d   |   %3f   \n", tab[i]->ident, tab[i]->val);
	}
	printf("\n");
}
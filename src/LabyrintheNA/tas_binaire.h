/**
 * \file tas_binaire.h
 * \brief Implémentation du tas min
 */

#ifndef _TAS_BIN_H_
#define _TAS_BIN_H_

#include <stdlib.h>
#include <stdio.h>

typedef struct maillon
{
	int ident; 	/**< Identifiant du maillon */
	float val;	/**< Valeur du maillon */
} maillon_t;

typedef struct tas
{
	int taille; 		/**< Taille actuelle du tas */
	int taille_max; 	/**< Taille maximum du tas */
	int * adr; 			/**< Tableau des adresses dans le tas de chaque élément (adr[i] contient l'indice dans tab du maillon d'identifiant i)*/
	maillon_t ** tab; 	/**< Tableau représentant le tas */
} tas_t;


/**
 * \fn maillon_t * init_maillon(int id, int v)
 * \brief Fonction d'allocation et d'initialisation d'un maillon
 * 
 * @param  id Identifiant du maillon à créer
 * @param  v  Valeur du maillon à créer
 * @return    L'adresse du maillon créer si l'allocation a réussi, NULL sinon
 */
maillon_t * init_maillon(int id, float v);

/**
 * \fn tas_t * init_tas(int taille)
 * \brief Fonction de création d'un tas
 * @param  taille Taille maximum du tas à créer
 * @return        L'adresse du tas créé ou NULL si le tas n'a pas pu être créé
 */
tas_t * init_tas(int taille);

/**
 * \fn void free_tas(tas_t * tas)
 * \brief Fonction de libération d'un tas, attention il ne libère dans le tableau que jusqu'à sa taille actuelle et pas sa taille maximum
 * 
 * @param tas Tas à libérer
 */
void free_tas(tas_t * tas);

/**
 * \fn void enfiler(tas_t * tas, maillon_t * elt)
 * \brief Fonction d'insertion d'un maillon dans un tas (le met à la bonne place), le maillon n'est pas inséré si le tas est plein
 * 
 * @param tas Tas dans lequel insérer
 * @param elt Maillon à insérer
 */
void enfiler(tas_t * tas, maillon_t * elt);

/**
 * \fn maillon_t * defiler(tas_t * tas)
 * \brief Fonction récupérant le maillon à la racine du tas (le maillon est supprimé du tas)
 * 
 * @param  tas Tas dont on récupère la racine
 * @return     Le maillon à la racine du tas ou NULL si le tas est vide
 */
maillon_t * defiler(tas_t * tas);

/**
 * \fn void echange(tas_t * tas, int i, int k)
 * \brief Fonction échangeant 
 * @param tas [description]
 * @param i   [description]
 * @param k   [description]
 */
void echange(tas_t * tas, int i, int k);

/**
 * \fn void tasser_parent(tas_t * t, int i)
 * \brief Fonction de tamisage du tas (version comparant le noeud à déplacer à son parent)
 * 
 * @param t Tas à tamiser
 * @param i Indice dans le tas de l'élément à tasser
 */
void tasser_parent(tas_t * t, int i);

/**
 * \fn void tasser_fils(tas_t * t, int i)
 * \brief Fonction de tamisage du tas (version comparant le noeud à déplacer à ses fils)
 * 
 * @param t Tas à tamiser
 * @param i Indice dans le tas de l'élément à tasser
 */
void tasser_fils(tas_t * tas, int i);

/**
 * \fn void affiche_tas(tas_t * tas)
 * \brief Fonction affichant le contenu du tas (taille actuelle, tableau représentant le tas, tableau des adresses des éléments dans le tas) 
 * 
 * @param tas Tas à afficher
 */
void affiche_tas(tas_t * tas);

/**
 * \fn void affiche_tab(maillon_t * tab[], int taille)
 * \\brief Fonction affichant le contenu d'un tableau de maillons
 * 
 * @param tab    Tableau à afficher
 * @param taille Taille du tableau à afficher
 */
void affiche_tab(maillon_t * tab[], int taille);

/**
 * \fn int tri_par_tas(maillon_t ** tab, int taille)
 * \brief Fonction triant un tableau de maillons via le tri par tas
 * 
 * @param  tab    Tableau à trier
 * @param  taille Taille du tableau à trié
 * @return        0 si le tri a été effectué, 1 sinon
 */
int tri_par_tas(maillon_t ** tab, int taille);

#endif
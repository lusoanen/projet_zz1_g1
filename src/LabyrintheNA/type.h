/**
 * \file type.h
 * \brief Définition du type pièce
 */

#ifndef _TYPE_H_
#define _TYPE_H_

#include "liste2.h"
#include "direction.h"

typedef struct piece
{
	int id; /**< identifiant de la pièce*/
	direction_t murs; /**< valeur binaire de la somme des murs dans la pièce*/
}piece_t;

typedef cellule2_t type_t; 

#endif
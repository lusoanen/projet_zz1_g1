/**
 * \file typePerso.h
 * \brief Enum pour la gestion du type de personnage
 */

#ifndef _TYPE_PERSO_H_
#define _TYPE_PERSO_H_

/**
 * @brief type des personnage
 * 
 */
typedef enum typePerso{
	PLAYER = 0, /**< type du personnage joueur */
	OURS, /**< type méchant qui cherche sa cible */
	MOUETTE /** type méchant qui ne tiens pas compte des murs */
}typePerso_t;

#endif
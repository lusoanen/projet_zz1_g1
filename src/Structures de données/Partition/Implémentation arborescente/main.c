#include <stdio.h>
#include "partition.h"
#include "liste.h"

int main()
{
	partition_t * part = creer_partition(10);
	if (part != NULL)
	{
		fusionner(part, 2, 3);
		fusionner(part, 1, 0);

		// for (int i=0; i < part->N; i++)
		// {
		// 	printf("i : %d pere : %d hauteur : %d\n", i, part->pere[i], part->hauteur[i]);
		// }

		fusionner(part, 2, 3);
		fusionner(part, 3, 10);
		fusionner(part, 3, 9);
		cellule_t ** tete = NULL;
		cellule_t *** tab = NULL;

		tete = lister_classe(part,3);
		if (tete != NULL)
		{
			AfficheListe(tete);
			free(tete);
		}

		tab = lister_partition(part);
		if (tab != NULL)
		{
			affiche_classes(tab, part->N);
			for (int i=0; i < part->N; i++)
			{
				if (tab[i] != NULL)
				{
					LibererListe(tab[i]);
				}
			}
			free(tab);
		}
		libere_partition(part);
	}

	return 0;
}
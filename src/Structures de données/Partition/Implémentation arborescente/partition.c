#include "partition.h"


partition_t * creer_partition(int n)
{	
	partition_t * part = (partition_t *) malloc(sizeof(partition_t));
	if (part == NULL)
	{
		perror("Problème allocation partition :");
	}
	else
	{	
		part->N = n;
		part->pere = (int *) malloc(sizeof(int)*n);
		if (part->pere == NULL)
		{
			free(part);
			part = NULL;
			perror("Problème allocation tableau père dans partition :");
		}
		else
		{
			part->hauteur = (int *) malloc(sizeof(int)*n);
			if (part->hauteur == NULL)
			{
				free(part->pere);
				free(part);
				part = NULL;
				perror("Problème allocation tableau hauteur dans partition :");
			}
			else
			{
				for (int i=0; i < n; i++)
				{
					part->pere[i] = i;
					part->hauteur[i] = 0;
				}
			}
		}
	}
	return part;
}

void libere_partition(partition_t * part)
{
	free(part->pere);
	free(part->hauteur);
	free(part);
}

int recuperer_classe(partition_t * part, int elt)
{
	int classe = -1;
	if (elt >= 0 && elt < part->N)
	{
		if (elt != part->pere[elt])
		{
			classe = recuperer_classe(part, part->pere[elt]);
		}
		else
		{
			classe = elt;
		}
	}
	return classe;
}

int fusionner(partition_t * part, int i, int j)
{	
	int ok = 1;
	int classe_i = recuperer_classe(part, i);
	int classe_j = recuperer_classe(part, j);

	// printf("i : %d classe_i : %d  j : %d classe_j : %d\n", i, classe_i, j, classe_j);

	if (classe_i > -1 && classe_j > -1)
	{
		
		if (part->hauteur[classe_i] > part->hauteur[classe_j])
		{
			part->pere[classe_j] = classe_i;
		}
		else
		{
			// printf("Patate\n");
			part->pere[classe_i] = classe_j;
			// printf("pere i : %d  pere j : %d\n", part->pere[i], part->pere[j]);
			if (part->hauteur[classe_i] == part->hauteur[classe_j])
			{
				part->hauteur[classe_j]++;
			}

		}
	}
	else
	{
		ok = 0;
	}

	return ok;
}

cellule_t ** lister_classe(partition_t * part, int elt)
{	
	int classe = recuperer_classe(part, elt);
	cellule_t ** tete = (cellule_t **) malloc(sizeof(cellule_t *));
	if (tete != NULL)
	{
		for (int i=0; i < part->N; i++)
		{
			if (classe == recuperer_classe(part, i))
			{
				cellule_t * elt = AllocationMaillon(i);
				if (elt != NULL)
				{
					insere_tete(tete, elt);
				}
			}
		}
	}
	return tete;
}

cellule_t *** lister_partition(partition_t * part)
{
	cellule_t *** tab = (cellule_t ***) malloc(part->N*sizeof(cellule_t **));
	if (tab != NULL)
	{
		for (int i=0; i < part->N; i++)
		{
			tab[i] = (cellule_t **) malloc(sizeof(cellule_t *));
		}
		for (int i=0; i < part->N; i++)
		{
			cellule_t * elt = AllocationMaillon(i);
			int classe_i = recuperer_classe(part, i);
			printf("classe_i : %d\n", classe_i);
			if (elt != NULL && tab[classe_i] != NULL)
			{
				insere_tete(tab[classe_i], elt);
			}
		}
	}
	return tab;
}

void affiche_classes(cellule_t *** classes, int n)
{
	for (int i=0; i < n; i++)
	{
		printf("Classe %d : \n", i);
		AfficheListe(classes[i]);
	}
}
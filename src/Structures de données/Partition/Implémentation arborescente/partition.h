#ifndef _PARTITION_H_
#define _PARTITION_H_ value

#include <stdlib.h>
#include <stdio.h>
#include "liste.h"

typedef struct partition
{
	int N;
	int * pere;
	int * hauteur;
}partition_t;

partition_t * creer_partition(int n);

void libere_partition(partition_t * part);

void affiche_partition(partition_t * part);

int recuperer_classe(partition_t * part, int elt);

int fusionner(partition_t * part, int i, int j);

cellule_t ** lister_classe(partition_t * t, int classe); 

cellule_t *** lister_partition(partition_t * t);

void affiche_classes(cellule_t *** classes, int n);

#endif
#include <stdio.h>
#include "partition.h"
#include "liste.h"

int main()
{
	partition_t * part = creer(6);
	if (part != NULL)
	{
		affiche_partition(part);
		fusionner(part, 0, 5);
		fusionner(part, 2, 3);
		fusionner(part, 2, 4);
		affiche_partition(part);
		cellule_t ** classes = lister_partition(part);
		if (classes != NULL)
		{
			affiche_classes(classes, part->N);
			libere_tab_classe(classes, part->N);
		}
		libere_partition(part);
	}

	return 0;
}
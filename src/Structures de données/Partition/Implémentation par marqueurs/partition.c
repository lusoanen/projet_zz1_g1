#include "partition.h"

partition_t * creer(int n)
{
	partition_t * part = (partition_t *) malloc(sizeof(partition_t));
	if (part != NULL)
	{	
		part->N = n;
		part->tab = (int *) malloc(n*sizeof(int));
		if (part->tab != NULL)
		{
			for (int i=0; i < n; i++)
			{
				part->tab[i] = i;
			}
		}
		else
		{
			free(part);
			part = NULL;
		}
	}
	return part;
}

void libere_partition(partition_t * part)
{
	free(part->tab);
	free(part);
}

void affiche_partition(partition_t * part)
{	
	char * indice = (char *) malloc((part->N*sizeof(char)+1)*4);
	if (indice != NULL)
	{
		char * classe = (char *) malloc((part->N*sizeof(char)+1)*4);
		if (classe != NULL)
		{	
			int i;
			for (i = 0; i < part->N; i++)
			{
				sprintf(&(indice[i*4]), "%3d ", i);
				sprintf(&(classe[i*4]), "%3d ", part->tab[i]);
			}
			indice[i*4] = '\0';
			classe[i*4] = '\0';
			printf("%s\n%s\n", indice, classe);
			free(classe);
		}
		free(indice);
	}
}

int recuperer_classe(partition_t * part, int elt)
{
	int classe = -1;
	if (elt >= 0 && elt < part->N)
	{
		classe = part->tab[elt];
	}
	return classe;
}

int fusionner(partition_t * part, int i, int j)
{	
	int code = 1;

	int classe_i = recuperer_classe(part, i);
	int classe_j = recuperer_classe(part, j);

	if (i != j && classe_i != -1 && classe_j != -1)
	{
		for (int k=0; k < part->N; k++)
		{
			if (part->tab[k] == classe_j)
			{
				part->tab[k] = classe_i;
			}
		}
	}
	else
	{
		code = 0;
	}
	return code;
}

cellule_t * lister_classe(partition_t * part, int classe)
{
	cellule_t * tete = NULL;
	cellule_t * cour = NULL;
	int i = part->N-1, continuer = 1;;

	while (continuer && i >= 0)
	{
		if (part->tab[i] == classe)
		{
			cour = AllocationMaillon(i);
			if (cour != NULL)
			{
				insere_tete(&tete, cour);
			}
			else
			{
				continuer = 0;
				LibererListe(&tete);
				tete = NULL;
			}
		}
		i--;
	}
	return tete;
}

cellule_t ** lister_partition(partition_t * part)
{
	cellule_t ** classes = (cellule_t **) malloc(part->N*sizeof(cellule_t *));
	if (classes != NULL)
	{
		for (int i=0; i < part->N; i++)
		{
			classes[i] = lister_classe(part,i);
		}
	}
	return classes; 
}

void libere_tab_classe(cellule_t ** tab, int n)
{
	for (int i=0; i < n; i++)
	{
		LibererListe(&(tab[i]));
	}
	free(tab);
}

void affiche_classes(cellule_t ** classes, int n)
{
	for (int i=0; i < n; i++)
	{
		printf("Classe : %d\n", i);
		AfficheListe(&(classes[i]));
		printf("\n");
	}
}
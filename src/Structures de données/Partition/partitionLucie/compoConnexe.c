#include "compoConnexe.h"

int ** initMatrice(int n){
	srand(time(NULL));
	int **matrice = malloc(sizeof(int*)*n);
	if(matrice){
		for(int i = 0; i < n; i++){
			matrice[i] = malloc(sizeof(int)*n);
		}
	}

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if(rand()%15 == 0)
				matrice[i][j] = 1;
		}
	}
	return matrice;
}


void afficherMatrice(int **matrice, int n){
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			printf("%d ", matrice[i][j]);
		}
		printf("\n");
	}
}

void libererMatrice(int **matrice, int n){
	for(int i = 0; i < n; i++){
			free(matrice[i]);
	}
}

graphe_t * initGraphe(int n){
	srand(time(NULL));
	graphe_t *graphe = malloc(sizeof(graphe_t));

	if(graphe){
		graphe->nbNoeud = n;

		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				if(rand()%15 == 0 && i!=j){
					cellule2_t * nouv = AllocationMaillon2(i, j);
					ADJ_CEL2(&(graphe->tabArete), nouv);
				}
			}
		}
	} else{
		fprintf(stderr, "Probleme malloc graphe\n");
	}
	return graphe;
}

void afficherGraphe(graphe_t *graphe){
	AfficheListe2(&(graphe->tabArete));
}



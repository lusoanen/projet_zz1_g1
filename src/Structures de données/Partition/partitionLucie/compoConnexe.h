#ifndef _COMPOCON_H_
#define _COMPOCON_H_

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "liste2.h"
#include "partition.h"


typedef struct graphe{
	int nbNoeud;
	cellule2_t * tabArete;
}graphe_t;

int ** initMatrice(int n);
void afficherMatrice(int **matrice, int n);
void libererMatrice(int **matrice, int n);

graphe_t * initGraphe(int n);
void afficherGraphe(graphe_t *graphe);

#endif
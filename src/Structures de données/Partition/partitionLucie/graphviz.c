#include "graphviz.h"	

void partitionVersDot(partition_t *partition){
	FILE * fic;
	fic = fopen("partition.dot", "w");
	if(fic != NULL){
		fprintf(fic, "digraph Partition{\n" );
		for(int i = 0; i < partition->N; i++){
			fprintf(fic, "\t%d -> %d;\n",i, partition->adr[i]->pere->val);
		}
		fprintf(fic, "}\n" );
		fclose(fic);
	} else {
		fprintf(stderr, "Le fichier ne s'ouvre pas\n");
	}
}

void matriceVersDot(int **matrice, int nbElem){
	FILE * fic;
	fic = fopen("matrice.dot", "w");
	if(fic != NULL){
		fprintf(fic, "graph Matrice{\n" );
		for(int i = 0; i < nbElem; i++){
			fprintf(fic, "\t%d;\n",i);
			for(int j = 0; j < nbElem; j++){
				if(matrice[i][j] == 1){
					fprintf(fic, "\t%d -- %d;\n",i, j);
				}
			}
		}
		fprintf(fic, "}\n" );
		fclose(fic);
	} else {
		fprintf(stderr, "Le fichier ne s'ouvre pas\n");
	}
}

void grapheVersDot(graphe_t g){
	FILE * fic;
	fic = fopen("graphe.dot", "w");
	cellule2_t *cour = g.tabArete;
	if(fic != NULL){
		fprintf(fic, "graph Graphe{\n" );
		for(int i = 0; i < g.nbNoeud; i++){
			fprintf(fic, "\t%d;\n",i);
		}
		while(cour){
			fprintf(fic, "\t%d -- %d;\n",cour->ext1, cour->ext2);
			cour = cour -> suivant;
		}
		fprintf(fic, "}\n" );
		fclose(fic);
	} else {
		fprintf(stderr, "Le fichier ne s'ouvre pas\n");
	}
}

void foretVersDot(cellule2_t * foret){
	FILE * fic;
	fic = fopen("foret.dot", "w");
	cellule2_t *cour = foret;
	if(fic != NULL){
		fprintf(fic, "graph Foret{\n" );
		while(cour){
			fprintf(fic, "\t%d -- %d;\n",cour->ext1, cour->ext2);
			cour = cour -> suivant;
		}
		fprintf(fic, "}\n" );
		fclose(fic);
	} else {
		fprintf(stderr, "Le fichier ne s'ouvre pas\n");
	}
}

void afficherForetGraphviz(){
	system("dot -Tjpg -o foret.jpg foret.dot");
}

void afficherPartitionGraphviz(){
	system("dot -Tjpg -o partition.jpg partition.dot");
}


void afficherMatriceGraphviz(){
	system("dot -Tjpg -o matrice.jpg matrice.dot");
}

void afficherGrapheGraphviz(){
	system("dot -Tjpg -o graphe.jpg graphe.dot");
}
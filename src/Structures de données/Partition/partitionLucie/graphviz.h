#ifndef _GRAPHVIZ_H_
#define _GRAPHVIZ_H_

#include <stdlib.h>
#include <stdio.h>
#include "partition.h"
#include "compoConnexe.h"

void partitionVersDot(partition_t *partition);
void matriceVersDot(int **matrice, int n);
void grapheVersDot(graphe_t g);
void foretVersDot(cellule2_t * foret);
void afficherForetGraphviz();
void afficherPartitionGraphviz();
void afficherMatriceGraphviz();
void afficherGrapheGraphviz();

#endif
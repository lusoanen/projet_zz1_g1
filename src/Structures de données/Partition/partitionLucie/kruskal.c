#include "kruskal.h"

cellule2_t * trieArete(cellule2_t * tabArete){
	return tabArete;
}


int compteurArete(graphe_t * g){
	int cmp = 0;
	cellule2_t * cour = g->tabArete;
	while(cour){
		cour = cour->suivant;
		cmp++;
	}

	return cmp;
}

void afficherForet(cellule2_t * foret){
	cellule2_t * cour = foret;
	while(cour){
		printf("%d -- %d\n", cour->ext1, cour->ext2);
		cour = cour->suivant;
	}
}

cellule2_t * kruskal(int n, cellule2_t* (*trie)(cellule2_t*), graphe_t *graphe){

	//graphe_t *graphe = initGraphe(n);
	//afficherGraphe(graphe);
	cellule2_t *A = NULL;

	if(graphe){

		int nbArete = compteurArete(graphe);
		cellule2_t * tmp = malloc(sizeof(cellule2_t)*nbArete);
		if(tmp){
			int i = 0;
			cellule2_t *cour = graphe->tabArete;

			while(cour){
				tmp[i].ext1 = cour->ext1;
				tmp[i].ext2 = cour->ext2;

				i++;
				cour = cour->suivant;
			}

			tmp = (*trie)(tmp);

			partition_t * partition = creer_partition(n);
			

			if(partition){
				int classe1, classe2;

				for(int i = 0; i <nbArete; i++){
					classe1 = recuperer_classe(partition, tmp[i].ext1);
					classe2 = recuperer_classe(partition, tmp[i].ext2);
					if(classe2 != classe1){
						fusionner(partition, tmp[i].ext1, tmp[i].ext2);
						cellule2_t * nouv = AllocationMaillon2(tmp[i].ext1, tmp[i].ext2);
						if(nouv)
							ADJ_CEL2(&A, nouv);
					}
					
				}
			}

			free(tmp);
		}
	}
	afficherForet(A);
	return A;
}


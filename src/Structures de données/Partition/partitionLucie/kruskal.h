#ifndef _KRUSKAL_H_
#define _KRUSKAL_H_

#include <stdlib.h>
#include <stdio.h>

#include "compoConnexe.h"
#include "partition.h"
#include "liste2.h"
#include "liste.h"


cellule2_t * trieArete(cellule2_t * tabArete);
int compteurArete(graphe_t * g);
cellule2_t * kruskal(int n,cellule2_t* (*trie)(cellule2_t*), graphe_t *g);
void afficherForet(cellule2_t * foret);


#endif
#ifndef _LISTE2_H_
#define _LISTE2_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct cellule2
{
    int  ext1;
    int ext2;
    struct cellule2   * suivant;
}cellule2_t;

cellule2_t * AllocationMaillon2(int a, int b);

void ADJ_CEL2(cellule2_t **prec, cellule2_t *elt);

void SUP_CEL2(cellule2_t **prec);

void LibererListe2(cellule2_t **tete);

void AfficheListe2(cellule2_t **tete);
#endif
#include <stdio.h>
#include "partition.h"
#include "liste.h"
#include "graphviz.h"
#include "compoConnexe.h"
#include "kruskal.h"



int main()
{
	/*
	partition_t * part = creer_partition(10);
	if (part != NULL)
	{
		affiche_classes(part->classes, part->N);
		printf("\n");
		fusionner(part, 2, 3);
		fusionner(part, 2, 3);
		fusionner(part, 3, 10);
		fusionner(part, 3, 9);
		affiche_classes(part->classes, part->N);

		partitionVersDot(part);
		afficherPartitionGraphviz();

		libere_partition(part);
	}

	int ** matrice = initMatrice(10);
	if(matrice){
		afficherMatrice(matrice, 10);
		matriceVersDot(matrice, 10);
		afficherMatriceGraphviz();

		libererMatrice(matrice, 10);
	}
	
	
*/
	graphe_t *g = initGraphe(20);
	if(g){
		afficherGraphe(g);
		grapheVersDot(*g);
		afficherGrapheGraphviz();
	
	}

	

	cellule2_t * (*trie)(cellule2_t*);
	trie = trieArete;

	cellule2_t * foret = kruskal(20, trie, g);
	foretVersDot(foret);
	afficherForetGraphviz();

	return 0;
}
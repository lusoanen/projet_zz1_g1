#include "partition.h"

noeud_t * creer_noeud(int val)
{
	noeud_t * noeud = (noeud_t *) malloc(sizeof(noeud_t));
	if (noeud != NULL)
	{
		noeud->val = val;
		noeud->pere = noeud;
	}
	return noeud;
}

partition_t * creer_partition(int n)
{	
	int ok = 1;
	partition_t * part = (partition_t *) malloc(sizeof(partition_t));
	if(part != NULL)
	{
		part->N = n;
		part->adr = (noeud_t **) malloc(n*sizeof(noeud_t *));
		if (part->adr != NULL)
		{
			part->hauteur = (int *) malloc(n*sizeof(int));
			if (part->hauteur != NULL)
			{
				part->classes = (cellule_t **) malloc(n*sizeof(cellule_t *));
				if (part->classes != NULL)
				{
					int i = 0, continuer = 1;
					while (continuer && i < n)
					{
						noeud_t * noeud = creer_noeud(i);
						if (noeud != NULL)
						{	
							part->classes[i] = AllocationMaillon(i);
							if (part->classes[i] != NULL)
							{								
								part->adr[i] = noeud;
								part->hauteur[i] = 1;
							}
							else
							{
								ok = 0;
								free(noeud);
							}
						}
						else
						{
							ok = 0;
						}
						if (!ok)
						{
							for (int j=0; j < i; j++)
							{
								free(part->adr[j]);
								LibererListe(&(part->classes[j]));
							}
						}
						i++;
					}
					if (!ok)
					{
						free(part->classes);
					}
				}
				else
				{
					ok = 0;
				}
				if (!ok)
				{
					free(part->hauteur);
				}
			}
			else
			{
				ok = 0;				
			}
			if (!ok)
			{
				free(part->adr);
			}
		}
		else
		{
			ok = 0;				
		}
		if (!ok)
		{
			free(part);
			part = NULL;
		}
	}
	return part;
}

void libere_partition(partition_t * part)
{
	for (int i=0; i < part->N; i++)
	{
		free(part->adr[i]);
		LibererListe(&(part->classes[i]));
	}
	free(part->adr);
	free(part->hauteur);
	free(part->classes);
	free(part);
}

int recuperer_classe(partition_t * part, int elt)
{
	int classe = -1;
	if (elt >= 0 && elt < part->N)
	{
		noeud_t * cour = part->adr[elt];
		while (cour != cour->pere)
		{
			cour = cour->pere;
		}
		classe = cour->val;
	}
	return classe;
}

int fusionner(partition_t * part, int i, int j)
{
	int ok = 1;
	int classe_i = recuperer_classe(part, i);
	int classe_j = recuperer_classe(part, j);
	if (i != j && classe_i != -1 && classe_j != -1 && classe_i != classe_j)
	{
		int hauteur_i = part->hauteur[classe_i];
		int hauteur_j = part->hauteur[classe_j];
		
		if (hauteur_i <= hauteur_j)
		{
			part->adr[classe_i]->pere = part->adr[classe_j];
			cellule_t * cour = part->classes[classe_i];
			while (cour != NULL)
			{
				cellule_t * temp = cour->suivant;
				insere_tete(&(part->classes[classe_j]), cour);
				cour = temp;
			}			
			part->classes[classe_i] = NULL;
			part->hauteur[classe_i] = 0;
			if (hauteur_i == hauteur_j)
			{
				part->hauteur[classe_j]++;
			}
		}
		else 
		{	
			cellule_t * cour = part->classes[classe_j];
			while (cour != NULL)
			{
				cellule_t * temp = cour->suivant;
				insere_tete(&(part->classes[classe_i]), cour);
				cour = temp;
			}
			part->adr[classe_j]->pere = part->adr[classe_i];
			part->classes[classe_j] = NULL;
			part->hauteur[classe_j] = 0;
		}	
	}
	else
	{
		ok = 0;
	}
	return ok;
}

// void affiche_partition(partition_t * part)
// {
// 	char * indice = (char *) malloc((part->N*sizeof(char)+1)*4);
// 	if (indice != NULL)
// 	{
// 		char * classe = (char *) malloc((part->N*sizeof(char)+1)*4);
// 		if (classe != NULL)
// 		{	
// 			int i;
// 			for (i = 0; i < part->N; i++)
// 			{
// 				sprintf(&(indice[i*4]), "%3d ", i);
// 				sprintf(&(classe[i*4]), "%3d ", recuperer_classe(part, i));
// 			}
// 			indice[i*4] = '\0';
// 			classe[i*4] = '\0';
// 			printf("%s\n%s\n", indice, classe);
// 			free(classe);
// 		}
// 		free(indice);
// 	}
// }

cellule_t * lister_classe(partition_t * part, int classe)
{
	return part->classes[classe];
}

cellule_t ** lister_partition(partition_t * part)
{
	return part->classes;
}

void affiche_classes(cellule_t ** classes, int n)
{
	for (int i=0; i < n; i++)
	{
		printf("Classe %d : \n", i);
		AfficheListe(&(classes[i]));
	}
}
#ifndef _PARTITION_H_
#define _PARTITION_H_

#include <stdlib.h>
#include <stdio.h>
#include "liste.h"

typedef struct noeud
{
	int val;
	struct noeud * pere; 
}noeud_t;

typedef struct partition
{
	int N;
	noeud_t ** adr;
	int * hauteur;
	cellule_t ** classes;
}partition_t;

noeud_t * creer_noeud(int val);

partition_t * creer_partition(int n);

void libere_partition(partition_t * part);

void affiche_partition(partition_t * part);

int recuperer_classe(partition_t * part, int elt);

int fusionner(partition_t * part, int i, int j);

cellule_t * lister_classe(partition_t * t, int classe);

cellule_t ** lister_partition(partition_t * t);

void affiche_classes(cellule_t ** classes, int n);

#endif
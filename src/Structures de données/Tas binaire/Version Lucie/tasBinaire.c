#include "tasBinaire.h"

void initTas(noeud_t tas[TAILLEMAX]){
	int i=0;
	for(;i<TAILLEMAX; i++){
		tas[i].identifiant = 0;
		tas[i].distance = 0;
	}
}

noeud_t * initNoeud(int id, int dist){
	noeud_t *nouveau = (noeud_t*)malloc(sizeof(noeud_t));
	if(nouveau != NULL){
		nouveau->identifiant = id;
		nouveau->distance = dist;
	} else{
		fprintf(stderr, "errno : %d", errno);
	}
	return nouveau;
}

void inverser2Noeuds(int indiceA, int indiceB, noeud_t tas[TAILLEMAX]){
	noeud_t tmp = tas[indiceA];
	tas[indiceA] = tas[indiceB];
	tas[indiceB] = tmp;
}

void insererNoeud(noeud_t *aRanger, noeud_t tas[TAILLEMAX]){
	int i=0, trouver = 0;

	while(tas[i].identifiant != 0){ //on considère l'identifiant de sommet 0 comme invalide
		i++;
	}
	tas[i] = *aRanger;

	while(trouver !=1 && i != 0){
		if(tas[i].distance < tas[(i-1)/2].distance){
			inverser2Noeuds(i, (i-1)/2, tas);
			i = (i-1)/2;
		}else{
			trouver = 1;
		}
	}

}

void retirerNoeud(noeud_t *aRetirer, noeud_t tas[TAILLEMAX]){
	int i = 0, indiceARetirer, existe = 0, trouver = 0;
	while(tas[i].identifiant != 0){
		if(tas[i].identifiant == aRetirer->identifiant){
			indiceARetirer = i;
			existe = 1;
		}	
		i++;
	}
	i--;

	if(existe != 1)
		printf("\nCe noeud n'est pas dans le tas \n\n");
	else {
		inverser2Noeuds(indiceARetirer, i, tas);

		tas[i].identifiant = 0;
		tas[i].distance = 0;

		i = indiceARetirer;

		while(trouver != 1){
			if(tas[i].distance > tas[(2*i) +1].distance && tas[(2*i) +1].identifiant != 0 && tas[(2*i) +1].distance < tas[(2*i) +2].distance){
				inverser2Noeuds(i, (2*i) +1, tas);
				i = (2*i) +1;
			}
			else{
				if(tas[i].distance > tas[(2*i) +2].distance && tas[(2*i) +2].identifiant != 0){
					inverser2Noeuds(i, (2*i) +2, tas);
					i = (2*i) +2;
				}
				else
					trouver = 1;
			}
		}
	}
	

}

void libererNoeud(noeud_t *aLiberer){
	free(aLiberer);
}

void afficherTas(noeud_t tas[TAILLEMAX]){
	int i = 0;
	for(;i<TAILLEMAX; i++){
		printf("sommet : %d\ndistance : %d\n\n",tas[i].identifiant, tas[i].distance);
	}
}

void trierTas(noeud_t tasNonTrie[TAILLEMAX]){
	int i = 0;

	noeud_t tasTrie[TAILLEMAX];
	initTas(tasTrie);


	while(tasNonTrie[0].identifiant != 0){
		tasTrie[i] = tasNonTrie[0];
		retirerNoeud(&tasNonTrie[0], tasNonTrie);
		i++;
	}

	i=0;
	while(tasTrie[i].identifiant != 0){
		tasNonTrie[i] = tasTrie[i];
		i++;
	}
}

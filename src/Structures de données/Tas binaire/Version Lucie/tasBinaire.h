#ifndef _TAS_BIN_H_
#define _TAS_BIN_H_

#define TAILLEMAX 10 /**< Taille maximal du tas */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>

typedef struct noeud{
	int identifiant; /**< identifiant unique d'un noeud (!= 0) */
	int distance; /**< distance du noeud dans le graphe */
} noeud_t;

/**
 * @fn void initTas(noeud_t tas[TAILLEMAX])
 * @brief initialise toute les valeurs (noeud composé d'un iddentifiant et d'une distance) du tableaux à 0
 * 
 * @param tas tas binaire utilisé
 */
void initTas(noeud_t tas[TAILLEMAX]);

/**
 * @fn noeud_t * initNoeud(int id, int dist)
 * @brief initialisation et allocation mémoire pour un nouveau noeud
 * 
 * @param id identifiant du nouveau noeud
 * @param dist distance du nouveaux noeud
 * @return pointeur sur le noeud alloué 
 */
noeud_t * initNoeud(int id, int dist);

/**
 * @fn void inverser2Noeuds(int indiceA, int indiceB, noeud_t tas[TAILLEMAX])
 * @brief inverse les noeuds d'indice A et B dans le tas binaire
 * 
 * @param indiceA indice du premier noeud
 * @param indiceB indice du deuxième noeud
 * @param tas tas binaire utilisé
 */
void inverser2Noeuds(int indiceA, int indiceB, noeud_t tas[TAILLEMAX]);

/**
 * @fn void insererNoeud(noeud_t *aRanger, noeud_t tas[TAILLEMAX])
 * @brief insertion d'un noeud dans le tas binaire
 * 
 * @param aRanger noeud a inserer
 * @param tas tas binaire utilisé
 */
void insererNoeud(noeud_t *aRanger, noeud_t tas[TAILLEMAX]);

/**
 * @fn void retirerNoeud(noeud_t *aRetirer, noeud_t tas[TAILLEMAX])
 * @brief retire un noeud donnée du tas
 * 
 * @param aRetirer noeud a enlever 
 * @param tas tas binaire utilisé
 */
void retirerNoeud(noeud_t *aRetirer, noeud_t tas[TAILLEMAX]);

/**
 * @fn void libererNoeud(noeud_t *aLiberer)
 * @brief libération de l'espace mémoire alloué à un noeud
 * 
 * @param aLiberer noeud à libérer
 */
void libererNoeud(noeud_t *aLiberer);

/**
 * @fn void afficherTas(noeud_t tas[TAILLEMAX]) 
 * @brief affiche tout les élément du tas, y compris les cases, du tableau représentant le tas, "vide"
 * 
 * @param tas tas binaire
 */
void afficherTas(noeud_t tas[TAILLEMAX]);

/**
 * @fn void trierTas(noeud_t tas[TAILLEMAX])
 * @brief trie le tas
 * 
 * @param tas tas binaire à trier
 */
void trierTas(noeud_t tas[TAILLEMAX]);
#endif
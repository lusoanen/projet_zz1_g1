#include <time.h>
#include <stdlib.h>
#include <time.h>
#include "tas_binaire.h"

int compar (const void * a, const void * b)
{
	return (*(int*)a - *(int*)b);
}

int main()
{
	srand(time(0));

	maillon_t ** tab1 = (maillon_t **) malloc(TAILLEMAX*sizeof(maillon_t*));
	if (tab1 == NULL) 
		exit(EXIT_FAILURE);
	int * tab2 = (int *) malloc(TAILLEMAX*sizeof(int *));
	if (tab2 == NULL)
		exit(EXIT_FAILURE);
	
	for (int i=0; i < TAILLEMAX; i++)
	{
		int alea = rand()%150;
		tab2[i] = alea;
		maillon_t * elt = init_maillon(i+1, alea);
		if (elt != NULL)
		{
			tab1[i] = elt;
		}
	}
	printf("Tri par tas :\n");
	// affiche_tab(tab1, TAILLEMAX);
	clock_t deb = clock();
	tri_par_tas(tab1, TAILLEMAX);
	clock_t fin = clock();
	// affiche_tab(tab1, TAILLEMAX);
	printf("Temps : %fsec\n\n", (double)(fin-deb)/CLOCKS_PER_SEC);

	printf("qsort :\n");
	int (*pf)(const void *, const void *);
	pf = compar;
	// for (int i=0; i < TAILLEMAX; i++)
	// {
	// 	printf("%d  ", tab2[i]);
	// }
	// printf("\n");
	deb = clock();
	qsort(tab2, TAILLEMAX, sizeof(int), pf);
	fin = clock();
	// for (int i=0; i < TAILLEMAX; i++)
	// {
	// 	printf("%d  ", tab2[i]);
	// }
	// printf("\n");
	printf("Temps : %fsec\n\n", (double)(fin-deb)/CLOCKS_PER_SEC);

	for (int i = 0; i < TAILLEMAX; i++)
	{
		free(tab1[i]);
	}
	free(tab1);
	free(tab2);

	return 0;
}


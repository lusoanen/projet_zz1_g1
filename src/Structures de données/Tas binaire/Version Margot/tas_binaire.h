#ifndef _TAS_BIN_H_
#define _TAS_BIN_H_

#include <stdlib.h>
#include <stdio.h>

#define TAILLEMAX 1000000
typedef struct maillon
{
	int ident;
	int val;
} maillon_t;

typedef struct tas
{
	int taille;
	maillon_t * tab[TAILLEMAX];
} tas_t;

maillon_t * init_maillon(int id, int v);

tas_t * init_tas();

void free_tas(tas_t * tas);

void enfiler(tas_t * tas, maillon_t * elt);

maillon_t * defiler(tas_t * tas);

void echange(tas_t * tas, int i, int k);

void tasser_parent(tas_t * t, int i);

void tasser_fils(tas_t * tas, int i);

void affiche_tas(tas_t * tas);

void affiche_tab(maillon_t * tab[], int taille);

int tri_par_tas(maillon_t ** tab, int taille);

#endif
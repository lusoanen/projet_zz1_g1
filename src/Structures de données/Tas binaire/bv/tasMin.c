#include"tasMin.h"

int main(){
    tasmin_t tas = construction(10);
    ajouter(tas,5);
    ajouter(tas,1);
    ajouter(tas,3);
    ajouter(tas,6);
    ajouter(tas,2);
    ajouter(tas,8);
    ajouter(tas,0);
    ajouter(tas,7);
    
    afficheTas(tas);
    tasmin_t tasQsort =  copiTas(tas);

    clock_t temps_Qsort = clock();
    qsort(&tasQsort.tab[1], *tasQsort.indice, sizeof(int), plusGrand);
    temps_Qsort = clock()-temps_Qsort;
    

    clock_t temps_triTas = clock();
    triParTas(tas);
    temps_triTas = clock()-temps_triTas;

    double tpsTriTas =(double)(temps_triTas/CLOCKS_PER_SEC), tpsQsort =(double)(temps_Qsort/CLOCKS_PER_SEC);
    afficheTas(tas);
    printf("\n comparaison des temps\ntri par tas %d\tqsort %d\n",temps_triTas,temps_Qsort); 
    // le tri par tas est nettement plus rapide sur cet exemple du moins
    
    free(tas.tab);
}


void ajouter(tasmin_t tas,int val){
    *tas.indice += 1;
    tas.tab[*tas.indice]=val;
    diminuer(tas,*tas.indice);
}
void retirer(tasmin_t tas){ // enleve a la racine
    tas.tab[1] = tas.tab[*tas.indice];
    *tas.indice -=1;
    augmenter(tas,1);
}

void diminuer(tasmin_t tas,int clef){
    if( clef && tas.tab[clef]<tas.tab[clef/2]){
        echange(&tas.tab[clef],&tas.tab[clef/2]);
        diminuer(tas,clef/2);
    }
}
void augmenter(tasmin_t tas,int clef){
    int choixFils;
    if(clef<= *tas.indice &&  (clef*2)<= *tas.indice){
        choixFils =( ((clef*2)+1 <= *tas.indice) && (tas.tab[(clef*2)+1] <tas.tab[clef*2])); // 1 si fils droit plus petit
        if( tas.tab[clef]>tas.tab[clef*2 + choixFils]){
            echange(&tas.tab[clef],&tas.tab[clef*2 + choixFils]);
            augmenter(tas,clef*2 + choixFils);
        }
    }
}
void echange(int* a,int* b){
    int c= *a;
    *a=*b;
    *b=c; 
}
tasmin_t construction(int taille){ // insertion sans proprietes puis tri
    tasmin_t tas;
    tas.tab = (int*)malloc((1+taille)*sizeof(int));
    int i;
    tas.indice= (int*)malloc(sizeof(int));
    *tas.indice= 1; // le 1er element est en position 1
    tas.taille = taille;
    for(i=1;i<taille;i++){
        tas.tab[i]=0;
    }
    return tas;
}
void afficheTas(tasmin_t tas){
    int i;
    printf("\nTas min \n");
    for(i=1;i<*tas.indice;i++){
        printf("%d\t",tas.tab[i]);
    }
    printf("\n");
}

int plusGrand(const void * a,const void * b){
    return (*(int  const *)a > *(int  const *)b);
}

void tamiser(tasmin_t tas, int noeud, int n){
    int k = noeud,j = 2*k;
    while(j<n){
        if( j< n && tas.tab[j]<tas.tab[j+1])
            j++;
        if(tas.tab[k]<tas.tab[j]){
            echange(&tas.tab[k],&tas.tab[j]);
            k = j;
            j = 2*k;
        }
        else{
            j = n +1;
        }
    }
}

void triParTas(tasmin_t tas){
    int i;
    for(i=*tas.indice/2;i>1;i--)
        tamiser(tas,i,*tas.indice);
    for(i =*tas.indice ;i>2;i--){
        echange(&tas.tab[i],&tas.tab[1]);
        tamiser(tas,1,i-1);
                    
    }
}

tasmin_t copiTas(tasmin_t tasSource){
    int i;
    tasmin_t tasNouv = construction(tasSource.taille);
    *tasNouv.indice = *tasSource.indice;
    for(i=1;i<*tasNouv.indice;i++)
        tasNouv.tab[i] = tasSource.tab[i];

    return tasNouv;
}
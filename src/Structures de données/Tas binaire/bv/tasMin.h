#ifndef _TASMIN_H_
#define _TASMIN_H_

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

// commencer a 1 et stocker le dernier element en 0

struct tasmin_t{
    int* tab;
    int taille;
    int* indice;
};
typedef struct tasmin_t tasmin_t;

int main();
void ajouter(tasmin_t,int);
void retirer(tasmin_t); // enleve a la racine
void diminuer(tasmin_t,int);
void echange(int*,int*);
void augmenter(tasmin_t,int);
tasmin_t construction(int); // insertion sans proprietes puis tri

int plusGrand(const void *,const void*);
void tamiser(tasmin_t,int,int);
void triParTas(tasmin_t);
void afficheTas(tasmin_t tas);

tasmin_t copiTas(tasmin_t);
#endif
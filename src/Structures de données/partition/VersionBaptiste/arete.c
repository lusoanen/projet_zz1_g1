#include "arete.h"

cellule_arete_t * AllocationMaillon_Arete(arete_t x)
{
    cellule_arete_t 		 * elt = (cellule_arete_t*) malloc(sizeof(cellule_arete_t));
    if (elt != NULL)
    {
        elt->val = x;
        elt->suiv = NULL;
    }
    return elt;
}

void ADJ_CEL_Arete(cellule_arete_t **prec, cellule_arete_t *elt)
{
	elt->suiv = *prec;
	*prec = elt;
}

void SUP_CEL_Arete(cellule_arete_t **prec)
{
    cellule_arete_t 		 * temp = *prec;
    *prec = temp->suiv;
    free(temp);
}

void LibererListe_Arete(cellule_arete_t **tete)
{
    cellule_arete_t        * cour = *tete;
    cellule_arete_t        * prec = NULL;

    while(cour != NULL)
    {
        prec = cour;
        cour = cour->suiv;
        free(prec);
    }
}

void AfficheListe_Arete(cellule_arete_t ** tete)
{
    cellule_arete_t        * cour = *tete;
    int                cpt=0;

    // printf("affichage de la liste :\n");
    while(cour != NULL)
    {
    	cpt++;

        printf("%d\t%d\n",cour->val.min,cour->val.max);
        cour = cour->suiv;
    }
    printf("\n");
}


int tailleListe_Arete(cellule_arete_t * tete){
    cellule_arete_t * cour = tete;
    int taille = 0;
    while(cour!=NULL){
        taille++;
        cour = cour->suiv;
    }
    return taille;
}
/*
void insere_tete(cellule_t ** tete, cellule_t * elt)
{
	ADJ_CEL(tete,elt);
}


cellule_t** rech_Prec(cellule_t* tete,int val){
    cellule_t * cour = tete;
    cellule_t ** prec = &tete;
    while(cour!= NULL && cour->val !=val){
        *prec = cour;
        cour = cour->suivant;
    }
    if(cour == NULL)
        prec = NULL;
    return prec;
}*/
#ifndef _LISTE_ARETE_H_
#define _LISTE_ARETE_H_

#include <stdio.h>
#include <stdlib.h>
#include "structuresArete.h"

typedef struct cellule_arete_t
{
    arete_t                val;
    struct cellule_arete_t * suiv;
}cellule_arete_t;

cellule_arete_t * AllocationMaillon_Arete(arete_t x);

void ADJ_CEL_Arete(cellule_arete_t **prec, cellule_arete_t *elt);

void SUP_CEL_Arete(cellule_arete_t **prec);

void LibererListe_Arete(cellule_arete_t **tete);

void AfficheListe_Arete(cellule_arete_t **tete);

int tailleListe_Arete(cellule_arete_t * tete);

//cellule_t** rech_Prec(cellule_t*,int);

#endif
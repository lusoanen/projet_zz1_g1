#ifndef _BASE_SDL_H_
#define _BASE_SDL_H_

#include<stdio.h>
#include<stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

void Init(SDL_Window** fenetre ,SDL_Renderer** rendu,SDL_DisplayMode * ecran); // initialisation SDL
void fermetureSdl(int ok, char const* message, SDL_Window* fenetre, SDL_Renderer* rendu); // fermture de SDL

#endif
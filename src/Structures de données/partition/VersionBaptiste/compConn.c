#include"compConn.h"


int main(){
    int taille =20;
    int** mat =  creeMatAdj(taille);
    system("rm *.png & rm *.dot");
    //affTab2D(mat,taille,taille);
    graphvizFy_MatAdj(mat,taille);
    part_arbre_t part =matAdjToPartArbre(mat,taille);
    graphvizFy(part);
    //graphvizFy_Sousclasse(part,0);
    graphvizFy_ComposanteConnexe(part);
    libereTab2D(mat,taille);
    liberePart(part);

}



int** creeMatAdj(int x){
    srand(time(NULL));

    int i,j,nbArete, arete;
    int ** mat =(int**)malloc(x*sizeof(int*));

    for(i=0;i<x;i++){
        mat[i] = (int*) malloc(x*sizeof(int));
        for(j=0;j<x;j++){
            mat[i][j]=0;
        }
    }

    for(i=0;i<x;i++){
        nbArete = compteArete(mat[i],x);
        j=0;
        while(j<i && nbArete<4){
            arete =(rand()& 1);
            if (arete && compteArete(mat[j],x)<4){
               mat[i][j] = arete;
               mat[j][i] = arete;
               nbArete++;
           } 
           j++;
        }
    }
    return mat;
}

int compteArete(int* ligne,int taille){
    int i=0,nbArete =0;
    while(i<taille && nbArete<4){
        nbArete +=ligne[i];
        i++;
    }
    return nbArete;
}

void affTab2D(int** tab,int x, int y){
    int i , j;
    printf("\nTableau \n");
    for(i=0;i<x;i++){
        for(j=0;j<y;j++){
            printf("%d\t",tab[i][j]);
        }
        printf("\n");
    }
}

void libereTab2D(int** tab,int x){
    int i;
    for(i=0;i<x;i++){
        free(tab[i]);
    }
    free(tab);
}


void graphvizFy_MatAdj(int** mat,int taille){

    int i,j;

    FILE * file = fopen("graphMatAdj.dot","w");
    fprintf(file,"graph matAdj {\n");
    for(i=0;i<taille;i++){
        fprintf(file,"%d",i);
        for(j=0;j<i;j++){
            if (mat[i][j])
                fprintf(file,"--%d;\n%d",j,i);
        }
        fprintf(file,";\n");
    }
    fprintf(file,"}");
    fclose(file);
    system("dot -Tpng graphMatAdj.dot -o graphMatAdj.png");
 }


 part_arbre_t matAdjToPartArbre(int** mat, int taille){
    part_arbre_t part =  creePartArbre( taille);
    int i,j;
    for(i=0;i<taille;i++){
        for(j=0;j<i;j++){
            if (mat[i][j])
                fusion(part,i,j);
        }
    }
    return part;
 }


void graphvizFy_Sousclasse(part_arbre_t part,int classe){
    int * hash = (int*)malloc(part.taille*sizeof(int));
    int i=0,j;
    cellule_t * listeIndice = listerIndiceClasse(part,classe);
    cellule_t * cour = listeIndice;
    for(i=0;i<part.taille;i++)
        hash[i]=0;
    i=0;
    while(cour !=NULL){
        hash[cour->val] = 1;
        i++;
        cour = cour->suivant;
    }

    char * path = (char *)malloc(sizeof(char)*120);
    sprintf(path,"sous_graphe_de_%d.dot",classe);
    FILE * file = fopen(path,"w");

    fprintf(file,"graph sous_graphe_de_%d {",classe);
    for(i=0;i<part.taille;i++){
        if(hash[i]){
            fprintf(file,"%d",part.val[i]);
            j = part.clef[i];
            if(i!=j && hash[j])
                fprintf(file,"--%d",j);
            fprintf(file,";\n");
        }
    }
    fprintf(file,"}");
    fclose(file);

    sprintf(path,"dot -Tpng sous_graphe_de_%d.dot -o sous_graphe_de_%d.png",classe,classe);
    system(path);
    free(hash);
    free(path);
    LibererListe(&listeIndice);
}



void graphvizFy_ComposanteConnexe(part_arbre_t part){
    cellule_t * cour =  listerPart(part);
    
    while(cour!=NULL){
        graphvizFy_Sousclasse(part,cour->val);
        SUP_CEL(&cour);
    }
}
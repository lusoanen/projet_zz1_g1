#ifndef _COMPOSANTE_CONNEXE_H_
#define _COMPOSANTE_CONNEXE_H_

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"partitionArbre.h"
#include"liste.h"



int main();

int** creeMatAdj(int);

int compteArete(int* ligne,int taille);
void affTab2D(int** tab,int x, int y);
void libereTab2D(int** tab,int x);
void graphvizFy_MatAdj(int** mat,int x);
part_arbre_t matAdjToPartArbre(int** tab, int taille);

void graphvizFy_Sousclasse(part_arbre_t part,int classe);
void graphvizFy_ComposanteConnexe(part_arbre_t part);

#endif
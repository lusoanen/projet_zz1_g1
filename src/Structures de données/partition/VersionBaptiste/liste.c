#include "liste.h"

cellule_t * AllocationMaillon(int x)
{
    cellule_t 		 * elt = (cellule_t*) malloc(sizeof(cellule_t));
    if (elt != NULL)
    {
        elt->val = x;
        elt->suivant = NULL;
    }
    return elt;
}

void ADJ_CEL(cellule_t **prec, cellule_t *elt)
{
	elt->suivant = *prec;
	*prec = elt;
}

void SUP_CEL(cellule_t **prec)
{
    cellule_t 		 * temp = *prec;
    *prec = temp->suivant;
    free(temp);
}

void LibererListe(cellule_t **tete)
{
    cellule_t        * cour = *tete;
    cellule_t        * prec = NULL;

    while(cour != NULL)
    {
        prec = cour;
        cour = cour->suivant;
        free(prec);
    }
}

void AfficheListe(cellule_t ** tete)
{
    cellule_t        * cour = *tete;
    int                cpt=0;

    // printf("affichage de la liste :\n");
    while(cour != NULL)
    {
    	cpt++;

        printf("%3d ",cour->val);
        cour = cour->suivant;
    }
    printf("\n");
}

void insere_tete(cellule_t ** tete, cellule_t * elt)
{
	ADJ_CEL(tete,elt);
}


cellule_t** rech_Prec(cellule_t* tete,int val){
    cellule_t * cour = tete;
    cellule_t ** prec = &tete;
    while(cour!= NULL && cour->val !=val){
        *prec = cour;
        cour = cour->suivant;
    }
    if(cour == NULL)
        prec = NULL;
    return prec;
}
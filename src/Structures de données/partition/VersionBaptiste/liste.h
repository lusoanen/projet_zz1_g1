#ifndef _LISTE_H_
#define _LISTE_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct cellule
{
    int                val;
    struct cellule   * suivant;
}cellule_t;

cellule_t * AllocationMaillon(int x);

void ADJ_CEL(cellule_t **prec, cellule_t *elt);

void SUP_CEL(cellule_t **prec);

void LibererListe(cellule_t **tete);

void AfficheListe(cellule_t **tete);

void insere_tete(cellule_t ** tete, cellule_t * elt);

cellule_t** rech_Prec(cellule_t*,int);

#endif
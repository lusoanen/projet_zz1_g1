#include"noeudArete.h"

int main(){
    noeudArete_t noeudArete = creeNoeudArete(10);
    creeRandGraphe(&noeudArete);
    graphvizFy_NoeudArete(noeudArete,"origine");
    noeudArete_t kru =kruskal(noeudArete);
    graphvizFy_NoeudArete(kru,"arbreCouvrant");
    AfficheListe_Arete(&noeudArete.arete);
    randOrdreArete(&noeudArete);
    noeudArete_t fischer= kruskal(noeudArete);
    graphvizFy_NoeudArete(fischer,"fischer");
    
    libereNoeudArete(noeudArete);
    libereNoeudArete(kru);
    libereNoeudArete(fischer);
    return 0;
}


noeudArete_t creeNoeudArete(int taille){
    noeudArete_t noeud;
    noeud.taille = taille;
    noeud.tabNoeud = (int*)malloc(taille*sizeof(int));
    int i;
    for(i=0;i<taille;i++)
        noeud.tabNoeud[i] = i;
    noeud.arete = NULL;
    return noeud;
}


void creeRandGraphe(noeudArete_t * noeudArete){
    srand(time(NULL));
    int i,j,nbArete=0;
    for(i=0;i<noeudArete->taille;i++){
        nbArete = nbAreteParNoeud(*noeudArete,i);
        //printf("nbArete %d\n",nbArete);
        j=0;
        while(nbArete<4 && j<i ){
            if(nbAreteParNoeud(*noeudArete,j)<4 && rand()&1){
                ADJ_CEL_Arete(&noeudArete->arete,AllocationMaillon_Arete(creeArete(j,i,1)));
                nbArete++;
            }
            j++;
        }
    }
}

int nbAreteParNoeud(noeudArete_t noeudArete,int noeud)
{
    cellule_arete_t * cour = noeudArete.arete;
    int nb = 0;
    while(cour!=NULL && nb<4){
        if(cour->val.min == noeud || cour->val.max == noeud)
            nb++;
        cour = cour->suiv;
    }
    return nb;
}

void graphvizFy_NoeudArete(noeudArete_t noeudArete,char * truc){
    
    char * path = (char *)malloc(sizeof(char)*120);
    sprintf(path,"%s.dot",truc);
    FILE * file = fopen(path,"w");
    fprintf(file,"graph grapheNoeudArete {\n");
    
    int i;
    for(i=0;i<noeudArete.taille;i++){
            fprintf(file,"%d\n",i);
    }

    cellule_arete_t * cour = noeudArete.arete;
    while(cour != NULL){
        fprintf(file,"%d--%d [label=\"%d\"]\n",cour->val.min,cour->val.max,cour->val.valuation);
        cour = cour->suiv;
    }
    fprintf(file,"}");

    fclose(file);
    sprintf(path,"dot -Tpng %s.dot -o %s.png",truc,truc);
    system(path);
    free(path);
    
}

void libereNoeudArete(noeudArete_t noeudArete){
    free(noeudArete.tabNoeud);
    LibererListe_Arete(&noeudArete.arete);
}


noeudArete_t kruskal(noeudArete_t graph)
{
    noeudArete_t arbCouvrant =  creeNoeudArete(graph.taille);
    int i,j=0,classeI,classeJ;
    part_arbre_t part = creePartArbre(graph.taille);
    
    for(i = 0;i<graph.taille;i++){
        arbCouvrant.tabNoeud[i] = graph.tabNoeud[i];
    }
    cellule_arete_t * cour = graph.arete;
    
    
    while(cour!=NULL){
        i = cour->val.min, j = cour->val.max ;
        classeI = chercheRacine(part,i), classeJ = chercheRacine(part,j);
        if(classeJ != classeI){
            fusion(part,classeI,classeJ);
            ADJ_CEL_Arete(&arbCouvrant.arete,AllocationMaillon_Arete(creeArete(cour->val.min,cour->val.max,cour->val.valuation)));
        }
        cour =cour->suiv;
    }

    liberePart(part);
    return arbCouvrant;
}


cellule_arete_t * tabToList(cellule_arete_t ** tabCel,int taille){
    cellule_arete_t * liste =NULL;
    int i;
    for(i=0;i<taille;i++){
        ADJ_CEL_Arete(&liste,tabCel[i]);
    }
    return liste;
}

void randOrdreArete(noeudArete_t * noeudArete){
    int i=0, nbArete = tailleListe_Arete(noeudArete->arete);
    cellule_arete_t ** tabCel = (cellule_arete_t**)malloc(nbArete*sizeof(cellule_arete_t*));
    cellule_arete_t * cour = noeudArete->arete;
    cellule_arete_t * listeOutput = NULL;

    while(i<nbArete && cour != NULL){
        tabCel[i] = cour;
        cour = cour->suiv;
        if(i+1<nbArete)
            tabCel[i]->suiv = tabCel[i+1];
        else
            tabCel[i]->suiv =NULL;
        i++;
    }
    fischerYates(tabCel,nbArete);
    noeudArete->arete = tabToList(tabCel,nbArete);

    free(tabCel);
}


void fischerYates(cellule_arete_t** tabCel,int taille){
    srand(time(NULL));
    int i,j;
    cellule_arete_t * tmp;
    for(i=taille-1;i>1;i--){
        //printf("%d\t%d\n",i,j);
        j = rand()%i;
        tmp = tabCel[i];
        tabCel[i] = tabCel[j];
        tabCel[j] = tmp;
        tabCel[i]->suiv = NULL;

    }
}

    
#ifndef _NOEUD_ARETE_H_
#define _NOEUD_ARETE_H_

#include"arete.h"
#include"structuresArete.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"partitionArbre.h"

struct noeudArete{
    int * tabNoeud;
    int taille;
    cellule_arete_t * arete;
}; typedef struct noeudArete noeudArete_t;

int main();

noeudArete_t creeNoeudArete(int);

void creeRandGraphe(noeudArete_t * noeud);
void graphvizFy_NoeudArete(noeudArete_t noeudArete,char* truc);
void libereNoeudArete(noeudArete_t noeudArete);
int nbAreteParNoeud(noeudArete_t noeudArete,int noeud);
noeudArete_t kruskal(noeudArete_t graph);


void randOrdreArete(noeudArete_t * noeudArete);
void fischerYates(cellule_arete_t** tabCel,int taille);
void swapCell(cellule_arete_t* c1,cellule_arete_t*c2);

#endif
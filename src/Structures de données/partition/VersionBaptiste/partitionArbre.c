#include"partitionArbre.h"
/*
int main(){
    part_arbre_t part =  creePartArbre(12);

    cellule_t * classes ;
    cellule_t * partitions;
    cellule_t * indices;

    classes =  listerClasse(part,2);
    partitions = listerPart(part);
    indices = listerIndiceClasse(part,2);
    AfficheListe(&classes);
    AfficheListe(&partitions);
    AfficheListe(&indices);

    LibererListe(&classes);
    LibererListe(&partitions);
    LibererListe(&indices);

    fusion(part,2,5);
    fusion(part,2,3);
    fusion(part,1,10);

    
    classes =  listerClasse(part,5);
    partitions = listerPart(part);
    indices = listerIndiceClasse(part,5);

    AfficheListe(&classes);
    AfficheListe(&partitions);
    AfficheListe(&indices);

    graphvizFy(part);

    LibererListe(&classes);
    LibererListe(&partitions);
    LibererListe(&indices);

    liberePart(part);
}

*/


part_arbre_t creePartArbre(int taille){
    part_arbre_t part;
    int i;
    part.taille = taille;
    part.val = (int*)malloc(taille*sizeof(int));
    part.clef = (int*)malloc(taille*sizeof(int));
    part.hauteur = (int*)malloc(taille*sizeof(int));
    for(i=0;i<taille;i++)
    {
        part.val[i] = i;
        part.clef[i] = i;
        part.hauteur[i]=1;
    }
    return part;
}

int getClass(part_arbre_t part,int val){
    return chercheRacine(part,val);
    
}
void setClass(part_arbre_t part,int classe,int val){
    int i=0;
    while(i<part.taille && val!= part.val[i]){
        i++;
    }
    if(i<part.taille)
        part.clef[i]=classe;
    
}

int chercheRacine(part_arbre_t part,int val){
    //renvoie la classe de la racine de la valeur passee
    int courIndice=0;
    while( courIndice<part.taille && part.val[courIndice] != val )
        courIndice++;

    while(part.hauteur[courIndice] != 1)
        courIndice = part.clef[courIndice];
    return courIndice;
}

void fusion(part_arbre_t part,int x1,int x2){
    int classeX1=chercheRacine(part,x1), classeX2 = chercheRacine(part,x2);
    
    if(classeX1 != classeX2){
        cellule_t * listeX1 = listerIndiceClasse(part,classeX1);
        cellule_t * listeX2 = listerIndiceClasse(part,classeX2);
        if(hauteurArbre(part,listeX1)>hauteurArbre(part,listeX2)){
            incrementeHauteur(part,listeX2);
            part.clef[classeX2] = classeX1;
        }
        else{
            incrementeHauteur(part,listeX1);
            part.clef[classeX1] = classeX2;
            
        }
        
        LibererListe(&listeX1);
        LibererListe(&listeX2);

    }
}



void incrementeHauteur(part_arbre_t part, cellule_t * listeIndice){
    cellule_t * cour = listeIndice;
    while(cour!= NULL){
        part.hauteur[cour->val]++;
        cour = cour->suivant;
    }
}

int hauteurArbre(part_arbre_t part, cellule_t * liste){
    int hauteur =0;
    cellule_t * cour  =liste;

    while(cour != NULL){
            hauteur = (part.hauteur[cour->val]> hauteur)?part.hauteur[cour->val]: hauteur;
            cour = cour->suivant;
    }

    return hauteur;
}
cellule_t * listerIndiceClasse(part_arbre_t part, int classe){
    int racineClasse  =chercheRacine(part,classe),i,racineCour;
    cellule_t * liste = NULL;
    for(i=0;i<part.taille;i++){
        racineCour = chercheRacine(part,part.val[i]);
        if(racineCour == racineClasse)
            ADJ_CEL(&liste,AllocationMaillon(i));
    }
    return liste;
}
cellule_t * listerClasse(part_arbre_t part, int classe){
    int racineClasse  =chercheRacine(part,classe),i,racineCour;
    cellule_t * liste = NULL;
    for(i=0;i<part.taille;i++){
        racineCour = chercheRacine(part,part.val[i]);
        if(racineCour == racineClasse)
            ADJ_CEL(&liste,AllocationMaillon(part.val[i]));
    }
    return liste;
}
cellule_t * listerPart(part_arbre_t part){
    int i;
    cellule_t * liste = NULL;
    for(i = 0;i<part.taille;i++){
        if(part.hauteur[i]==1)
            ADJ_CEL(&liste,AllocationMaillon(part.clef[i]));
    }
    return liste;
}
void liberePart(part_arbre_t part){
    free(part.val);
    free(part.clef);
    free(part.hauteur);
}

void graphvizFy(part_arbre_t part){
    FILE * file = fopen("graph.dot","w");
    int i;
    fprintf(file,"digraph partition {\n");
    for(i=0;i<part.taille;i++){
        fprintf(file,"%d->%d;\n",part.val[i],part.clef[i]);
    }

    fprintf(file,"}");

    fclose(file);
    system("dot -Tpng graph.dot -o graph.png");
}
#ifndef _PART_ARBRE_
#define _PART_ARBRE_

#include <stdio.h>
#include <stdlib.h>
#include"liste.h"

struct part_arbre_t{
    int* val;
    int* clef;
    int* hauteur;
    int taille;
};
typedef struct part_arbre_t part_arbre_t;

part_arbre_t creePartArbre(int taille);
int getClass(part_arbre_t,int);
void setClass(part_arbre_t part,int classe,int val);

int hauteurArbre(part_arbre_t part, cellule_t * liste);
int chercheRacine(part_arbre_t part,int val);

void incrementeHauteur(part_arbre_t part, cellule_t * listeIndice);


void fusion(part_arbre_t,int,int);
cellule_t * listerClasse(part_arbre_t, int);
cellule_t * listerIndiceClasse(part_arbre_t, int);
cellule_t * listerPart(part_arbre_t);
void liberePart(part_arbre_t part);
void graphvizFy(part_arbre_t part);

//int main();

#endif
#include"partitionMarqueur.h"


int main(){
    part_marque_t part =  creePartMarq(12);

    cellule_t * classes =  listerPart(part);
    cellule_t * partitions = listerPart(part);
    LibererListe(&classes);
    LibererListe(&partitions);

    fusion(part,2,5);
    fusion(part,2,3);
    fusion(part,1,10);

    
    classes =  listerClasse(part,2);
    partitions = listerPart(part);

    AfficheListe(&classes);
    AfficheListe(&partitions);

    LibererListe(&classes);
    LibererListe(&partitions);

    liberePart(part);
}



part_marque_t creePartMarq(int taille){
    part_marque_t part;
    int i;
    part.taille = taille;
    part.val = (int*)malloc(taille*sizeof(int));
    part.clef = (int*)malloc(taille*sizeof(int));
    for(i=0;i<taille;i++)
    {
        part.val[i] = i;
        part.clef[i] = i;
    }
    return part;
}

int getClass(part_marque_t part,int val){
    int i=0,ret = -1;
    while(i<part.taille && val!= part.val[i]){
        
        i++;
    }
    if(i<part.taille)
        ret =i;
    
    return ret;
}
void setClass(part_marque_t part,int classe,int val){
    int i=0;
    while(i<part.taille && val!= part.val[i]){
        i++;
    }
    if(i<part.taille)
        part.clef[i]=classe;
    
}

void fusion(part_marque_t part,int x1,int x2){
    cellule_t * listeX2 = listerClasse(part,getClass(part,x2));
    cellule_t **prec = &listeX2;
    cellule_t * cour = listeX2;
    int classeX1 = getClass(part,x1);
    while(cour != NULL){
        setClass(part,classeX1,cour->val);
        *prec = cour;
        cour =cour->suivant;
        SUP_CEL(prec);
    }
    LibererListe(&listeX2);
}

cellule_t* listerClasse(part_marque_t part, int classe){
    cellule_t* tete =NULL;
    int i ;
    for(i=0;i<part.taille;i++){
        if(part.clef[i] == classe)
            ADJ_CEL(&tete,AllocationMaillon(part.val[i]));
    }
    return tete;
}
cellule_t* listerPart(part_marque_t part){
    cellule_t* tete =NULL;
    int i ;
    for(i=0;i<part.taille;i++){
        if(rech_Prec(tete,part.clef[i]) == NULL)
            ADJ_CEL(&tete,AllocationMaillon(part.clef[i]));
    }
    return tete;   
}

void liberePart(part_marque_t part){
    free(part.val);
    free(part.clef);
}
#ifndef _PART_MARQUEUR_
#define _PART_MARQUEUR_

#include <stdio.h>
#include <stdlib.h>
#include"liste.h"

struct part_marque_t{
    int* val;
    int* clef;
    int taille;
};
typedef struct part_marque_t part_marque_t;

part_marque_t creePartAbre(int taille);
int getClass(part_marque_t,int);
void setClass(part_marque_t part,int classe,int val);

void fusion(part_marque_t,int,int);
cellule_t * listerClasse(part_marque_t, int);
cellule_t * listerPart(part_marque_t);
void liberePart(part_marque_t part);

int main();

#endif
#ifndef _ARETE_H_
#define _ARETE_H_

#include<stdio.h>
#include<stdlib.h>

struct arete{
    int min;
    int max;
    int valuation;
}; typedef struct arete arete_t; 

arete_t creeArete(int,int,int);
#endif